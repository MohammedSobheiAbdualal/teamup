<?php

Route::get('/', function () {
    return redirect(admin_vw() . '/login');
});


Route::get('/reset-password', function () {
    return 'Reset password successfully';
});

Auth::routes(); //['verify' => true]

Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');
Route::get('/user/verify_page', 'Auth\RegisterController@verifyingPage');


Route::group(['prefix' => 'admin'], function () {

    Route::get('login', 'Auth\Admin\LoginController@showLoginForm')->name('admin.login');
    Route::post('login', 'Auth\Admin\LoginController@loginAdmin')->name('admin.auth.login');


    //admin password reset routes
    //https://academy.muva.tech/blog/part-viii-admin-password-reset-in-our-multiple-authentication-system-for-laravel-5-4/
    Route::post('/password/email', 'Auth\Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('/password/reset', 'Auth\Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('/password/reset', 'Auth\Admin\ResetPasswordController@reset');
    Route::get('/password/reset/{token}', 'Auth\Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');

    Route::get('/', 'HomeController@index');
    Route::get('dashboard', 'HomeController@index')->name('admin.index');

    Route::group(['middleware' => 'auth:admin'], function () {

//
//        foreach (\App\Models\Permission::all() as $permission) {
//            if ($permission->type == 'get')
//                Route::get('/' . $permission->alias . '/' . $permission->name, ['middleware' => 'permission:' . $permission->name, 'uses' => $permission->controller_name . '@' . $permission->function_name]);
//            if ($permission->type == 'post')
//                Route::post('/' . $permission->alias . '/' . $permission->name, ['middleware' => 'permission:' . $permission->name, 'uses' => $permission->controller_name . '@' . $permission->function_name]);
//            if ($permission->type == 'put')
//                Route::put('/' . $permission->alias . '/' . $permission->name, ['middleware' => 'permission:' . $permission->name, 'uses' => $permission->controller_name . '@' . $permission->function_name]);
//            if ($permission->type == 'delete')
//                Route::delete('/' . $permission->alias . '/' . $permission->name, ['middleware' => 'permission:' . $permission->name, 'uses' => $permission->controller_name . '@' . $permission->function_name]);
//        }

        Route::get('users-home', 'HomeController@getUsers');

        Route::group(['prefix' => 'role'], function () {
//            Route::get('roles', 'RoleController@roles');
//            Route::get('add-role', 'RoleController@addRole');
//            Route::post('add-role', 'RoleController@postAddRole');
//            Route::get('edit-role/{id}', 'RoleController@editRole');
//            Route::put('edit-role/{id}', 'RoleController@putRole');
//            Route::delete('delete-role/{id}', 'RoleController@delete');
            Route::get('role-data', 'RoleController@roleData');
//            Route::get('addPermission', 'PermissionController@addPermission');
//            Route::get('add-permission-role/{role_id}', 'PermissionController@permissions');
//            Route::get('add-permission-role/{role_id}', 'PermissionController@addPermission');
//            Route::get('add-permission-role/{role_id}', 'PermissionController@addPermissionRole');

            Route::get('permission-role/{role_id}', 'PermissionController@getPermissionRole');
            Route::post('add-role-permissions/{role_id}', 'PermissionController@postAddRolePermissions');

        });

        Route::group(['prefix' => 'manage'], function () {
            Route::get('admins', 'Admin\AdminController@index')->name('admin.admins.index');
            Route::get('admin-data', 'Admin\AdminController@anyData');
            Route::get('admin/{id}/edit', 'Admin\AdminController@edit');
            Route::put('admin/{id}', 'Admin\AdminController@update');
            Route::get('admin/create', 'Admin\AdminController@create');
            Route::post('admin', 'Admin\AdminController@store');
            Route::delete('admin/{id}', 'Admin\AdminController@delete');
//            Route::post('admin/export', 'AdminController@export');
            Route::put('admin-activation', 'Admin\AdminController@adminActivate');

            Route::get('users', 'Admin\UserController@index')->name('admin.users.index');
            Route::get('user-data', 'Admin\UserController@anyData');
            Route::get('user-details/{id}', 'Admin\UserController@userDetails');
//            Route::put('verify-email', 'UserController@verifyEmail');
            Route::put('user-activation', 'Admin\UserController@userActive');
            Route::put('user-league', 'Admin\UserController@userHasLeague');
//            Route::post('user/export', 'UserController@export');

        });

        Route::resource('teams', 'Admin\TeamController');
        Route::group(['prefix' => 'team'], function () {
            Route::get('team-data', 'Admin\TeamController@anyData');
            Route::put('activation', 'Admin\TeamController@teamActivate');
        });

//////////Sport/////////////////////////////////
        Route::resource('sports', 'Admin\SportController');
        Route::post('sports/upload-media/{id}/', 'Admin\SportController@upload_media');
        Route::delete('sports/remove-media/{id}/', 'Admin\SportController@remove_media');

        Route::group(['prefix' => 'sport'], function () {
            Route::get('sports-data', 'Admin\SportController@anyData');
            Route::put('activation', 'Admin\SportController@activation');
            Route::post('/{id}', 'Admin\SportController@update');

        });
 /////////////////////////////League/////////////
        ///
         Route::resource('leagues', 'Admin\LeagueController');
        Route::group(['prefix' => 'league'], function () {
            Route::get('league-data', 'Admin\LeagueController@anyData');
            Route::put('activation', 'Admin\LeagueController@activation');

        });
///////////////////Constants///
        Route::get('constant', 'Admin\ConstantController@index')->name('constants.index');

        Route::group(['prefix' => 'constant'], function () {

            Route::resource('payments', 'Admin\PaymentController');
            Route::get('payment/payment-data', 'Admin\PaymentController@anyData');

            Route::resource('awards', 'Admin\AwardController');
            Route::get('award/award-data', 'Admin\AwardController@anyData');

            Route::resource('cities', 'Admin\CityController');
            Route::get('city/city-data', 'Admin\CityController@anyData');

            /////FacilityGround
            Route::resource('facilityGrounds', 'Admin\FacilityGroundController');
            Route::get('facilityGround/ground-data', 'Admin\FacilityGroundController@anyData');


            /////FacilitySize
            Route::resource('facilitySizes', 'Admin\FacilitySizeController');
            Route::get('facilitySize/size-data', 'Admin\FacilitySizeController@anyData');


//            Route::get('setting/settings', 'SettingController@getSettings');
            Route::get('setting/setting-data', 'SettingController@anyData');
//            Route::get('setting/{id}/edit', 'SettingController@edit');
//            Route::put('setting/{id}/edit', 'SettingController@update');
//            Route::get('setting/create', 'SettingController@create');
//            Route::post('setting/create', 'SettingController@store');
//            Route::post('setting/export', 'SettingController@export');
//            Route::delete('setting/{id}', 'SettingController@delete');

        });


//        Route::post('addPermission', 'PermissionController@postAddPermission');

        Route::post('logout', 'Auth\Admin\LoginController@logout')->name('admin.logout');

    });
});
