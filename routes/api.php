<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// byv6alv1fc17


Route::group(['prefix' => version_api(), 'namespace' => namespace_api()], function () {

    Route::post('login', 'UserController@login');
    Route::post('refresh_token', 'UserController@refresh_token');
    Route::post('user', 'UserController@postUser');
    Route::post('confirm_code', 'UserController@verifyUser');
    Route::post('resend_code', 'UserController@resendCode');

    Route::post('forget', 'UserController@forget');
    Route::get('instructions', 'InstructionController@getInstructions');
    Route::get('setting/{title}', 'SettingController@getSetting');
    Route::get('settings', 'SettingController@getSettings');
    Route::get('sports', 'SportController@getSports');
    Route::get('cities', 'CityController@getCities');

    // Settings
    Route::get('socials', 'SettingController@getSocials');
    Route::get('league_property', 'LeagueController@getProperties');

    Route::group(['middleware' => ['auth:api']], function () {

        Route::put('user', 'UserController@update');
        Route::post('edit_mobile', 'UserController@putUserMobile');
        Route::get('user/{id?}', 'UserController@getUser');
        Route::post('user_sports', 'UserController@postSports');

        //
        Route::put('user', 'UserController@putUser');
        Route::post('logout', 'UserController@logout');
        Route::post('users', 'UserController@getUsers');

        // Sponsor
        Route::post('sponsors', 'SponsorController@getSponsors');
        Route::post('sponsor', 'SponsorController@createUpdateSponsor');
        Route::delete('sponsor/{id}', 'SponsorController@deleteSponsor');

        // CelebritiesGuest
        Route::post('celebrities_guests', 'CelebritiesGuestController@getCelebritiesGuests');
        Route::post('celebrities_guest', 'CelebritiesGuestController@createUpdateCelebritiesGuest');
        Route::delete('celebrities_guest/{id}', 'CelebritiesGuestController@deleteCelebritiesGuest');

        // leagues

        Route::post('league', 'LeagueController@createUpdateLeague');
        Route::post('save_publish_leagues', 'LeagueController@getSavedPublishLeagues');
        Route::delete('league/{id}', 'LeagueController@deleteLeague');
        Route::post('saved_locations', 'LeagueController@getSavedLocations');
        Route::post('teams_league', 'LeagueController@getTeamsInLeague');
        Route::post('league_players_categorizes_team', 'LeagueTeamController@getLeaguePlayersCategoryTeam');
        Route::post('league_players_team', 'LeagueTeamController@getLeaguePlayersTeam');

        // Team
        Route::post('save_publish_teams', 'TeamController@getSavedPublishTeams');

        Route::post('team', 'TeamController@createUpdateTeam');
        Route::delete('team/{id}', 'TeamController@deleteTeam');

        Route::post('invite', 'TeamPlayerController@postInvitation');
        Route::post('invite/change_status', 'TeamPlayerController@changeStatus');
        Route::post('invite/change_player_status', 'TeamPlayerController@changePlayerStatus');
        Route::put('team_player', 'TeamPlayerController@updateTeamPlayer');
        Route::put('change_injured', 'TeamPlayerController@setInjured');
        Route::delete('player_team', 'TeamPlayerController@deletePlayerTeam');

        Route::post('league_invite', 'LeagueTeamController@postInvitation');
        Route::post('league_invite/change_status', 'LeagueTeamController@changeStatus');
//        Route::post('invite/change_player_status', 'LeagueTeamController@changePlayerStatus');

        Route::post('categorizes', 'CategorizeController@getCategorizes');

        Route::post('favorite', 'FavoriteController@createRemoveFavorite');
        Route::post('friend', 'FriendController@createRemoveFriend');
        Route::post('follow', 'FollowController@createRemoveFollow');

        Route::post('notifications', 'NotificationController@getNotifications');
        Route::post('refresh_fcm_token', 'NotificationController@refreshFcmToken');
        Route::delete('notification', 'NotificationController@delete');
        Route::post('chat-notify', 'NotificationController@postChatNotification');
        Route::get('unseen-notification', 'NotificationController@getUnseenNotification');


    });

    Route::group(['middleware' => 'authGuest:api'], function () {
        Route::get('user/{id?}', 'UserController@getUser');

        Route::post('teams', 'TeamController@getTeams');

        Route::post('top_all_teams', 'TeamController@getTopAllTeams');
        Route::post('all_teams', 'TeamController@getAllTeams');
        Route::get('team/{id}', 'TeamController@getTeam');
        Route::post('players', 'TeamPlayerController@getPlayers');
        Route::post('category_players', 'TeamPlayerController@getCategoryPlayers');
        Route::post('team_category_players', 'TeamPlayerController@getTeamCategoryPlayers');
        Route::post('league_category_players', 'LeagueTeamController@getLeagueCategoryPlayers');

        Route::get('league/{id}', 'LeagueController@getLeague');
        Route::post('leagues', 'LeagueController@getLeagues');
        Route::post('all_leagues', 'LeagueController@getAllLeagues');


        Route::post('friends', 'FriendController@getFriends');
        Route::post('followers', 'FollowController@getFollowers');
        Route::post('followings', 'FollowController@getFollowings');


    });
});
