<?php

namespace App\Http\Resources\League;

use App\Models\LeagueTeam;
use App\Models\LeagueTeamPlayer;
use Illuminate\Http\Resources\Json\JsonResource;

class PlayersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $league_teams = LeagueTeam::where('team_id', request()->get('team_id'))->pluck('id')->unique();

        $league_team_player = LeagueTeamPlayer::whereIn('league_team_id', $league_teams)->where('player_id', $this->id)->first();

        return [
            'id' => $league_team_player->id,
            'user_id' => $this->id,
            'avatar' => $this->avatar,
            'full_name' => $this->full_name,
            'username' => $this->username,
            'rate' => $this->rate,
            'rate_num' => $this->rate_num,
            'primary_position' => $league_team_player->Position()->first(),
            'secondary_position' => $league_team_player->SecondaryPosition()->first(),
            'is_captain' => $league_team_player->is_captain,
            'is_injured' => $league_team_player->is_injured,
            'player_no' => $league_team_player->player_no,

        ];
    }
}
