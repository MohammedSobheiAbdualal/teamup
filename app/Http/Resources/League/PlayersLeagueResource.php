<?php

namespace App\Http\Resources\League;

use App\Models\LeagueTeam;
use App\Models\LeagueTeamPlayer;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class PlayersLeagueResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $league_teams = LeagueTeam::where('team_id', request()->get('team_id'))->where('status', 'accepted')->pluck('id')->unique();
        $league_team_players = LeagueTeamPlayer::where('position_id', $this->position_id)->where('league_team_id', $league_teams)->pluck('player_id')->unique();

        return [
            'position_id' => $this->position_id,
            'total_records' => $this->total_records,
            'players' => PlayersResource::collection(User::whereIn('id',$league_team_players)->get()),
            'position_name' => $this->position_name,
            'shortcut' => $this->shortcut,

        ];
    }
}
