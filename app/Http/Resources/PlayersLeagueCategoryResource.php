<?php

namespace App\Http\Resources;

use App\Http\Resources\League\PlayersResource;
use App\Models\LeagueTeam;
use App\Models\LeagueTeamPlayer;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class PlayersLeagueCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        $league_teams = LeagueTeam::where('team_id', $this->team_id)->where('league_id', $request->get('league_id'))->pluck('id')->unique();


        $league_team_players = LeagueTeamPlayer::whereIn('league_team_id', $league_teams)->pluck('player_id')->unique();

        request()->request->add(['team_id' => $this->team_id]);
        return [
            'team' => $this->Team,
            'total_records' => $this->total_records,
            'players' => User::whereIn('id', $league_team_players)->take(2)->get(),
            'team_name' => $this->Team->name,

        ];
    }
}
