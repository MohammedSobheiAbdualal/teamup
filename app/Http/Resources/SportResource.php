<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'image' => $this->image,
            'icon' => $this->icon,
            'has_position' => $this->has_position,
            'is_active' => $this->is_active,
            'positions' => $this->Positions()->get(),
        ];
    }
}
