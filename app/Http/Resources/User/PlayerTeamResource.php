<?php

namespace App\Http\Resources\User;

use App\Models\TeamPlayer;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class PlayerTeamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $players = TeamPlayer::where('position_id', $this->position_id)->where('team_id', request()->get('team_id'))->where('status', 'accepted')->pluck('user_id')->unique();

        return [
            'position_id' => $this->position_id,
            'total_records' => $this->total_records,
            'position_name' => $this->position_name,
            'shortcut' => $this->shortcut,
            'players' => PlayerResource::collection(User::where('is_active', 1)->whereIn('id', $players)->get()),
        ];
    }
}
