<?php

namespace App\Http\Resources\User;

use App\Models\Team;
use App\Models\TeamPlayer;
use Illuminate\Http\Resources\Json\JsonResource;

class PlayerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $team_player = TeamPlayer::where('team_id', request()->get('team_id'))->where('status', 'accepted')->where('user_id', $this->id)->first();

        return [
            'id' => $team_player->id,
            'user_id' => $this->id,
            'avatar' => $this->avatar,
            'full_name' => $this->full_name,
            'username' => $this->username,
            'rate' => $this->rate,
            'rate_num' => $this->rate_num,
            'primary_position' => $team_player->Position()->first(),
            'secondary_position' => $team_player->SecondaryPosition()->first(),
            'is_captain' => $team_player->is_captain,
            'is_injured' => $team_player->is_injured,
            'player_no' => $team_player->player_no,
        ];
    }
}
