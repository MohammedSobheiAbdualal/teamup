<?php

namespace App\Http\Requests\Constants;

use Illuminate\Foundation\Http\FormRequest;

class AwardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {

        return [
            //
            'title_ar' => 'required|string',
            'title_en' => 'required|string',
        ];

    }

    public function messages()
    {
        return [
            'title_ar' => 'Title EN is required',
            'title_en' => 'Title Ar is required',

        ];
    }
}
