<?php

namespace App\Http\Requests\Constants;

use Illuminate\Foundation\Http\FormRequest;

class CityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {

        return [
            //
            'name_ar' => 'required|string',
            'name_en' => 'required|string',
        ];

    }

    public function messages()
    {
        return [
            'name_ar' => 'Name EN is required',
            'name_en' => 'Name Ar is required',

        ];
    }
}
