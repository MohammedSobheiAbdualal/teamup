<?php

namespace App\Http\Requests\Constants;

use Illuminate\Foundation\Http\FormRequest;

class PaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {

        return [
            //
            'description_ar' => 'required|string',
            'description_en' => 'required|string',
        ];

    }

    public function messages()
    {
        return [
            'description_ar' => 'Description EN is required',
            'description_en' => 'Description Ar is required',

        ];
    }
}
