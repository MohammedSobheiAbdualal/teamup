<?php

namespace App\Http\Requests\Social;

use Illuminate\Foundation\Http\FormRequest;

class DeleteUserSocialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'social_id' => 'required|exists:user_social_links,id,user_id,' . auth()->user()->id
        ];
    }
}
