<?php

namespace App\Http\Requests\Sport;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {

        return [
            //
            'name_en' => 'required|string',
            'name_ar' => 'required|string',
            'icon' => 'required|integer',
            'image' => 'nullable|image',
            'has_position'=>'nullable',
            'position.0'=>'required_if:has_position,1|array',
            'position.1'=>'required_if:has_position,1|array',
            'position.2'=>'required_if:has_position,1|array'
        ];


    }


    public function messages()
    {
        return [
            'name_en' => 'Sport Name EN is required',
            'name_ar' => 'Sport Name Ar is required',
            'position.0.required_if' => 'Position Name EN is required',
            'position.1.required_if' => 'Position Name AR is required',
            'position.2.required_if' => 'Position Abbreviation is required',
        ];
    }
}
