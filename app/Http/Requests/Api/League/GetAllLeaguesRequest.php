<?php

namespace App\Http\Requests\Api\League;

use Illuminate\Foundation\Http\FormRequest;

class GetAllLeaguesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'is_all' => 'nullable|boolean',
//            'league_id' => 'nullable|exists:leagues,id,is_active,1',
//            'category_type' => 'nullable|in:favorite_team,previous_team,following_team,top_team,nlf_team',

            'type' => 'nullable|in:favorite,new',
            'sport_id' => 'nullable|exists:sports,id',

        ];
    }
}
