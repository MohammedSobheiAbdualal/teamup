<?php

namespace App\Http\Requests\Api\League;

use Illuminate\Foundation\Http\FormRequest;

class CreateUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


        if (request()->has('location'))
            request()->request->add(['location_' => json_decode(request()->get('location'))]);//
        if (request()->has('facility'))
            request()->request->add(['facility_' => json_decode(request()->get('facility'))]);//
        if (request()->has('channels'))
            request()->request->add(['channels_' => json_decode(request()->get('channels'))]);//
        if (request()->has('organizers'))
            request()->request->add(['organizers_' => json_decode(request()->get('organizers'))]);//
        if (request()->has('committee_officials'))
            request()->request->add(['committee_officials_' => json_decode(request()->get('committee_officials'))]);//
        if (request()->has('winners'))
            request()->request->add(['winners_' => json_decode(request()->get('winners'))]);//
        
        if (request()->has('league_id')) {

            return [
                //
                'league_id' => 'required|exists:leagues,id',
                'sport_id' => 'nullable|exists:sports,id,is_active,1',
                'type' => 'nullable|in:open,specify',
                'team_num' => 'required_if:type,specify|numeric|gt:0',
                'start_date' => 'nullable|date_format:Y-m-d',
                'end_date' => 'nullable|date_format:Y-m-d|after:start_date',
                'deadline_date' => 'nullable|date_format:Y-m-d H:i:s|before:start_date',
                'subscription_type' => 'nullable|in:free,paid',
                'payment_method_id' => 'required_if:subscription_type,paid|exists:payment_methods,id',
                'fees' => 'required_if:subscription_type,paid|numeric',
                'frcd' => 'required_if:subscription_type,paid|numeric',
                'main_lineup' => 'nullable|numeric',
                'bench' => 'nullable|numeric',
                'reserve_num' => 'nullable|numeric',
                'half_duration' => 'nullable|numeric',
                'extra' => 'nullable|in:penalties,extra_halves_penalties',
                'extra_half_duration' => 'required_if:extra,extra_halves_penalties|numeric',
                'short_description' => 'nullable',
                'allow_call_me' => 'nullable|boolean',
                'has_restricted' => 'nullable|boolean',
                'has_service' => 'nullable|boolean',
                'has_rule' => 'nullable|boolean',
                'logo' => 'nullable|image',
                'bg_image' => 'nullable|image',
//                'location_' => 'nullable',
                'location_.*.city_id' => 'nullable|exists:cities,id',
                'location_.*.address' => 'nullable',
                'location_.*.latitude' => 'nullable',
                'location_.*.longitude' => 'nullable',
//                'facility_' => 'nullable',
                'facility_.*.name' => 'nullable',
                'facility_.*.size_id' => 'nullable|exists:facility_sizes,id',
                'facility_.*.ground_id' => 'nullable|exists:facility_grounds,id',
                'facility_images.*' => 'nullable|image',
                'old_facility_images.*' => 'nullable|numeric',
//                'channels_' => 'nullable',
                'channels_.*.user_id' => 'required_if:channels,<>,null|exists:users,id,is_active,1',
//                'organizers_' => 'nullable',
                'organizers_.*.user_id' => 'required_if:organizers,<>,null|exists:users,id,is_active,1',
//                'committee_officials_' => 'nullable',
                'committee_officials_.*.user_id' => 'required_if:committee_officials,<>,null|exists:users,id,is_active,1',
                'committee_officials_.*.sub_type' => 'required_if:committee_officials,<>,null|in:committee,official',

//                'awards_' => 'nullable',
//                'awards_.*.winner_id' => 'nullable|exists:winners,id',
//                'awards_.*.award_id' => 'nullable|exists:awards,id',
//                'awards_.*.other_winner' => 'nullable',
//                'awards_.*.other_award' => 'nullable',
//                'awards_.*.money_value' => 'nullable|numeric|gte:0',

                'winners_.*.winner_id' => 'nullable|exists:winners,id',
                'winners_.*.other_winner' => 'nullable',

                'winners_.*.awards.*.award_id' => 'nullable|exists:awards,id',
                'winners_.*.awards.*.other_award' => 'nullable',
                'winners_.*.awards.*.money_value' => 'nullable|numeric|gte:0',

                'celebrities_guests.*' => 'nullable|exists:celebrities_guests,id',
                'restrictions.*' => 'required_if:has_restricted,1',
                'services.*' => 'required_if:has_service,1',
                'rules.*' => 'required_if:has_rule,1',
                'sponsors.*' => 'nullable|exists:sponsors,id',
                'main_sponsor_id' => 'nullable|exists:sponsors,id',

            ];

        }

        return [
            //
            'name' => 'required',
            'sport_id' => 'required|exists:sports,id,is_active,1',
            'type' => 'required|in:open,specify',
            'team_num' => 'required_if:type,specify|numeric|gt:0',
            'start_date' => 'required|date_format:Y-m-d',
            'end_date' => 'required|date_format:Y-m-d|after:start_date',
            'deadline_date' => 'required|date_format:Y-m-d H:i:s|before:start_date',
            'subscription_type' => 'required|in:free,paid',
            'payment_method_id' => 'required_if:subscription_type,paid|exists:payment_methods,id',
            'fees' => 'required_if:subscription_type,paid|numeric',
            'frcd' => 'required_if:subscription_type,paid|numeric',
            'main_lineup' => 'required|numeric',
            'bench' => 'required|numeric',
            'reserve_num' => 'nullable|numeric',
            'half_duration' => 'required|numeric',
            'extra' => 'required|in:penalties,extra_halves_penalties',
            'extra_half_duration' => 'required_if:extra,extra_halves_penalties|numeric',
            'short_description' => 'required',
            'allow_call_me' => 'nullable|boolean',
            'has_restricted' => 'nullable|boolean',
            'has_service' => 'nullable|boolean',
            'has_rule' => 'nullable|boolean',
            'logo' => 'nullable|image',
            'bg_image' => 'nullable|image',
//            'location' => 'required',
            'location_.*.city_id' => 'required|exists:cities,id',
            'location_.*.address' => 'required',
            'location_.*.latitude' => 'required',
            'location_.*.longitude' => 'required',
//            'facility' => 'required',
            'facility_.*.name' => 'required',
            'facility_.*.size_id' => 'required|exists:facility_sizes,id',
            'facility_.*.ground_id' => 'required|exists:facility_grounds,id',
            'facility_images.*' => 'nullable|image',
//            'channels' => 'nullable',
            'channels_.*.user_id' => 'required_if:channels,<>,null|exists:users,id,is_active,1',
//            'organizers_' => 'nullable',
            'organizers_.*.user_id' => 'required_if:channels,<>,null|exists:users,id,is_active,1',
//            'committee_officials_' => 'nullable',
            'committee_officials_.*.user_id' => 'required_if:channels,<>,null|exists:users,id,is_active,1',
            'committee_officials_.*.sub_type' => 'required_if:channels,<>,null|in:committee,official',



//            'winners_' => 'nullable',
            'winners_.*.winner_id' => 'nullable|exists:winners,id',
            'winners_.*.other_winner' => 'nullable',

            'winners_.*.awards.*.award_id' => 'nullable|exists:awards,id',
            'winners_.*.awards.*.other_award' => 'nullable',
            'winners_.*.awards.*.money_value' => 'nullable|numeric|gte:0',


            'celebrities_guests' => 'nullable|exists:celebrities_guests,id',
            'restrictions' => 'required_if:has_restricted,1',
            'restrictions.*' => 'required_if:has_restricted,1',
            'services' => 'required_if:has_service,1',
            'services.*' => 'required_if:has_service,1',
            'rules' => 'required_if:has_rule,1',
            'rules.*' => 'required_if:has_rule,1',
//            'sponsors' => 'nullable|exists:sponsors,id',
            'sponsors.*' => 'nullable|exists:sponsors,id',
            'main_sponsor_id' => 'nullable|exists:sponsors,id',

        ];
    }
}
