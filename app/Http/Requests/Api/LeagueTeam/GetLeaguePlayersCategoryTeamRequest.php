<?php

namespace App\Http\Requests\Api\LeagueTeam;

use Illuminate\Foundation\Http\FormRequest;

class GetLeaguePlayersCategoryTeamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'page_size' => 'nullable|numeric|gt:0',
            'page_number' => 'nullable|numeric|gt:0',
            'league_id' => 'required|exists:leagues,id',
        ];

    }
}
