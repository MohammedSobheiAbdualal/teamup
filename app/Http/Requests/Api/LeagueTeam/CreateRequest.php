<?php

namespace App\Http\Requests\Api\LeagueTeam;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'team_id.*' => 'required|exists:teams,id,is_active,1',
            'league_id' => 'required|exists:leagues,id,is_active,1',
        ];
    }
}
