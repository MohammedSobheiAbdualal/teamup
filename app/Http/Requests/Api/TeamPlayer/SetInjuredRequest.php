<?php

namespace App\Http\Requests\Api\TeamPlayer;

use Illuminate\Foundation\Http\FormRequest;

class SetInjuredRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'team_player_id' => 'required|exists:team_players,id'
        ];
    }
}
