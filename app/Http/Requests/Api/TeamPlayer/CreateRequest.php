<?php

namespace App\Http\Requests\Api\TeamPlayer;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'user_id.*' => 'required|exists:users,id,is_active,1',
            'team_id' => 'required|exists:teams,id,is_active,1',
            'manager_id' => 'nullable|exists:users,id,is_active,1',
        ];
    }
}
