<?php

namespace App\Http\Requests\Api\TeamPlayer;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTeamPlayerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if (request()->has('players'))
            request()->request->add(['players_' => json_decode(request()->get('players'))]);//

        return [
            //
            'players_.*.team_player_id' => 'required|exists:team_players,id',
            'players_.*.player_no' => 'required|numeric',
            'players_.*.position_id' => 'required|exists:positions,id',
            'players_.*.secondary_position_id' => 'required|exists:positions,id',
            'players_.*.is_captain' => 'required|in:0,1',
            'players_.*.is_injured' => 'required|in:0,1',
        ];
    }
}
