<?php

namespace App\Http\Requests\Api\Categorized;

use Illuminate\Foundation\Http\FormRequest;

class GetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'action_id' => 'required',//|exists:teams,id', Team or League
            'type' => 'required|in:team,league'
        ];
    }
}
