<?php

namespace App\Http\Requests\Api\Sponsor;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->has('social_media'))
            request()->request->add(['social_media_' => json_decode(request()->get('social_media'))]);//

        if (request()->has('sponsor_id')) {
            return [
                //
                'user_id' => 'nullable|exists:users,id',
                'name' => 'nullable',
                'description' => 'nullable',
                'details' => 'nullable',
                'logo' => 'nullable|image',
                'sponsor_id' => 'required|exists:sponsors,id',
                'social_media_.*.social_id' => 'nullable|exists:social_media,id',
                'social_media_.*.link' => 'nullable|url',
            ];
        }
        return [
            //
            'user_id' => 'nullable|exists:users,id',
            'name' => 'required',
            'description' => 'required',
            'details' => 'nullable',
            'logo' => 'nullable|image',
            'social_media_.*.social_id' => 'nullable|exists:social_media,id',
            'social_media_.*.link' => 'nullable|url',
        ];
    }
}
