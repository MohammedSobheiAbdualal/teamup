<?php

namespace App\Http\Requests\Api\Sponsor;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'user_id' => 'nullable|exists:users,id',
            'name' => 'nullable',
            'description' => 'nullable',
            'details' => 'nullable',
            'logo' => 'nullable|image',
            'social_media.*.social_id' => 'nullable|exists:social_media,id',
            'social_media.*.link' => 'nullable|exists:social_media,id',
        ];
    }
}
