<?php

namespace App\Http\Requests\Api\Team;

use Illuminate\Foundation\Http\FormRequest;

class CreateUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if (request()->has('team_id')) {
            return [
                //
                'name' => 'required',
                'bio' => 'nullable',
                'city_id' => 'required|exists:cities,id',
                'team_id' => 'required|exists:teams,id',
                'sport_id' => 'required|exists:sports,id',
                'status' => 'nullable|in:saved,published',
                'logo' => 'nullable|image',
                'bg_image' => 'nullable|image',
                'sponsors.*' => 'nullable|exists:sponsors,id',
                'main_sponsor_id' => 'nullable|exists:sponsors,id',
            ];
        }
        return [
            //
            'name' => 'required',
            'bio' => 'nullable',
            'city_id' => 'required|exists:cities,id',
            'sport_id' => 'required|exists:sports,id',
            'status' => 'nullable|in:saved,published',
            'logo' => 'nullable|image',
            'bg_image' => 'nullable|image',
            'sponsors.*' => 'nullable|exists:sponsors,id',
            'main_sponsor_id' => 'nullable|exists:sponsors,id',
        ];
    }
}
