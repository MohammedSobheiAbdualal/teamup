<?php

namespace App\Http\Requests\Api\CelebritiesGuest;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->has('celebrities_guest_id')) {
            return [
                //
                'user_id' => 'nullable|exists:users,id',
                'guest_name' => 'required_without:user_id',
                'guest_picture' => 'nullable|image',
                'celebrities_guest_id' => 'required|exists:celebrities_guests,id',
            ];
        }
        return [
            //
            'user_id' => 'nullable|exists:users,id',
            'guest_name' => 'required_without:user_id',
            'guest_picture' => 'nullable|image',
        ];
    }
}
