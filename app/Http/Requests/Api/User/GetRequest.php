<?php

namespace App\Http\Requests\Api\User;

use Illuminate\Foundation\Http\FormRequest;

class GetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'is_all' => 'required|boolean',
            'team_id' => 'nullable|exists:teams,id',
            'exclude_sponsor' => 'nullable|boolean',
            'category_type' => 'nullable|in:friend,favorite_player,team_mate,top_player,near_by_player,previous_players,following,follower,in_team,played_league,nlf_league',
            'category_team_id' => 'required_if:category_type,team_mate',
            'page_size' => 'nullable|numeric|gt:0',
            'page_number' => 'nullable|numeric|gt:0',
        ];
    }
}
