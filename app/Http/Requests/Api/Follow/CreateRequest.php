<?php

namespace App\Http\Requests\Api\Follow;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $temp = '';
        if (request()->has('type'))
            if (request()->get('type') == 'player')
                $temp .= '|exists:users,id,deleted_at,NULL';
            elseif (request()->get('type') == 'team')
                $temp .= '|exists:teams,id,deleted_at,NULL';

        return [
            //'team', 'user'
            'follow_id' => 'required' . $temp,
            'type' => 'required|in:team,player,league',

        ];
    }
}
