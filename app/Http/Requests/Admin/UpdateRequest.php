<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            //
            'username' => 'required|unique:admins,username,'.request()->route('id'),
            'name' => 'required|string',
            'email' => 'required|email|unique:admins,email,'.request()->route('id'),
            'mobile'=>'nullable|digits_between:9,12',
           // 'password' => 'required|min:6|confirmed',

        ];
    }
}
