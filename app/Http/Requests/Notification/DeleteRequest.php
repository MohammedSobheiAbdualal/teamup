<?php

namespace App\Http\Requests\Notification;

use Illuminate\Foundation\Http\FormRequest;

class DeleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'notification_id' => 'required|exists:notifications,id'

        ];
    }

    public function attributes()
    {
        return [
            'notification_id' => trans('app.notification.notification_id'),
        ];
    }
}
