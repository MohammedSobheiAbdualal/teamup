<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PostSportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'sports.*.sport_id'=>'required|exists:sports,id',
            'sports.*.positions.primary_position'=>'nullable|exists:positions,id',
            'sports.*.positions.secondary_position'=>'nullable|exists:positions,id',
        ];


    }

    public function attributes()
    {
        return [
            'sports.*.sport_id' => trans('app.sport'),
            'sports.*.positions.primary_position' => trans('app.primary_position'),
            'sports.*.positions.secondary_position'=> trans('app.secondary_position'),

        ];
    }
}
