<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'username' => ['required', 'string', 'max:30', 'unique:users,username', 'regex:/^[A-Za-z0-9_.-]+$/'],
            'mobile' => 'required|numeric|digits:9|unique:users,mobile',
            'country_code' => 'required|numeric|digits_between:1,3',
            'password' => 'required|min:6|confirmed',

        ];
    }
}
