<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserMobileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'code'=>'required|numeric|digits:4',
            'mobile' => 'required|numeric|digits:9|unique:users,mobile,'.auth()->user()->id,
            'country_code' => 'required|numeric|digits_between:1,3',

        ];


    }
}
