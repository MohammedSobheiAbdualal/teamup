<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            //
            'first_name' => 'nullable|string|min:3',
            'last_name' => 'nullable|string|min:3',
            'bio' => 'nullable|max:400',
            'email' => 'nullable|email|unique:users,email,' . auth()->user()->id,
            'birthdate' => 'nullable|string',
            'city_id' => 'nullable|integer|exists:cities,id',
            'avatar' => 'nullable|image',
            'bg_image' => 'nullable|image',
        ];

        if (request()->has('password') && request()->get('password') != '') {
            $rules['old_password'] = 'required';
            $rules['password'] = 'required|min:6|confirmed';
        }
        return $rules;
    }
}
