<?php

namespace App\Http\Requests\User;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            //
            'grant_type' => 'required|in:password',
            'username' => 'required',
            'password' => 'required',
            'device_type' => 'required|in:android,ios',
            'device_token' => 'required',
            'device_id' => 'sometimes',
        ];
    }
}
