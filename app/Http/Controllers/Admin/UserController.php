<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\Eloquents\UserEloquent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    //
    private $user;

    public function __construct(UserEloquent $userEloquent)
    {
        $this->user = $userEloquent;
        view()->share(['main_title' => 'Users management']);

    }

    public function index()
    {

        $data = [
            'sub_title' => 'User apps',
            'icon' => 'icon-users'
        ];
        return view(admin_users_vw() . '.index', $data);
    }

    public function anyData()
    {
        return $this->user->anyData();
    }
    public function userDetails($id)
    {
        $user = $this->user->getById($id);
        $data = [
            'sub_title' => 'User apps',
            'icon' => 'icon-users',
            'user' => $user,
            'back_url'=>url(admin_manage_url().'/users')

        ];
        return view(admin_users_vw() . '.view', $data);
    }
    public function userActive(Request $request)
    {
        return $this->user->userActive($request->all());
    }

    public function verifyEmail(Request $request)
    {
        return $this->user->verifyEmail($request->only('user_id'));
    }

    public function userDet($id)
    {
        $user = $this->user->getById($id);
        $data = [
            'sub_title' => 'User apps',
            'icon' => 'icon-users',
            'user' => $user,
            'back_url'=>url(admin_manage_url().'/users')

        ];
        return view(admin_users_vw() . '.view', $data);
    }

    public function export()
    {
        return $this->user->export();
    }

    public function userHasLeague(Request $request){
        return $this->user->hasLeague($request->all());
    }

}
