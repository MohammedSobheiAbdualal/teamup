<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\City;
use Illuminate\Http\Request;
use App\Repositories\Eloquents\CityEloquent;
use App\Http\Requests\Constants\CityRequest;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $city;
    public function __construct(CityEloquent $cityEloquent)
    {
        $this->city = $cityEloquent;
        view()->share(['main_title' => 'Cities management']);

    }
    public function index()
    {
        //
        $data = [
            'sub_title' => 'Cities',
            'icon' => 'icon-awards',
        ];

        return view(admin_constants_vw() . '.cities',$data);
    }
    public function anyData()
    {
        return $this->city->anyData();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $view = view()->make(admin_vw() . '.modal', [
            'modal_id' => 'add-city',
            'modal_title' => 'Add New City',
            'form' => [
                'method' => 'POST',
                'url' => url(admin_constant_url() . '/cities'),
                'form_id' => 'formAdd',
                'fields' => [
                    'name_ar' => 'text',
                    'name_en' => 'text',
                    'map'=>'map'
                ],
                'fields_name' => [
                    'name_ar' => 'Name Arabic',
                    'name_en' => 'Name English',
                    'map'=>'Location'
                ]
            ]
        ]);

        $html = $view->render();

        return $html;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CityRequest $request)
    {
        //
        return $this->city->create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $city = City::find($id);
        $html = 'This city does not exist';
        if (isset($city)) {
            $view = view()->make(admin_vw() . '.modal', [
                'modal_id' => 'edit-city',
                'modal_title' => 'Edit City',
                'form' => [
                    'method' => 'PUT',
                    'url' => url(admin_constant_url() . '/cities/' . $id),
                    'form_id' => 'formEdit',
                    'fields' => [
                        'name_ar' => 'text',
                        'name_en' => 'text',
                        'map'=>'map'

                    ],
                    'values' => [
                        'name_ar' => $city->name_ar,
                        'name_en' => $city->name_en,
                        'lat'=>$city->latitude,
                        'lng'=>$city->longitude,
                    ],
                    'fields_name' => [
                        'name_ar' => 'Name Arabic',
                        'name_en' => 'Name English',
                        'map'=>'Location'
                    ]
                ]
            ]);

            $html = $view->render();
        }
        return $html;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CityRequest $request, $id)
    {
        //
        return $this->city->update($request->all(),$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        return $this->city->delete($id);
    }
}
