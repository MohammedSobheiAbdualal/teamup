<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Sport;
use Illuminate\Http\Request;
use App\Repositories\Eloquents\LeagueEloquent;

class LeagueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $league;

    public function __construct(LeagueEloquent $leagueEloquent)
    {
        $this->league = $leagueEloquent;
        view()->share(['main_title' => 'Leagues management']);

    }
    public function anyData()
    {
        return $this->league->anyData();
    }
    public function index()
    {
        $sports=Sport::all();
        $data = [
            'sub_title' => 'Leagues',
            'icon' => 'icon-leagues',
            'sports'=>$sports
        ];
        return view(admin_leagues_vw() . '.index', $data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $league= $this->league->getById($id);
        $data = [
            'sub_title' => 'Teams',
            'icon' => 'icon-bubbles',
            'league' => $league,
            'back_url' => url(admin_league_url() . '/')
        ];


        return view(admin_leagues_vw() . '.view',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function activation(Request $request)
    {
        return $this->league->activation($request->all());
    }
}
