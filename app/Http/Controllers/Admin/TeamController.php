<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Sport;
use Illuminate\Http\Request;
use App\Repositories\Eloquents\TeamEloquent;


class TeamController extends Controller
{
    //
    private $team;

    public function __construct(TeamEloquent $teamEloquent)
    {
        $this->team = $teamEloquent;
        view()->share(['main_title' => 'Teams management']);

    }

    public function index()
    {
       $sports=Sport::all();
        $data = [
            'sub_title' => 'Teams',
            'icon' => 'icon-teams',
            'sports'=>$sports,
        ];
        return view(admin_teams_vw() . '.index', $data);
    }

    public function anyData()
    {
        return $this->team->anyData();
    }

    public function show($id)
    {
        $team= $this->team->getById($id);
        $data = [
            'sub_title' => 'Teams',
            'icon' => 'icon-bubbles',
            'team' => $team,
            'back_url' => url(admin_team_url() . '/teams')
        ];


        return view(admin_teams_vw() . '.view',$data);
    }

    public function teamActivate(Request $request)
    {
        return $this->team->teamActivate($request->all());
    }

}
