<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PaymentMethod;
use Illuminate\Http\Request;
use App\Repositories\Eloquents\PaymentMethodEloquent;
use App\Http\Requests\Constants\PaymentRequest;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $payment;
    public function __construct(PaymentMethodEloquent $paymentEloquent)
    {
        $this->payment = $paymentEloquent;
        view()->share(['main_title' => 'Payments management']);

    }
    public function index()
    {
        //
        $data = [
            'sub_title' => 'Payments',
            'icon' => 'icon-payments',
        ];
        return view(admin_constants_vw() . '.payments');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function anyData()
    {
        return $this->payment->anyData();
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
               $payment = PaymentMethod::find($id);
                $html = 'This payment does not exist';
                if (isset($payment)) {
                    $view = view()->make(admin_vw() . '.modal', [
                        'modal_id' => 'edit-payment',
                        'modal_title' => 'Edit Payment',
                        'form' => [
                            'method' => 'PUT',
                            'url' => url(admin_constant_url() . '/payments/' . $id),
                            'form_id' => 'formEdit',
                            'fields' => [
                                'title_ar' => 'text_dis',
                                'title_en' => 'text_dis',
                                'type' => 'text_dis',
                                'description_ar' => 'ckeditor',
                                'description_en' => 'ckeditor',

                            ],
                            'values' => [
                                'title_ar' => $payment->title_ar,
                                'title_en' => $payment->title_en,
                                'type' => $payment->type,
                                'description_ar' => $payment->description_ar,
                                'description_en' => $payment->description_en,
                            ],
                            'fields_name' => [
                                'title_ar' => 'Title Arabic',
                                'title_en' => 'Title English',
                                'type' => 'Type',
                                'description_ar' => 'Description Arabic ',
                                'description_en' => 'Description English ',
                            ]
                        ]
                    ]);

                    $html = $view->render();
                }
                return $html;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PaymentRequest $request, $id)
    {
        //
        return $this->payment->update($request->all(),$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
