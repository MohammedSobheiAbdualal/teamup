<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreateAdminRequest;
use App\Http\Requests\Admin\UpdateRequest;
use App\Repositories\Eloquents\AdminEloquent;
use App\Repositories\Eloquents\UserRoleEloquent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//use App\Models\Role;


class AdminController extends Controller
{
    //
    private $admin, $user_role;

    public function __construct(AdminEloquent $adminEloquent, UserRoleEloquent $userRoleEloquent)
    {
        $this->middleware('auth:admin');
        $this->admin = $adminEloquent;
        $this->user_role = $userRoleEloquent;
       // $roles = Role::all();

       // view()->share(['main_title' => 'Users management', 'roles' => $roles]);
        view()->share(['main_title' => 'System management']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {

        $data = [
            'sub_title' => 'Admins',
            'icon' => 'icon-users',
        ];

        return view(admin_admins_vw() . '.index', $data);
    }

    public function anyData()
    {
        return $this->admin->anyData();
    }

//
    public function edit($id)
    {

        $admin = $this->admin->getById($id);

        $html = 'This admin does not exist';
        if (isset($admin)) {
            $view = view()->make(admin_vw() . '.modal', [
                'modal_id' => 'edit-admin',
                'modal_title' => 'Edit Admin',
                //'roles_id' => Role::all()->pluck('id')->toArray(),
                'form' => [
                    'method' => 'PUT',
                    'url' => url(admin_vw() . '/manage/admin/' . $id),
                    'form_id' => 'formEdit',
                    'fields' => [
                        'username' => 'text',
                        'name' => 'text',
                        'email' => 'email',
                        'mobile'=>'text',
//                        'password' => 'password',
//                        'password_confirmation' => 'password',
                     //   'role[]' => Role::all(),
                    ],
                    'values' => [
                        'username' => $admin->username,
                        'name' => $admin->name,
                        'email' => $admin->email,
                        'mobile'=>$admin->mobile?$admin->mobile : '',
//                        'password' => '',
//                        'password_confirmation' => '',
                       // 'role[]' => $admin->roles()->get(),
                       // 'role_res[]' => $admin->roles()->pluck('id')->toArray(),
                    ],
                    'fields_name' => [
                        'username' => 'Username',
                        'name' => 'Name',
                        'email' => 'Email',
                        'mobile' => 'Mobile',
//                        'password' => 'Password',
//                        'password_confirmation' => 'Confirm Password',
                      //  'role[]' => 'Role',
                    ]
                ]
            ]);

            $html = $view->render();
        }
        return $html;
    }

////
    public function update(UpdateRequest $request, $id)
    {
        return $this->admin->update($request->all(), $id);
    }
//
////
    public function create()
    {
        $view = view()->make(admin_vw() . '.modal', [
            'modal_id' => 'add-admin',
            'modal_title' => 'Add New Admin',
            'form' => [
                'method' => 'POST',
                'url' => url(admin_vw() . '/manage/admin'),
                'form_id' => 'formAdd',
                'fields' => [
                    'username' => 'text',
                    'name'=>'text',
                    'email' => 'email',
                    'mobile'=>'text',
                    'password' => 'password',
                    'password_confirmation' => 'password',
                  //  'role[]' => Role::all(),
                ],
                'fields_name' => [
                    'username' => 'Username',
                    'name'=>'Name',
                    'email' => 'Email',
                    'mobile'=>'Mobile',
                    'password' => 'Password',
                    'password_confirmation' => 'Confirm Password',
                    //'role[]' => 'Role',
                ]
            ]
        ]);

        $html = $view->render();

        return $html;
    }

//
    public function store(CreateAdminRequest $request)
    {
        return $this->admin->create($request->all());

    }
//
    public function delete($id)
    {
        return $this->admin->delete($id);
    }

    public function adminActivate(Request $request)
    {
        return $this->admin->adminActivate($request->only('admin_id'));
    }

//
////
//    public function export()
//    {
//
//        return $this->admin->export();
//    }
}
