<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Sport\CreateRequest;
use App\Models\Icon;
use App\Repositories\Eloquents\SportEloquent;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    private $sport;

    public function __construct(SportEloquent $sportEloquent)
    {
        $this->sport = $sportEloquent;
        view()->share(['main_title' => 'Sports management']);

    }

    public function index()
    {
        //
        $data = [
            'sub_title' => 'Sports',
            'icon' => 'icon-sports',

        ];
        return view(admin_sports_vw() . '.index');
    }

    public function anyData()
    {
        return $this->sport->anyData();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        $icons = Icon::all();
        return view(admin_sports_vw() . '.create', compact('icons'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(CreateRequest $request)
    {
        //
        return $this->sport->create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $icons = Icon::all();
        $sport = $this->sport->getById($id);
        return view(admin_sports_vw() . '.edit', compact('icons', 'sport'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(CreateRequest $request, $id)
    {
        return $this->sport->update($request->all(), $id);
    }

    public function upload_media(Request $request, $id)
    {
        return $this->sport->uploadMedia($request->all(), $id);
    }
    public function remove_media(Request $request, $id)
    {
        return $this->sport->removeMedia($request->all(), $id);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        return $this->sport->delete($id);
    }

    public function activation(Request $request)
    {
        return $this->sport->activation($request->all());
    }
}
