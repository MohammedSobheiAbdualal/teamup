<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Award;
use Illuminate\Http\Request;
use App\Repositories\Eloquents\AwardEloquent;
use App\Http\Requests\Constants\AwardRequest;

class AwardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $award;
    public function __construct(AwardEloquent $awardEloquent)
    {
        $this->award = $awardEloquent;
        view()->share(['main_title' => 'Awards management']);

    }
    public function index()
    {
        //
        $data = [
            'sub_title' => 'Awards',
            'icon' => 'icon-awards',
        ];
        return view(admin_constants_vw() . '.awards',$data);
    }


    public function anyData()
    {
        return $this->award->anyData();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $award = Award::find($id);

        $html = 'This award does not exist';
        if (isset($award)) {
            $view = view()->make(admin_vw() . '.modal', [
                'modal_id' => 'edit-award',
                'modal_title' => 'Edit Award',
                'form' => [
                    'method' => 'PUT',
                    'url' => url(admin_constant_url() . '/awards/' . $id),
                    'form_id' => 'formEdit',
                    'fields' => [
                        'title_ar' => 'text',
                        'title_en' => 'text',
                    ],
                    'values' => [
                        'title_ar' => $award->title_ar,
                        'title_en' => $award->title_en,
                    ],
                    'fields_name' => [
                        'title_ar' => 'Title Arabic',
                        'title_en' => 'Title English',
                    ]
                ]
            ]);

            $html = $view->render();
        }
        return $html;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AwardRequest $request, $id)
    {
        //
        return $this->award->update($request->all(),$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
