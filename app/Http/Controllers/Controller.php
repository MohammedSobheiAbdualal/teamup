<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        // default language system
        $lang = 'en';
        if (request()->hasHeader('lang'))
            $lang = request()->header('lang');
        app()->setLocale($lang);
        $lang = \App\Models\Language::where('iso', $lang)->first();
        if (isset($lang))
            config()->set(['app.lang_id' => $lang->id]);


    }
}
