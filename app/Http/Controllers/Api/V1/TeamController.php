<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Team\CreateUpdateRequest;
use App\Http\Requests\Api\Team\GetAllTeamsRequest;
use App\Http\Requests\Api\Team\GetRequest;
use App\Http\Requests\Api\Team\GetTopTeamsRequest;
use App\Repositories\Eloquents\TeamEloquent;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    //
    private $team;

    public function __construct(TeamEloquent $teamEloquent)
    {
        parent::__construct();
        $this->team = $teamEloquent;
    }

    public function getTeam($id)
    {
        return $this->team->getById($id);
    }

    public function getTeams(GetRequest $request)
    {
        return $this->team->getAll($request->all());
    }

    public function getAllTeams(GetAllTeamsRequest $request)
    {
        return $this->team->getAllTeams($request->all());
    }

    public function getSavedPublishTeams(GetRequest $request)
    {
        return $this->team->getSavedPublishTeams($request->all());
    }

    public function createUpdateTeam(CreateUpdateRequest $request)
    {
        if ($request->has('team_id'))
            return $this->team->update($request->all(), $request->get('team_id'));
        return $this->team->create($request->all());
    }

    public function deleteTeam($id)
    {
        return $this->team->delete($id);
    }

    public function getTopAllTeams(GetTopTeamsRequest $request)
    {
        return $this->team->getTopAllTeams($request->all());
    }
}
