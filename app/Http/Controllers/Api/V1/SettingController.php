<?php

namespace App\Http\Controllers\Api\V1;

use App\Repositories\Eloquents\SettingEloquent;
use App\Repositories\Eloquents\SocialMediaEloquent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    //
    private $setting, $social;

    public function __construct(SettingEloquent $settingEloquent, SocialMediaEloquent $socialMediaEloquent)
    {
        parent::__construct();
        $this->setting = $settingEloquent;
        $this->social = $socialMediaEloquent;
    }

    //get system's setting by title
    public function getSetting($title)
    {
        return $this->setting->getByTitle($title);
    }

    //get all system's settings
    public function getSettings()
    {
        return $this->setting->getAll([]);
    }


    public function getSocials()
    {
        return $this->social->getAll([]);
    }
}
