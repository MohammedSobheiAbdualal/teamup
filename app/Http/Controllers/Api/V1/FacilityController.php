<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Repositories\Eloquents\FacilityEloquent;
use App\Repositories\Eloquents\FacilityGroundEloquent;
use App\Repositories\Eloquents\FacilitySizeEloquent;
use Illuminate\Http\Request;

class FacilityController extends Controller
{
    //
    private $facility, $size, $ground;

    public function __construct(FacilityEloquent $facilityEloquent, FacilitySizeEloquent $facilitySizeEloquent, FacilityGroundEloquent $facilityGroundEloquent)
    {
        parent::__construct();
        $this->facility = $facilityEloquent;
        $this->size = $facilitySizeEloquent;
        $this->ground = $facilityGroundEloquent;
    }

    public function getSizes()
    {
        return $this->size->getAll([]);
    }

    public function getGrounds()
    {
        return $this->ground->getAll([]);
    }
}
