<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\GetRequest;
use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\ForgetRequest;
use App\Http\Requests\User\LoginRequest;
use App\Http\Requests\User\ResendCodeRequest;
use App\Http\Requests\User\VerifyUserRequest;
use App\Http\Requests\User\PostSportRequest;
use App\Http\Requests\User\UpdateUserMobileRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Repositories\Eloquents\UserEloquent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use App\Models\User;

class UserController extends Controller
{
    //
    private $user;

    public function __construct(UserEloquent $user)
    {
        parent::__construct();
        $this->user = $user;
    }

    // generate access token for login
    public function login(LoginRequest $request)
    {
        return $this->user->login($request->all());
    }

    // generate refresh token when access token expired
    public function refresh_token()
    {
        return $this->user->refresh_token();
    }

    // Sign up
    public function postUser(CreateUserRequest $request)
    {
        return $this->user->create($request->all());
    }

    // get User profile by id
    public function getUser($id = null)
    {
        return $this->user->getById($id);
    }

    // get Users
    public function getUsers(GetRequest $request)
    {
        return $this->user->getAll($request->all());
    }

    // User update profile
    public function putUser(UpdateUserRequest $request)
    {
        return $this->user->update($request->all());
    }

    // forget password "We have to establish an email account from mail server"
    public function forget(ForgetRequest $request)
    {
        return $this->user->forget($request->all());
    }

    public function putUserMobile(UpdateUserMobileRequest $request)
    {
        return $this->user->putMobile($request->all());
    }

    public function verifyUser(VerifyUserRequest $request)
    {
        return $this->user->verifyUser($request->all());
    }

    public function update(UpdateUserRequest $request)
    {
        return $this->user->update($request->all());
    }

    //logout
    public function logout(Request $request)
    {
        return $this->user->logout();
    }

    public function resendCode(ResendCodeRequest $request)
    {
        return $this->user->resendCode($request->all());
    }

    public function postSports(PostSportRequest $request)
    {
        return $this->user->postUserSports($request->all());
    }
}
