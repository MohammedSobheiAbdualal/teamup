<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Language\CreateLanguageRequest;
use App\Http\Requests\Language\CreateUserLanguageRequest;
use App\Http\Requests\Language\DeleteLanguageRequest;
use App\Http\Requests\Language\DeleteUserLanguageRequest;
use App\Http\Requests\Language\GetLanguageRequest;
use App\Http\Requests\Language\UpdateLanguageRequest;
use App\Http\Requests\Language\UpdateUserLanguageRequest;
use App\Repositories\Eloquents\LanguageEloquent;
use Illuminate\Http\Request;

class LanguageController extends Controller
{
    //
    private $language;

    public function __construct(LanguageEloquent $languageEloquent)
    {
        parent::__construct();
        $this->language = $languageEloquent;
    }

    //view list of system's language
    public function getLanguages(Request $request)
    {
        return $this->language->getAll($request->all());
    }

    //create new system's language
    public function postLanguage(CreateLanguageRequest $request)
    {
        return $this->language->create($request->all());
    }
    //update system's language
    public function putLanguage(UpdateLanguageRequest $request, $id)
    {
        return $this->language->update($request->all(), $id);
    }

    //delete system's language
    public function deleteLanguage(DeleteLanguageRequest $request)
    {
        return $this->language->delete($request->get('lang_id'));
    }

    // UserLanguage

    //view list of user's language
    public function getUserLanguages()
    {
        return $this->language->getUserLanguages();
    }

    //create new user's language
    public function postUserLanguage(CreateUserLanguageRequest $request)
    {
        return $this->language->createUserLanguage($request->all());
    }

    //delete user's language
    public function deleteUserLanguage(DeleteUserLanguageRequest $request)
    {
        return $this->language->deleteUserLanguage($request->get('lang_id'));
    }
}
