<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Categorized\GetRequest;
use App\Repositories\Eloquents\CategorizeEloquent;
use Illuminate\Http\Request;

//test
class CategorizeController extends Controller
{
    protected $categorize;

    public function __construct(CategorizeEloquent $categorizeEloquent)
    {
        parent::__construct();
        $this->categorize = $categorizeEloquent;
    }

    function getCategorizes(GetRequest $request)
    {
        // TODO: Implement getAll() method.

        if ($request->get('type') == 'team')
            return $this->categorize->getTeamCategorizes($request->all());
        if ($request->get('type') == 'league')
            return $this->categorize->getLeagueCategorizes($request->all());

    }

}
