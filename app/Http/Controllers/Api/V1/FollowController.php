<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Follow\CreateRequest;
use App\Http\Requests\Api\Follow\GetFollowerRequest;
use App\Http\Requests\Api\Follow\GetFollowingRequest;
use App\Repositories\Eloquents\FollowEloquent;
use Illuminate\Http\Request;

class FollowController extends Controller
{
    //
    private $follow;

    public function __construct(FollowEloquent $followEloquent)
    {
        parent::__construct();
        $this->follow = $followEloquent;
    }

    public function createRemoveFollow(CreateRequest $request)
    {
        return $this->follow->createRemoveFollow($request->all());

    }

    public function getFollowings(GetFollowingRequest $request)
    {
        return $this->follow->getFollowings($request->all());

    }

    public function getFollowers(GetFollowerRequest $request)
    {
        return $this->follow->getFollowers($request->all());

    }
}
