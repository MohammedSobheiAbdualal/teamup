<?php

namespace App\Http\Controllers\Api\V1;

use App\Repositories\Eloquents\SportEloquent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SportController extends Controller
{
    //
    private  $sport;

    public function __construct(SportEloquent  $sportEloquent)
    {
        parent::__construct();
        $this->sport =  $sportEloquent;
    }

    //get system's setting by title
    public function getSport($title)
    {
        return $this->sport->getByTitle($title);
    }

    //get all system's settings
    public function getSports()
    {
        return $this->sport->getAll([]);
    }
}
