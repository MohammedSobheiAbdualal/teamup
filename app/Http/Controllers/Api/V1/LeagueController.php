<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\League\CreateUpdateRequest;
use App\Http\Requests\Api\League\GetAllLeaguesRequest;
use App\Http\Requests\Api\League\GetRequest;
use App\Http\Requests\Api\League\GetSavedLocationRequest;
use App\Http\Requests\Api\League\GetTeamsRequest;
use App\Repositories\Eloquents\AwardEloquent;
use App\Repositories\Eloquents\FacilityGroundEloquent;
use App\Repositories\Eloquents\FacilitySizeEloquent;
use App\Repositories\Eloquents\LeagueEloquent;
use App\Repositories\Eloquents\PaymentMethodEloquent;
use App\Repositories\Eloquents\SettingEloquent;
use App\Repositories\Eloquents\WinnerEloquent;
use Illuminate\Http\Request;

class LeagueController extends Controller
{
    //
    private $league, $facility_size, $facility_ground, $payment_method, $award, $winner, $setting;

    public function __construct(LeagueEloquent $leagueEloquent,
                                FacilitySizeEloquent $facilitySizeEloquent,
                                FacilityGroundEloquent $facilityGroundEloquent,
                                PaymentMethodEloquent $paymentMethodEloquent,
                                AwardEloquent $awardEloquent,
                                WinnerEloquent $winnerEloquent,
                                SettingEloquent $settingEloquent)
    {
        parent::__construct();

        $this->league = $leagueEloquent;
        $this->payment_method = $paymentMethodEloquent;
        $this->facility_size = $facilitySizeEloquent;
        $this->facility_ground = $facilityGroundEloquent;
        $this->award = $awardEloquent;
        $this->winner = $winnerEloquent;
        $this->setting = $settingEloquent;
    }

    public
    function getProperties()
    {

        $payment_methods = $this->payment_method->getAll([]);
        $facility_sizes = $this->facility_size->getAll([]);
        $facility_grounds = $this->facility_ground->getAll([]);
        $awards = $this->award->getAll([]);
        $winners = $this->winner->getAll([]);
        $settings = $this->setting->getSettings([]);

        $data = [
            'payment_methods' => $payment_methods,
            'facility_sizes' => $facility_sizes,
            'facility_grounds' => $facility_grounds,
            'awards' => $awards,
            'winners' => $winners,
            'settings' => $settings,
        ];
        return response_api(true, 200, null, $data);
    }

    public
    function createUpdateLeague(CreateUpdateRequest $request)
    {

        if ($request->has('league_id'))
            return $this->league->update(\request()->all(), $request->get('league_id'));
        return $this->league->create(\request()->all());
    }

    public function getLeague($id)
    {
        return $this->league->getById($id);
    }

    public function getLeagues(GetRequest $request)
    {
        return $this->league->getAll($request->all());
    }
    public function getAllLeagues(GetAllLeaguesRequest $request)
    {
        return $this->league->getAllLeagues($request->all());
    }


    public function getSavedPublishLeagues(GetRequest $request)
    {
        return $this->league->getSavedPublishLeagues($request->all());
    }
    public function deleteLeague($id)
    {
        return $this->league->delete($id);
    }

    public function getSavedLocations(GetSavedLocationRequest $request)
    {
        return $this->league->getSavedLocations($request->all());
    }

    public function getTeamsInLeague(GetTeamsRequest $request)
    {
        return $this->league->getTeamsInLeague($request->all());

    }
}
