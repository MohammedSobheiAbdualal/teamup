<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\CelebritiesGuest\CreateRequest;
use App\Http\Requests\Api\CelebritiesGuest\GetRequest;
use App\Repositories\Eloquents\CelebritiesGuestEloquent;
use Illuminate\Http\Request;

class CelebritiesGuestController extends Controller
{
    //
    private $celebrities_guest;

    public function __construct(CelebritiesGuestEloquent $celebritiesGuestEloquent)
    {
        parent::__construct();
        $this->celebrities_guest = $celebritiesGuestEloquent;
    }

    public function getCelebritiesGuests(GetRequest $request)
    {
        return $this->celebrities_guest->getAll($request->all());
    }

    public function createUpdateCelebritiesGuest(CreateRequest $request)
    {
        if ($request->has('celebrities_guest_id'))
            return $this->celebrities_guest->update(request()->all(), request()->get('celebrities_guest_id'));
        return $this->celebrities_guest->create(request()->all());
    }

    public function deleteCelebritiesGuest($id)
    {
        return $this->celebrities_guest->delete($id);
    }
}
