<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Favorite\CreateRequest;
use App\Repositories\Eloquents\FavoriteEloquent;
use Illuminate\Http\Request;

class FavoriteController extends Controller
{
    //
    private $favorite;

    public function __construct(FavoriteEloquent $favoriteEloquent)
    {
        parent::__construct();
        $this->favorite = $favoriteEloquent;
    }

    public function createRemoveFavorite(CreateRequest $request)
    {
        return $this->favorite->createRemoveFavorite($request->all());
    }
}
