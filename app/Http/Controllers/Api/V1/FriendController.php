<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Friend\CreateRequest;
use App\Http\Requests\Api\Friend\GetRequest;
use App\Repositories\Eloquents\FriendEloquent;
use Illuminate\Http\Request;

class FriendController extends Controller
{
    //
    private $friend;

    public function __construct(FriendEloquent $friendEloquent)
    {
        parent::__construct();
        $this->friend = $friendEloquent;
    }

    public function createRemoveFriend(CreateRequest $request)
    {
        return $this->friend->createRemoveFriend($request->all());

    }

    public function getFriends(GetRequest $request)
    {
        return $this->friend->getFriends($request->all());
    }
}
