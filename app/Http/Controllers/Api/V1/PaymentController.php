<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Repositories\Eloquents\PaymentMethodEloquent;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    //

    private $payment_method;

    public function __construct(PaymentMethodEloquent $paymentMethodEloquent)
    {
        parent::__construct();
        $this->payment_method = $paymentMethodEloquent;
    }

    public function getPaymentMethods()
    {
        return $this->payment_method->getAll([]);
    }

}
