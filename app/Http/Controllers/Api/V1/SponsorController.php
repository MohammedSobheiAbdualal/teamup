<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Sponsor\CreateRequest;
use App\Http\Requests\Api\Sponsor\GetRequest;
use App\Http\Requests\Api\Sponsor\UpdateRequest;
use App\Models\Sponsor;
use App\Repositories\Eloquents\SponsorEloquent;
use Illuminate\Http\Request;

class SponsorController extends Controller
{
    //
    private $sponsor;

    public function __construct(SponsorEloquent $sponsorEloquent)
    {
        parent::__construct();
        $this->sponsor = $sponsorEloquent;
    }

    public function getSponsors(GetRequest $request)
    {
        return $this->sponsor->getAll($request->all());
    }

    public function createUpdateSponsor(CreateRequest $request)
    {
        if ($request->has('sponsor_id'))
            return $this->sponsor->update(request()->all(), request()->get('sponsor_id'));
        return $this->sponsor->create(request()->all());
    }

//    public function putSponsor(UpdateRequest $request, $id)
//    {
//        return $this->sponsor->update($request->all(), $id);
//    }

    public function deleteSponsor($id)
    {
        return $this->sponsor->delete($id);
    }
}
