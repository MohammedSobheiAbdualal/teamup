<?php

namespace App\Http\Controllers\Api\V1;

use App\Repositories\Eloquents\InstructionEloquent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InstructionController extends Controller
{
    //
    private $instruction;

    public function __construct(InstructionEloquent  $instructionEloquent)
    {
        parent::__construct();
        $this->instruction =  $instructionEloquent;
    }

    //get system's setting by title
    public function getInstruction($title)
    {
        return $this->instruction->getByTitle($title);
    }

    //get all system's settings
    public function getInstructions()
    {
        return $this->instruction->getAll([]);
    }
}
