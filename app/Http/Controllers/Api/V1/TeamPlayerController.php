<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\TeamPlayer\ChangePlayerStatusRequest;
use App\Http\Requests\Api\TeamPlayer\ChangeStatusRequest;
use App\Http\Requests\Api\TeamPlayer\CreateRequest;
use App\Http\Requests\Api\TeamPlayer\DeletePlayerTeamRequest;
use App\Http\Requests\Api\TeamPlayer\GetCategoryPlayersRequest;
use App\Http\Requests\Api\TeamPlayer\GetRequest;
use App\Http\Requests\Api\TeamPlayer\SetInjuredRequest;
use App\Http\Requests\Api\TeamPlayer\UpdateTeamPlayerRequest;
use App\Repositories\Eloquents\TeamPlayerEloquent;
use Illuminate\Http\Request;

class TeamPlayerController extends Controller
{
    //
    private $teamPlayer;

    public function __construct(TeamPlayerEloquent $teamPlayerEloquent)
    {
        parent::__construct();
        $this->teamPlayer = $teamPlayerEloquent;
    }

    public function postInvitation(CreateRequest $request)
    {
        return $this->teamPlayer->create($request->all());
    }

    public function changeStatus(ChangeStatusRequest $request)
    {
        // unfinished yet
        if ($request->get('status') == 'rejected' || $request->get('status') == 'canceled') {
            return $this->teamPlayer->delete($request->get('team_id'));
        }
        return $this->teamPlayer->changeStatus($request->all());
    }

    public function changePlayerStatus(ChangePlayerStatusRequest $request)
    {
        return $this->teamPlayer->changePlayerStatus($request->all());
    }

    public function getPlayers(GetRequest $request)
    {
        return $this->teamPlayer->getPlayers($request->all());
    }

    public function getCategoryPlayers(GetCategoryPlayersRequest $request)
    {
        return $this->teamPlayer->getCategoryPlayers($request->all());
    }

    public function getTeamCategoryPlayers(GetCategoryPlayersRequest $request)
    {
        return $this->teamPlayer->getTeamCategoryPlayers($request->all());
    }

    public function updateTeamPlayer(UpdateTeamPlayerRequest $request)
    {
        return $this->teamPlayer->updateTeamPlayer(\request()->all());
    }

    public function setInjured(SetInjuredRequest $request)
    {
        return $this->teamPlayer->setInjured(\request()->all());
    }


    public function deletePlayerTeam(DeletePlayerTeamRequest $request)
    {
        return $this->teamPlayer->deletePlayerTeam(\request()->all());
    }

}
