<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LeagueTeam\ChangeStatusRequest;
use App\Http\Requests\Api\LeagueTeam\CreateRequest;
use App\Http\Requests\Api\LeagueTeam\GetLeaguePlayersCategoryTeamRequest;
use App\Http\Requests\Api\LeagueTeam\GetLeaguePlayersTeamRequest;
use App\Repositories\Eloquents\LeagueTeamEloquent;
use Illuminate\Http\Request;

class LeagueTeamController extends Controller
{
    //
    private $leagueTeam;

    public function __construct(LeagueTeamEloquent $leagueTeamEloquent)
    {
        parent::__construct();
        $this->leagueTeam = $leagueTeamEloquent;
    }

    public function postInvitation(CreateRequest $request)
    {
        return $this->leagueTeam->create($request->all());
    }

    public function changeStatus(ChangeStatusRequest $request)
    {
        // unfinished yet
        if ($request->get('status') == 'removed') {
//        if ($request->get('status') == 'rejected' || $request->get('status') == 'removed' || $request->get('status') == 'withdraw') {
            return $this->leagueTeam->delete($request->all());
        }
        return $this->leagueTeam->changeStatus($request->all());
    }

    public function getLeagueCategoryPlayers(Request $request)
    {
        return $this->leagueTeam->getLeagueCategoryPlayers($request->all());
    }

    public function getLeaguePlayersCategoryTeam(GetLeaguePlayersCategoryTeamRequest $request)
    {
        return $this->leagueTeam->getLeaguePlayersCategoryTeam($request->all());
    }


    public function getLeaguePlayersTeam(GetLeaguePlayersTeamRequest $request)
    {
        return $this->leagueTeam->getLeaguePlayersTeam($request->all());
    }


}
