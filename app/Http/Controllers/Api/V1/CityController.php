<?php

namespace App\Http\Controllers\Api\V1;

use App\Repositories\Eloquents\CityEloquent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
    //
    private  $city;

    public function __construct(CityEloquent  $cityEloquent)
    {
        parent::__construct();
        $this->city =  $cityEloquent;
    }

    //get system's setting by title
    public function getCity($title)
    {
        return $this->city->getByTitle($title);
    }

    //get all system's settings
    public function getCities()
    {
        return $this->city->getAll([]);
    }
}
