<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param \Illuminate\Http\Request $request
     * @return string
     */
    protected function redirectTo($request)
    {

        
//        if ($request->segment(1) == 'api' && !auth()->user()->is_active) {
//            return response_api(false, 401, null, []);
//        }
        if (!$request->expectsJson()) {
            return route(admin_vw() . '.login');
        }
    }
}
