<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Facade;
use PhpParser\Node\Expr\Cast\Object_;

class Categorize extends Model
{
    protected $total_records;
    protected $category_type;
    protected $type;
    protected $list;

    /**
     * Categorize constructor.
     * @param $total_records
     * @param $category_type
     * @param $type
     * @param $list
     */
    public function __construct($total_records, $category_type, $type, $list)
    {
        $this->total_records = $total_records;
        $this->category_type = $category_type;
        $this->type = $type;
        $this->list = $list;
    }
}