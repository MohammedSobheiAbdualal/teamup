<?php
/**
 * Created by PhpStorm.
 * UserRequest: mohammedsobhei
 * Date: 5/2/18
 * Time: 11:43 PM
 */

namespace App\Repositories\Eloquents;

use App\Repositories\Interfaces\Repository;
use App\Models\City;
use App\Models\User;
use App\Models\Team;
use Excel;

class CityEloquent implements Repository
{

    private $model;

    public function __construct(City $model)
    {
        $this->model = $model;

    }

    // for cpanel
    function anyData()
    {
        $cities = $this->model->orderByDesc('id');
        return datatables()->of($cities)
            ->filter(function ($query) {
            })
            ->addColumn('action', function ($city) {
                $teams = Team::where('city_id', $city->id)->first();
                $users = User::where('city_id', $city->id)->first();
                $delete = '';
                if (!$teams && !$users)
                    $delete = '<button type="button" class="btn btn-outline-danger btn-elevate btn-circle btn-icon btn-delete" title="Delete" data-id="' . $city->id . '"><i class="flaticon-delete"></i></button>';


                $edit = '<a href="' . route('cities.edit', $city->id) . '"  class="btn btn-outline-warning btn-elevate btn-circle btn-icon edit-city-mdl" title="Edit" ><i class="flaticon-edit"></i></a>';

                return $edit.$delete;
            })->addIndexColumn()
            ->rawColumns(['action'])->toJson();
    }

    function export()
    {


    }

    function getAll(array $attributes)
    {

        // TODO: Implement getAll() method.

        $cities = $this->model->get();
        if (request()->segment(1) == 'api') {
            return response_api(true, 200, null,  $cities );
        }
        return  $cities ;
    }

    function getById($id)
    {
        if (request()->segment(1) == 'api') {
            // TODO: Implement getById() method.
            $setting = $this->model->where('lang_id', config()->get('app.lang_id'))->find($id);
            if (isset($setting))
                return response_api(true, 200, null, $setting);
            return response_api(false, 422, trans('app.not_data_found'), []);
        }
        return $this->model->find($id);

    }

    function getByTitle($title)
    {
        // TODO: Implement getById() method.


    }


    function create(array $attributes)
    {
        // TODO: Implement create() method.

        $city=new City();
        $city->name_ar=$attributes['name_ar'];
        $city->name_en=$attributes['name_en'];
        $city->latitude=$attributes['lat'];
        $city->longitude=$attributes['lng'];
        if($city->save())
        return response_api(true, 200, trans('app.created'), $city);
    }

    function update(array $attributes, $id = null)
    {
        // TODO: Implement update() method.
        $city=$this->model->find($id);
        $city->name_ar=$attributes['name_ar'];
        $city->name_en=$attributes['name_en'];

        if ( $city->save()) {

            return response_api(true, 200, trans('app.updated'), $city);

        }
        return response_api(false, 422, trans('app.not_updated'));


    }

    function delete($id)
    {
        // TODO: Implement delete() method.
        $city = $this->model->find($id);
        if (isset($city) && $city->delete()) {
            return response_api(true, 200, trans('app.deleted'), []);
        }
        return response_api(false, 422, null, []);

    }
}
