<?php
/**
 * Created by PhpStorm.
 * UserRequest: mohammedsobhei
 * Date: 5/2/18
 * Time: 11:43 PM
 */

namespace App\Repositories\Eloquents;

use App\Models\Award;
use App\Repositories\Interfaces\Repository;

class AwardEloquent implements Repository
{

    private $model;

    public function __construct(Award $model)
    {
        $this->model = $model;

    }

    // for cpanel
    function anyData()
    {
        $awards = $this->model->orderByDesc('id');
        return datatables()->of($awards)
            ->filter(function ($query) {
            })
            ->addColumn('action', function ($award) {

                $edit = '<a href="' . route('awards.edit', $award->id).'"  class="btn btn-outline-warning btn-elevate btn-circle btn-icon edit-award-mdl" title="Edit" ><i class="flaticon-edit"></i></a>';
                return $edit;
            })->addIndexColumn()
            ->rawColumns(['action'])->toJson();
    }

    function export()
    {


    }

    function getAll(array $attributes)
    {
        // TODO: Implement getAll() method.
        return $this->model->all();
    }

    function getById($id)
    {
        return $this->model->find($id);
    }

    function create(array $attributes)
    {
        // TODO: Implement create() method.

        $award = new Award();
        $award->title_ar = $attributes['title_ar'];
        $award->title_en = $attributes['title_en'];
        if ($award->save())
            return response_api(true, 200, trans('app.created'), $award);
        return response_api(false, 422, null, []);

    }

    function update(array $attributes, $id = null)
    {
        // TODO: Implement update() method.

        $award = $this->model->find($id);
        $award->title_ar = $attributes['title_ar'];
        $award->title_en = $attributes['title_en'];
        if ($award->save())
            return response_api(true, 200, trans('app.updated'), $award);
        return response_api(false, 422, null, []);

    }

    function delete($id)
    {
        // TODO: Implement delete() method.
        $award = $this->model->find($id);
        if (isset($award) && $award->delete()) {
            return response_api(true, 200, trans('app.deleted'), $award);
        }
        return response_api(false, 422, null, []);

    }
}
