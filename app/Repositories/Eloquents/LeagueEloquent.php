<?php
/**
 * Created by PhpStorm.
 * UserRequest: mohammedsobhei
 * Date: 5/2/18
 * Time: 11:43 PM
 */

namespace App\Repositories\Eloquents;

use App\Models\City;
use App\Models\Facility;
use App\Models\FacilityImage;
use App\Models\Favorite;
use App\Models\League;
use App\Models\LeagueAward;
use App\Models\LeagueCelebritiesGuest;
use App\Models\LeagueCondition;
use App\Models\LeagueLocation;
use App\Models\LeagueParticipant;
use App\Models\LeagueSponsor;
use App\Models\LeagueTeam;
use App\Models\LeagueWinner;
use App\Models\Team;
use App\Models\TeamPlayer;
use App\Models\User;
use App\Repositories\Interfaces\Repository;
use App\Repositories\Uploader;
use Carbon\Carbon;

class LeagueEloquent extends Uploader implements Repository
{

    private $model;

    public function __construct(League $model)
    {
        $this->model = $model;
    }

    // for cpanel
    function anyData()
    {
        $leagues = $this->model->orderByDesc('created_at');
        return datatables()->of($leagues)
            ->filter(function ($query) {
                if (request()->filled('league_name')) {
                    $query->where('name', 'like', '%' . request()->get('league_name') . '%');
                }
                if (request()->filled('owner_name')) {
                    $owner = request()->get('owner_name');

                    $query->whereHas('User', function ($q) use ($owner) {
                        $q->whereRaw("CONCAT(first_name,' ',last_name) like ?", ["%{$owner}%"]);
                    });
                }
                if (request()->filled('sport_id') && request()->get('sport_id') != '') {

                    $query->where('sport_id', request()->get('sport_id'));
                }
                if (request()->filled('is_active')) {
                    $query->where('is_active', request()->get('is_active'));
                }

                if (request()->filled('type')) {
                    $query->where('type', request()->get('type'));
                }

                if (request()->filled('status')) {
                    $query->where('status', request()->get('status'));
                }

                if (request()->filled('start_from') && request()->filled('start_to')) {
                    $query->whereBetween('start_date', [request()->get('start_from'), request()->get('start_to')]);
                }

            })
            ->editColumn('logo100', function ($league) {
                return $league->logo ? '<img src="' . $league->logo100 . '" alt="Empty Image" class="img-circle">' : '';
            })
            ->editColumn('sport', function ($league) {
                return $league->sport->name_en;
            })
            ->addColumn('user', function ($league) {
                return $league->user->first_name . ' ' . $league->user->last_name;
            })
            ->editColumn('is_active', function ($league) {

                return ($league->is_active == 1) ? '<span class="m-badge m-badge--success m-badge--wide">active</span>' : '<span class="m-badge m-badge--danger m-badge--wide">inactive</span>';
            })->addColumn('action', function ($league) {
                $checked = '';
                if ($league->is_active)
                    $checked = 'checked="checked"';
                $activate = '<span class="m-switch m-switch m-switch--outline m-switch--icon m-switch--danger m-switch--danger" style="margin-left:3px;vertical-align: middle;"><label><input type="checkbox"' . $checked . 'name="is_active" class="is_active" data-id="' . $league->id . '"><span></span></label></span>';

                return '<a href="' . url(admin_league_url() . '/' . $league->id) . '" class="btn btn-primary m-btn m-btn--icon m-btn--icon-only m-btn--pill" title="Team details"><i class="fa flaticon-medical"></i></a>' . $activate;
            })->addIndexColumn()
            ->rawColumns(['is_active', 'action', 'logo100'])->toJson();
    }

    function export()
    {

    }


    function getTeamsInLeague(array $attributes)
    {

        $page_size = isset($attributes['page_size']) ? $attributes['page_size'] : max_pagination();
        $page_number = isset($attributes['page_number']) ? $attributes['page_number'] : 1;
        $teams = LeagueTeam::where('status', 'confirmed')->where('league_id', $attributes['league_id'])->pluck('team_id')->unique();

        // all teams user id is owner and published or user id is player in that teams
//        $teams_id = TeamPlayer::where('user_id', $user_id)->where('status', 'accepted')->pluck('team_id');
        $collection = Team::whereIn('id', $teams);
        $count = $collection->count();
        $page_count = page_count($count, $page_size);
        $page_number = $page_number - 1;
        $page_number = $page_number > $page_count ? $page_number = $page_count - 1 : $page_number;

        $object = $collection->take($page_size)->skip((int)$page_number * $page_size)->get();
        if (request()->segment(1) == 'api' || request()->ajax()) {                                                                                                                                                                                     //auth()->user()->invites_num     //auth()->user()->requests_num //auth()->user()->my_teams_num
            return response_api(true, 200, null, $object, $page_count, $page_number, $count);
        }
    }

    function getSavedPublishLeagues(array $attributes)
    {

        $page_size = isset($attributes['page_size']) ? $attributes['page_size'] : max_pagination();
        $page_number = isset($attributes['page_number']) ? $attributes['page_number'] : 1;
        $collection = $this->model->where('is_active', 1);

        $user_id = (isset($attributes['user_id'])) ? $attributes['user_id'] : auth()->user()->id;

        // all teams user id is owner and published or user id is player in that teams
//        $teams_id = TeamPlayer::where('user_id', $user_id)->where('status', 'accepted')->pluck('team_id');

        $collection = $collection->where('status', 'published')->where(function ($query) use ($user_id) { //$teams_id
            $query->where('user_id', $user_id);//->orWhereIn('id', $teams_id);
        });

        if (isset($attributes['type'])) {
            //    upcoming,ongoing,ended
            if ($attributes['type'] == 'upcoming') {
                $collection = $collection->whereDate('start_date', '>', Carbon::now()->format('Y-m-d'));
            }
            if ($attributes['type'] == 'ongoing') {

                $collection = $collection->whereDate('start_date', '<=', Carbon::now()->format('Y-m-d'))->whereDate('end_date', '>=', Carbon::now()->format('Y-m-d'));
            }

            if ($attributes['type'] == 'ended') {
                $collection = $collection->whereDate('end_date', '<', Carbon::now()->format('Y-m-d'));
            }
        }

        $count = $collection->count();
        $page_count = page_count($count, $page_size);
        $page_number = $page_number - 1;
        $page_number = $page_number > $page_count ? $page_number = $page_count - 1 : $page_number;

        $published_leagues = $collection->take($page_size)->skip((int)$page_number * $page_size)->get();

        $saved_leagues = $this->model->where('status', 'saved')->where('user_id', auth()->user()->id); //where('is_active', 1)->

        /*
        // get auth saved leagues
        if (isset($attributes['type'])) {
            //    upcoming,ongoing,ended
            if ($attributes['type'] == 'upcoming') {
                $saved_leagues = $saved_leagues->whereDate('start_date', '>', Carbon::now()->format('Y-m-d'));
            }
            if ($attributes['type'] == 'ongoing') {

                $saved_leagues = $saved_leagues->whereDate('start_date', '<=', Carbon::now()->format('Y-m-d'))->whereDate('end_date', '>=', Carbon::now()->format('Y-m-d'));
            }

            if ($attributes['type'] == 'ended') {
                $saved_leagues = $saved_leagues->whereDate('end_date', '<', Carbon::now()->format('Y-m-d'));
            }
        }*/

        $saved_leagues = $saved_leagues->get();
        if (request()->segment(1) == 'api' || request()->ajax()) {                                                                                                                                                                                     //auth()->user()->invites_num     //auth()->user()->requests_num //auth()->user()->my_teams_num
            return response_api(true, 200, null, ['published_leagues' => ['data' => $published_leagues, 'total_pages' => $page_count, 'current_page' => $page_number + 1, 'total_records' => $count], 'saved_leagues' => $saved_leagues, 'invites_num' => 0, 'requests_num' => 0, 'my_teams_num' => 0]);
        }
    }


    function getSavedLocations(array $attributes)
    {

        $page_size = isset($attributes['page_size']) ? $attributes['page_size'] : max_pagination();
        $page_number = isset($attributes['page_number']) ? $attributes['page_number'] : 1;
//        $user_id = (isset($attributes['user_id'])) ? $attributes['user_id'] : ((auth()->check()) ? auth()->user()->id : null);
        $collection = new LeagueLocation();


        if (isset($attributes['search'])) {
            $city_ids = City::where('name_ar', 'LIKE', '%' . $attributes['search'] . '%')->orWhere('name_en', 'LIKE', '%' . $attributes['search'] . '%')->pluck('id');
            $league_ids = $this->model->where('name', 'LIKE', '%' . $attributes['search'] . '%')->pluck('id');
            $collection = $collection->where('address', 'LIKE', '%' . $attributes['search'] . '%')->orWhereIn('city_id', $city_ids)->orWhereIn('league_id', $league_ids);
        }
        $count = $collection->count();
        $page_count = page_count($count, $page_size);
        $page_number = $page_number - 1;
        $page_number = $page_number > $page_count ? $page_number = $page_count - 1 : $page_number;

        $object = $collection->take($page_size)->skip((int)$page_number * $page_size)->get();

        if (request()->segment(1) == 'api' || request()->ajax()) {
            return response_api(true, 200, null, $object, $page_count, $page_number, $count);
        }
    }

    function getAll(array $attributes)
    {

        $page_size = isset($attributes['page_size']) ? $attributes['page_size'] : max_pagination();
        $page_number = isset($attributes['page_number']) ? $attributes['page_number'] : 1;
        $user_id = (isset($attributes['user_id'])) ? $attributes['user_id'] : null;//((auth()->check()) ? auth()->user()->id : null);

//
        $collection = $this->model->where('status', 'published')->where('is_active', 1);

        if (isset($attributes['team_id']) && auth()->check())
            $my_team = Team::where('user_id', auth()->user()->id)->find($attributes['team_id']);

        // in team det
        if (isset($attributes['type'])) {

            if (isset($attributes['team_id']) && !isset($my_team)) {
                $leagues_id = LeagueTeam::where('team_id', $attributes['team_id'])->where('status', 'confirmed')->pluck('league_id')->unique();
                $collection = $collection->whereIn('id', $leagues_id);
            } else
                if (isset($user_id)) {
                    $teams_id = TeamPlayer::where(function ($query) use ($user_id) {
                        $query->where('user_id', $user_id)->orWhere('owner_id', $user_id);
                    })->where('status', 'accepted')->pluck('team_id')->unique();
//                $teams_id = Team::where(function ($query) use ($teams_id, $user_id) {
//                    $query->where('user_id', $user_id)->orWhereIn('id', $teams_id);
//                })->pluck('team_id')->unique();

                    $leagues_id = LeagueTeam::whereIn('team_id', $teams_id)->where('status', 'confirmed')->pluck('league_id')->unique();
                    $collection = $collection->whereIn('id', $leagues_id);
                }

            //    upcoming,ongoing,ended
            if ($attributes['type'] == 'upcoming') {
                $collection = $collection->whereDate('start_date', '>', Carbon::now()->format('Y-m-d'));
            }
            if ($attributes['type'] == 'ongoing') {


                //    upcoming,ongoing,ended
                if ($attributes['type'] == 'upcoming') {
                    $collection = $collection->whereDate('start_date', '>', Carbon::now()->format('Y-m-d'));
                }
                if ($attributes['type'] == 'ongoing') {

                    $collection = $collection->whereDate('start_date', '<=', Carbon::now()->format('Y-m-d'))->whereDate('end_date', '>=', Carbon::now()->format('Y-m-d'));
                }

                if ($attributes['type'] == 'ended') {
                    $collection = $collection->whereDate('end_date', '<', Carbon::now()->format('Y-m-d'));
                }

                if (isset($my_team)) {
                    //  my_league,invite,request
                    if ($attributes['type'] == 'my_league') {
                        // all leagues user id is owner and confirmed or user id is player in that teams are confirmed in league
//                    $teams_id = TeamPlayer::where('user_id', $user_id)->where('status', 'accepted')->pluck('team_id');
                        $leagues_id = LeagueTeam::where('user_id', auth()->user()->id)->where('team_id', $attributes['team_id'])->where('is_confirmed', 1)->where(function ($query) {
                            $query->where('status', 'confirmed')->orWhere('status', 'withdraw');
                        })->pluck('league_id');
                        $leagues_id = LeagueTeam::where('user_id', auth()->user()->id)->where('team_id', $attributes['team_id'])->where('status', 'confirmed')->pluck('league_id');
                        $collection = $collection->whereIn('id', $leagues_id);
//                    $collection = $collection->where(function ($query) use ($attributes, $leagues_id, $user_id) {
//                        $query->where('user_id', $user_id)->orWhereIn('id', $leagues_id);
//                    });


                    } elseif ($attributes['type'] == 'invite' && auth()->check()) {

                        $leagues_id = LeagueTeam::where('team_id', $attributes['team_id'])->where('is_confirmed', 0)->where(function ($query) { //where('user_id', auth()->user()->id)->
                            $query->where('status', 'invited')->orWhere('status', 'accepted')->orWhere('status', 'rejected')->orWhere('status', 'withdraw');
                        })->where('type', 'league')->pluck('league_id');
                        $leagues_id = LeagueTeam::where('user_id', auth()->user()->id)->where('team_id', $attributes['team_id'])->where('status', 'invited')->where('type', 'league')->pluck('league_id');
                        $collection = $collection->whereIn('id', $leagues_id);


                    } elseif ($attributes['type'] == 'request' && auth()->check()) {

                        $leagues_id = LeagueTeam::where('team_id', $attributes['team_id'])->where('is_confirmed', 0)->where(function ($query) {
                            $query->where('status', 'joined')->orWhere('status', 'rejected')->orWhere('status', 'accepted')->orWhere('status', 'withdraw');
                        })->where('type', 'team')->pluck('league_id'); //where('user_id', auth()->user()->id)->

                        $leagues_id = LeagueTeam::where('user_id', auth()->user()->id)->where('team_id', $attributes['team_id'])->where('status', 'joined')->where('type', 'team')->pluck('league_id');
                        $collection = $collection->whereIn('id', $leagues_id);

                    }
                }
            }

            $count = $collection->count();
            $page_count = page_count($count, $page_size);
            $page_number = $page_number - 1;
            $page_number = $page_number > $page_count ? $page_number = $page_count - 1 : $page_number;

            $object = $collection->take($page_size)->skip((int)$page_number * $page_size)->get();

            if (request()->segment(1) == 'api' || request()->ajax()) {
                return response_api(true, 200, null, $object, $page_count, $page_number, $count);
            }
        }
    }

    function getAllLeagues(array $attributes)
    {

        $page_size = isset($attributes['page_size']) ? $attributes['page_size'] : max_pagination();
        $page_number = isset($attributes['page_number']) ? $attributes['page_number'] : 1;
        $collection = $this->model->where('is_active', 1)->where('status', 'published');
        $leagues_id = [];
//        if (isset($attributes['search'])) {
//            $collection = $collection->where('name', 'LIKE', '%' . $attributes['search'] . '%');
//        }
//        if (isset($attributes['league_id'])) {
//            $league = League::find($attributes['league_id']);
//            $teams_id = LeagueTeam::where('status', 'accepted')->where('league_id', $attributes['league_id'])->pluck('team_id')->toArray();
//            $teams_id = Team::whereNotIn('id', $teams_id)->where('sport_id', $league->sport_id)->pluck('id')->toArray();
//            $collection = $collection->whereIn('id', $teams_id);
//        }

        if (isset($attributes['sport_id'])) {
            $collection = $this->model->where('sport_id', $attributes['sport_id']);
        }

        if (isset($attributes['type']) && $attributes['type'] == 'favorite' && auth()->check()) {

            $leagues_id = Favorite::where('type', 'league')->where('user_id', auth()->user()->id)->orderByDesc('created_at')->pluck('favorite_id')->toArray();
            if (count($leagues_id) > 0) {
                $ids_leagues = implode(',', $leagues_id);
                $collection = $collection->whereIn('id', $leagues_id)->orderByRaw("FIELD(id, $ids_leagues)");
            } else {
                $collection = $collection->whereIn('id', $leagues_id);
            }

        } else {
            //new
            $collection = $collection->orderByDesc('created_at');
        }

        $count = $collection->count();

        if (isset($attributes['is_all']) && $attributes['is_all']) {
            return response_api(true, 200, null, $collection->get(), 1, 0, $count);
        }
        $count = $collection->count();
        $page_count = page_count($count, $page_size);
        $page_number = $page_number - 1;
        $page_number = $page_number > $page_count ? $page_number = $page_count - 1 : $page_number;

        $object = $collection->take($page_size)->skip((int)$page_number * $page_size)->get();

        if (request()->segment(1) == 'api' || request()->ajax()) {
            return response_api(true, 200, null, $object, $page_count, $page_number, $count);
        }
    }

    function getById($id)
    {
        if (request()->segment(1) == 'api') {
            $league = $this->model->find($id);
            if (isset($league))
                return response_api(true, 200, null, $league);
            return response_api(false, 422, trans('app.not_data_found'), []);
        }

        return $this->model->findOrFail($id);
    }

    function create(array $attributes)
    {

        // TODO: Implement create() method.
        // check if player has permission to add league
        if (!auth()->user()->has_league)
            return response_api(false, 422, trans('app.no_permission'), []);

        $league = new League();

        $league->name = $attributes['name'];
        $league->user_id = auth()->user()->id;
        $league->sport_id = $attributes['sport_id'];
        $league->type = $attributes['type'];
        if ($attributes['type'] == 'specify')
            $league->team_num = $attributes['team_num'];
        $league->start_date = $attributes['start_date'];
        $league->end_date = $attributes['end_date'];
        $league->deadline_date = $attributes['deadline_date'];
        $league->subscription_type = $attributes['subscription_type'];
        if ($attributes['subscription_type'] == 'paid') {
            $league->payment_method_id = $attributes['payment_method_id'];
            $league->fees = $attributes['fees'];
            $league->frcd = $attributes['frcd'];

        }
        if (isset($attributes['status']))
            $league->status = $attributes['status'];
        $league->main_lineup = $attributes['main_lineup'];
        $league->bench = $attributes['bench'];
        if (isset($attributes['reserve_num']))
            $league->reserve_num = $attributes['reserve_num'];
        $league->half_duration = $attributes['half_duration'];
        $league->extra = $attributes['extra'];
        if ($attributes['extra'] == 'extra_halves_penalties')
            $league->extra_half_duration = $attributes['extra_half_duration'];
        $league->short_description = $attributes['short_description'];
        if (isset($attributes['league_description']))
            $league->league_description = $attributes['league_description'];
        if (isset($attributes['celebrities_guest_description']))
            $league->celebrities_guest_description = $attributes['celebrities_guest_description'];
        if (isset($attributes['general_note']))
            $league->general_note = $attributes['general_note'];
        if (isset($attributes['contact_number']))
            $league->contact_number = $attributes['contact_number'];

        if (isset($attributes['allow_call_me']))
            $league->allow_call_me = $attributes['allow_call_me'];
        if (isset($attributes['has_restricted']))
            $league->has_restricted = $attributes['has_restricted'];
        if (isset($attributes['has_service']))
            $league->has_service = $attributes['has_service'];
        if (isset($attributes['has_rule']))
            $league->has_rule = $attributes['has_rule'];


        if ($league->save()) {
            if (isset($attributes['logo']) || isset($attributes['bg_image'])) {
                if (isset($attributes['logo']))
                    $league->logo = $this->storeImageThumb('leagues', $league->id, $attributes['logo']);

                sleep(1);

                if (isset($attributes['bg_image']))
                    $league->bg_image = $this->storeImageThumb('leagues', $league->id, $attributes['bg_image']);
                $league->save();
            }
            if (isset($attributes['location_'])) {
                $location = $attributes['location_'][0];
                $league_location = new LeagueLocation();
                $league_location->league_id = $league->id;
                $league_location->city_id = $location->city_id;
                $league_location->address = $location->address;
                $league_location->latitude = $location->latitude;
                $league_location->longitude = $location->longitude;
                $league_location->save();
            }

            if (isset($attributes['facility_'])) {
                $facility = $attributes['facility_'][0];
                $league_facility = new Facility();
                $league_facility->league_id = $league->id;
                $league_facility->name = $facility->name;
                $league_facility->size_id = $facility->size_id;
                $league_facility->ground_id = $facility->ground_id;
                if ($league_facility->save())
                    if (isset($attributes['facility_images']) && count($attributes['facility_images']) > 0)
                        foreach ($attributes['facility_images'] as $facility_image) {
                            sleep(1);
                            $league_facility_image = new FacilityImage();
                            $league_facility_image->facility_id = $league_facility->id;
                            if ($league_facility_image->save()) {
                                $league_facility_image->image = $this->storeImageThumb('facility', $league_facility->id, $facility_image);
                                $league_facility_image->save();
                            }
                        }
            }

            if (isset($attributes['winners_']) && count($attributes['winners_']) > 0)
                foreach ($attributes['winners_'] as $winner) {

                    $league_winner = new LeagueWinner();
                    $league_winner->league_id = $league->id;
                    if (isset($winner->winner_id))
                        $league_winner->winner_id = $winner->winner_id;
                    if (isset($winner->other_winner))
                        $league_winner->other_winner = $winner->other_winner;

                    if ($league_winner->save()) {
                        foreach ($winner->awards as $award) {

                            $league_award = new LeagueAward();
                            $league_award->league_id = $league->id;
                            $league_award->league_winner_id = $league_winner->id;
                            if (isset($award->award_id))
                                $league_award->award_id = $award->award_id;
                            if (isset($award->other_award))
                                $league_award->other_award = $award->other_award;
                            if (isset($award->award_id) && isset($award->money_value) && $award->award_id == 1)
                                $league_award->money_value = $award->money_value;
                            $league_award->save();
                        }
                    }
                }

            if (isset($attributes['channels_']) && count($attributes['channels_']) > 0)
                foreach ($attributes['channels_'] as $channel) {
                    $league_channel = new LeagueParticipant();
                    $league_channel->league_id = $league->id;
                    $league_channel->user_id = $channel->user_id;
                    $league_channel->type = 'channel';
                    if (isset($channel->description))
                        $league_channel->description = $channel->description;
                    $league_channel->save();
                }

            if (isset($attributes['organizers_']) && count($attributes['organizers_']) > 0)
                foreach ($attributes['organizers_'] as $organizer) {
                    $league_organizer = new LeagueParticipant();
                    $league_organizer->league_id = $league->id;
                    $league_organizer->user_id = $organizer->user_id;
                    $league_organizer->type = 'organizer';
                    if (isset($organizer->description))
                        $league_organizer->description = $organizer->description;
                    $league_organizer->save();
                }


            if (isset($attributes['committee_officials_']) && count($attributes['committee_officials_']) > 0)
                foreach ($attributes['committee_officials_'] as $committee_official) {
                    $league_committee_official = new LeagueParticipant();
                    $league_committee_official->league_id = $league->id;
                    $league_committee_official->user_id = $committee_official->user_id;
                    $league_committee_official->type = 'committee_official';
                    $league_committee_official->sub_type = $committee_official->sub_type;
                    if (isset($committee_official->description))
                        $league_committee_official->description = $committee_official->description;
                    $league_committee_official->save();
                }

            if (isset($attributes['celebrities_guests']) && count($attributes['celebrities_guests']) > 0) {
                foreach ($attributes['celebrities_guests'] as $celebrities_guest_id) {

                    $league_celebrities_guest = new LeagueCelebritiesGuest();
                    $league_celebrities_guest->league_id = $league->id;
                    $league_celebrities_guest->celebrities_guest_id = $celebrities_guest_id;
                    $league_celebrities_guest->save();

                }
            }

            if (isset($attributes['has_restricted']) && $attributes['has_restricted'])
                foreach ($attributes['restrictions'] as $restriction) {
                    $league_restriction = new LeagueCondition();
                    $league_restriction->league_id = $league->id;
                    $league_restriction->text = $restriction;
                    $league_restriction->type = 'restrict';
                    $league_restriction->save();
                }

            if (isset($attributes['has_service']) && $attributes['has_service'])
                foreach ($attributes['services'] as $service) {
                    $league_service = new LeagueCondition();
                    $league_service->league_id = $league->id;
                    $league_service->text = $service;
                    $league_service->type = 'service';
                    $league_service->save();
                }
            if (isset($attributes['has_rule']) && $attributes['has_rule'])
                foreach ($attributes['rules'] as $rule) {
                    $league_rule = new LeagueCondition();
                    $league_rule->league_id = $league->id;
                    $league_rule->text = $rule;
                    $league_rule->type = 'rule';
                    $league_rule->save();
                }

            if (isset($attributes['sponsors']) && count($attributes['sponsors']) > 0) {
                foreach ($attributes['sponsors'] as $sponsor_id) {
                    $league_sponsor = new LeagueSponsor();
                    $league_sponsor->league_id = $league->id;
                    $league_sponsor->sponsor_id = $sponsor_id;
                    $league_sponsor->save();
                }

                if (isset($attributes['main_sponsor_id'])) {
                    $main_sponsor = LeagueSponsor::where('league_id', $league->id)->where('sponsor_id', $attributes['main_sponsor_id'])->first();
                    if (isset($main_sponsor)) {
                        $main_sponsor->is_main = 1;
                        $main_sponsor->save();
                    }
                }
            }

            $league = $this->model->find($league->id);
            return response_api(true, 200, trans('app.created'), $league);

        }
        return response_api(false, 422, null, []);

    }

    function update(array $attributes, $id = null)
    {
        // TODO: Implement update() method.

        $league = $this->model->where('user_id', auth()->user()->id)->find($id);

        // check if player has permission to add league
        if (!auth()->user()->has_league)
            return response_api(false, 422, trans('app.no_permission'), []);

        if (isset($league)) {
            if (isset($attributes['name']))
                $league->name = $attributes['name'];
            $league->user_id = auth()->user()->id;
            if (isset($attributes['sport_id']))
                $league->sport_id = $attributes['sport_id'];
            if (isset($attributes['type']))
                $league->type = $attributes['type'];
            if (isset($attributes['team_num']))
                $league->team_num = $attributes['team_num'];
            if (isset($attributes['start_date']))
                $league->start_date = $attributes['start_date'];
            if (isset($attributes['end_date']))
                $league->end_date = $attributes['end_date'];
            if (isset($attributes['deadline_date']))
                $league->deadline_date = $attributes['deadline_date'];
            if (isset($attributes['subscription_type']))
                $league->subscription_type = $attributes['subscription_type'];

            if (isset($attributes['subscription_type']) && $attributes['subscription_type'] == 'paid') {
                if (isset($attributes['payment_method_id']))
                    $league->payment_method_id = $attributes['payment_method_id'];
                if (isset($attributes['fees']))
                    $league->fees = $attributes['fees'];
                if (isset($attributes['frcd']))
                    $league->frcd = $attributes['frcd'];
            }
            if (isset($attributes['main_lineup']))
                $league->main_lineup = $attributes['main_lineup'];
            if (isset($attributes['bench']))
                $league->bench = $attributes['bench'];
            if (isset($attributes['reserve_num']))
                $league->reserve_num = $attributes['reserve_num'];
            if (isset($attributes['half_duration']))
                $league->half_duration = $attributes['half_duration'];
            if (isset($attributes['extra']))
                $league->extra = $attributes['extra'];
            if (isset($attributes['extra_half_duration']))
                $league->extra_half_duration = $attributes['extra_half_duration'];
            if (isset($attributes['short_description']))
                $league->short_description = $attributes['short_description'];
            if (isset($attributes['league_description']))
                $league->league_description = $attributes['league_description'];
            if (isset($attributes['celebrities_guest_description']))
                $league->celebrities_guest_description = $attributes['celebrities_guest_description'];
            if (isset($attributes['general_note']))
                $league->general_note = $attributes['general_note'];
            if (isset($attributes['contact_number']))
                $league->contact_number = $attributes['contact_number'];
            if (isset($attributes['allow_call_me']))
                $league->allow_call_me = $attributes['allow_call_me'];
            if (isset($attributes['has_restricted']))
                $league->has_restricted = $attributes['has_restricted'];
            if (isset($attributes['has_service']))
                $league->has_service = $attributes['has_service'];
            if (isset($attributes['has_rule']))
                $league->has_rule = $attributes['has_rule'];

            if (isset($attributes['status']))
                $league->status = $attributes['status'];

            if ($league->save()) {
                if (isset($attributes['logo']) || isset($attributes['bg_image'])) {
                    if (isset($attributes['logo']))
                        $league->logo = $this->storeImageThumb('leagues', $league->id, $attributes['logo']);

                    sleep(1);

                    if (isset($attributes['bg_image']))
                        $league->bg_image = $this->storeImageThumb('leagues', $league->id, $attributes['bg_image']);
                    $league->save();
                }

                if (isset($attributes['location_'])) {
                    $location = $attributes['location_'][0];

                    $league_location = LeagueLocation::where('league_id', $league->id)->first();
                    if (!isset($league_location))
                        $league_location = new LeagueLocation();

                    $league_location->league_id = $league->id;
                    $league_location->city_id = $location->city_id;
                    $league_location->address = $location->address;
                    $league_location->latitude = $location->latitude;
                    $league_location->longitude = $location->longitude;
                    $league_location->save();
                }

                if (isset($attributes['facility_'])) {
                    $facility = $attributes['facility_'][0];
                    $league_facility = Facility::where('league_id', $league->id)->first();
                    if (!isset($league_facility))
                        $league_facility = new Facility();


                    $league_facility->league_id = $league->id;
                    $league_facility->name = $facility->name;
                    $league_facility->size_id = $facility->size_id;
                    $league_facility->ground_id = $facility->ground_id;
                    if ($league_facility->save())
                        if (isset($attributes['facility_images']) && count($attributes['facility_images']) > 0) {

                            if (isset($attributes['old_facility_images'])) {
                                FacilityImage::where('facility_id', $league_facility->id)->whereNotIn('id', $attributes['old_facility_images'])->forceDelete();
                            } else {
                                FacilityImage::where('facility_id', $league_facility->id)->forceDelete();
                            }
                            foreach ($attributes['facility_images'] as $facility_image) {
                                sleep(1);
                                $league_facility_image = new FacilityImage();
                                $league_facility_image->facility_id = $league_facility->id;
                                if ($league_facility_image->save()) {
                                    $league_facility_image->image = $this->storeImageThumb('facility', $league_facility->id, $facility_image);
                                    $league_facility_image->save();
                                }
                            }
                        }
                }

                if (isset($attributes['winners_']) && count($attributes['winners_']) > 0) {
                    LeagueWinner::where('league_id', $league->id)->forceDelete();

                    foreach ($attributes['winners_'] as $winner) {

                        $league_winner = new LeagueWinner();
                        $league_winner->league_id = $league->id;
                        if (isset($winner->winner_id))
                            $league_winner->winner_id = $winner->winner_id;

                        if (isset($winner->other_winner))
                            $league_winner->other_winner = $winner->other_winner;

                        if ($league_winner->save()) {
                            foreach ($winner->awards as $award) {
                                $league_award = new LeagueAward();
                                $league_award->winner_id = $league_winner->id;
                                if (isset($award->award_id))
                                    $league_award->award_id = $award->award_id;
                                if (isset($award->other_award))
                                    $league_award->other_award = $award->other_award;
                                if (isset($award->money_value))
                                    $league_award->money_value = $award->money_value;
                                $league_award->save();
                            }
                        }
                    }
                }

                if (isset($attributes['awards_']) && count($attributes['awards_']) > 0) {
                    LeagueParticipant::where('league_id', $league->id)->forceDelete();
                    foreach ($attributes['awards_'] as $award) {
                        $league_award = new LeagueAward();
                        $league_award->league_id = $league->id;
                        if (isset($award->winner_id))
                            $league_award->winner_id = $award->winner_id;
                        if (isset($award->award_id))
                            $league_award->award_id = $award->award_id;

                        if (isset($winner->other_winner))
                            $league_winner->other_winner = $winner->other_winner;

                        if ($league_winner->save()) {
                            foreach ($winner->awards as $award) {
                                $league_award = new LeagueAward();

                                $league_award->winner_id = $league_winner->id;
                                $league_award->league_id = $league->id;
                                $league_award->league_winner_id = $league_winner->id;
                                if (isset($award->award_id))
                                    $league_award->award_id = $award->award_id;
                                if (isset($award->other_award))
                                    $league_award->other_award = $award->other_award;
                                if (isset($award->award_id) && isset($award->money_value) && $award->award_id == 1)
                                    $league_award->money_value = $award->money_value;
                                $league_award->save();
                            }
                        }
                    }
                }

                if (isset($attributes['channels_']) && count($attributes['channels_']) > 0) {
                    LeagueParticipant::where('league_id', $league->id)->where('type', 'channel')->forceDelete();

                    foreach ($attributes['channels_'] as $channel) {
                        $league_channel = new LeagueParticipant();
                        $league_channel->league_id = $league->id;
                        $league_channel->user_id = $channel->user_id;
                        $league_channel->type = 'channel';
                        if (isset($channel->description))
                            $league_channel->description = $channel->description;
                        $league_channel->save();
                    }
                }

                if (isset($attributes['organizers_']) && count($attributes['organizers_']) > 0) {
                    LeagueParticipant::where('league_id', $league->id)->where('type', 'organizer')->forceDelete();

                    foreach ($attributes['organizers_'] as $organizer) {
                        $league_organizer = new LeagueParticipant();
                        $league_organizer->league_id = $league->id;
                        $league_organizer->user_id = $organizer->user_id;
                        $league_organizer->type = 'organizer';
                        if (isset($organizer->description))
                            $league_organizer->description = $organizer->description;
                        $league_organizer->save();
                    }
                }


                if (isset($attributes['committee_officials_']) && count($attributes['committee_officials_']) > 0) {
                    LeagueParticipant::where('league_id', $league->id)->where('type', 'committee_official')->forceDelete();
                    foreach ($attributes['committee_officials_'] as $committee_official) {
                        $league_committee_official = new LeagueParticipant();
                        $league_committee_official->league_id = $league->id;
                        $league_committee_official->user_id = $committee_official->user_id;
                        $league_committee_official->type = 'committee_official';
                        $league_committee_official->sub_type = $committee_official->sub_type;
                        if (isset($committee_official->description))
                            $league_committee_official->description = $committee_official->description;
                        $league_committee_official->save();
                    }
                }

                if (isset($attributes['celebrities_guests']) && count($attributes['celebrities_guests']) > 0) {
                    LeagueCelebritiesGuest::where('league_id', $league->id)->forceDelete();
                    foreach ($attributes['celebrities_guests'] as $celebrities_guest_id) {

                        $league_celebrities_guest = new LeagueCelebritiesGuest();
                        $league_celebrities_guest->league_id = $league->id;
                        $league_celebrities_guest->celebrities_guest_id = $celebrities_guest_id;
                        $league_celebrities_guest->save();

                    }
                }

                if (isset($attributes['has_restricted']) && $attributes['has_restricted']) {
                    LeagueCondition::where('league_id', $league->id)->where('type', 'restrict')->forceDelete();

                    foreach ($attributes['restrictions'] as $restriction) {
                        $league_restriction = new LeagueCondition();
                        $league_restriction->league_id = $league->id;
                        $league_restriction->text = $restriction;
                        $league_restriction->type = 'restrict';
                        $league_restriction->save();
                    }
                }

                if (isset($attributes['has_service']) && $attributes['has_service']) {
                    LeagueCondition::where('league_id', $league->id)->where('type', 'service')->forceDelete();

                    foreach ($attributes['services'] as $service) {
                        $league_service = new LeagueCondition();
                        $league_service->league_id = $league->id;
                        $league_service->text = $service;
                        $league_service->type = 'service';
                        $league_service->save();
                    }
                }
                if (isset($attributes['has_rule']) && $attributes['has_rule']) {
                    LeagueCondition::where('league_id', $league->id)->where('type', 'rule')->forceDelete();

                    foreach ($attributes['rules'] as $rule) {
                        $league_rule = new LeagueCondition();
                        $league_rule->league_id = $league->id;
                        $league_rule->text = $rule;
                        $league_rule->type = 'rule';
                        $league_rule->save();
                    }
                }

                if (isset($attributes['sponsors']) && count($attributes['sponsors']) > 0) {

                    LeagueSponsor::where('league_id', $league->id)->forceDelete();
                    foreach ($attributes['sponsors'] as $sponsor_id) {
                        $league_sponsor = new LeagueSponsor();
                        $league_sponsor->league_id = $league->id;
                        $league_sponsor->sponsor_id = $sponsor_id;
                        $league_sponsor->save();
                    }

                    if (isset($attributes['main_sponsor_id'])) {
                        $main_sponsor = LeagueSponsor::where('league_id', $league->id)->where('sponsor_id', $attributes['main_sponsor_id'])->first();
                        if (isset($main_sponsor)) {
                            $main_sponsor->is_main = 1;
                            $main_sponsor->save();
                        }
                    }
                }

                $league = $this->model->find($league->id);

                return response_api(true, 200, trans('app.updated'), $league);
            }
        }

        return response_api(false, 422, null, []);

    }

    function delete($id)
    {
        // TODO: Implement delete() method.
        $league = $this->model->where('user_id', auth()->user()->id)->find($id);
        if (isset($league) && $league->delete()) {
            return response_api(true, 200, trans('app.deleted'), $league);
        }
        return response_api(false, 422, null, []);

    }

    function activation(array $attributes)
    {
        $league = $this->model->find($attributes['league_id']);
        if (isset($league)) {
            $league->is_active = $attributes['is_active'];

            if ($league->save()) {
                if (!$league->is_active) {

                    return response_api(true, 200);

                }
                return response_api(true, 200);
            }
        }
        return response_api(false, 422);
    }


}
