<?php
/**
 * Created by PhpStorm.
 * UserRequest: mohammedsobhei
 * Date: 5/2/18
 * Time: 11:43 PM
 */

namespace App\Repositories\Eloquents;

use App\Models\FacilityGround;
use App\Repositories\Interfaces\Repository;

class FacilityGroundEloquent implements Repository
{

    private $model;

    public function __construct(FacilityGround $model)
    {
        $this->model = $model;

    }

    // for cpanel
    function anyData()
    {

    }

    function export()
    {


    }

    function getAll(array $attributes)
    {

        // TODO: Implement getAll() method.

        return $this->model->all();
    }

    function getById($id)
    {
        return $this->model->find($id);
    }

    function create(array $attributes)
    {
        // TODO: Implement create() method.

        $facility_ground = new FacilityGround();
        $facility_ground->title_ar = $attributes['title_ar'];
        $facility_ground->title_en = $attributes['title_en'];
        if ($facility_ground->save())
            return response_api(true, 200, trans('app.created'), $facility_ground);
        return response_api(false, 422, null, []);

    }

    function update(array $attributes, $id = null)
    {
        // TODO: Implement update() method.

        $facility_ground = $this->model->find($id);
        $facility_ground->title_ar = $attributes['title_ar'];
        $facility_ground->title_en = $attributes['title_en'];
        if ($facility_ground->save())
            return response_api(true, 200, trans('app.updated'), $facility_ground);
        return response_api(false, 422, null, []);

    }

    function delete($id)
    {
        // TODO: Implement delete() method.
        $facility_ground = $this->model->find($id);
        if (isset($facility_ground) && $facility_ground->delete()) {
            return response_api(true, 200, trans('app.deleted'), $facility_ground);
        }
        return response_api(false, 422, null, []);

    }
}
