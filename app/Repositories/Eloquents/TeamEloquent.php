<?php
/**
 * Created by PhpStorm.
 * UserRequest: mohammedsobhei
 * Date: 5/2/18
 * Time: 11:43 PM
 */

namespace App\Repositories\Eloquents;

use App\Models\Favorite;
use App\Models\League;
use App\Models\LeagueTeam;
use App\Models\Sponsor;
use App\Models\SponsorSocial;
use App\Models\Team;
use App\Models\TeamPlayer;
use App\Models\TeamSponsor;
use App\Repositories\Interfaces\Repository;
use App\Repositories\Uploader;
use Excel;

class TeamEloquent extends Uploader implements Repository
{

    private $model;

    public function __construct(Team $model)
    {
        $this->model = $model;

    }

    // for cpanel
    function anyData()
    {

        $teams = $this->model->orderByDesc('created_at');
        return datatables()->of($teams)
            ->filter(function ($query) {
                if (request()->filled('team_name')) {
                    $query->where('name', 'like', '%'.request()->get('team_name').'%');
                }
                if (request()->filled('owner_name')) {
                    $owner = request()->get('owner_name');

                    $query->whereHas('User', function ($q) use ($owner) {
                        $q->whereRaw("CONCAT(first_name,' ',last_name) like ?", ["%{$owner}%"]);
                    });
                }
                if (request()->filled('sport_id') && request()->get('sport_id') != '') {

                    $query->where('sport_id', request()->get('sport_id'));
                }
                if (request()->filled('is_active')) {
                    $query->where('is_active', request()->get('is_active'));
                }

            })
            ->editColumn('logo100', function ($team) {
                return $team->logo ? '<img src="' . $team->logo100 . '" alt="Empty Image" class="img-circle">' : '';
            })
            ->editColumn('sport', function ($team) {
                return $team->sport->name_en;
            })
            ->editColumn('owner', function ($team) {
                return $team->owner->first_name . ' ' . $team->owner->last_name;
            })
            ->editColumn('is_active', function ($team) {

                return ($team->is_active == 1) ? '<span class="m-badge m-badge--success m-badge--wide">active</span>' : '<span class="m-badge m-badge--danger m-badge--wide">inactive</span>';
            })->addColumn('action', function ($team) {
                $checked='';
                if($team->is_active)
                    $checked='checked="checked"';
                $activate= '<span class="m-switch m-switch m-switch--outline m-switch--icon m-switch--danger m-switch--danger" style="margin-left:3px;vertical-align: middle;"><label><input type="checkbox"'.$checked .'name="is_active" class="is_active" data-id="'.$team->id.'"><span></span></label></span>';

                return '<a href="' . url(admin_team_url() . '/' . $team->id) . '" class="btn btn-primary m-btn m-btn--icon m-btn--icon-only m-btn--pill" title="Team details"><i class="fa flaticon-medical"></i></a>' . $activate;
            })->addIndexColumn()
            ->rawColumns(['is_active', 'action', 'logo100'])->toJson();
    }

    function export()
    {


    }

    function getAllTeams(array $attributes)
    {

        $page_size = isset($attributes['page_size']) ? $attributes['page_size'] : max_pagination();
        $page_number = isset($attributes['page_number']) ? $attributes['page_number'] : 1;
        $collection = $this->model->where('is_active', 1)->where('status', 'published');
        $teams_id = [];
        if (isset($attributes['search'])) {
            $collection = $collection->where('name', 'LIKE', '%' . $attributes['search'] . '%');
        }
        if (isset($attributes['league_id'])) {
            $league = League::find($attributes['league_id']);
            $teams_id = LeagueTeam::where('status', 'accepted')->where('league_id', $attributes['league_id'])->pluck('team_id')->toArray();
            $teams_id = Team::whereNotIn('id', $teams_id)->where('sport_id', $league->sport_id)->pluck('id')->toArray();
            $collection = $collection->whereIn('id', $teams_id);
        }

        if (isset($attributes['category_type'])) {

//            favorite_team,previous_team,following_team,top_team,nlf_team

            if ($attributes['category_type'] == 'favorite_team') {

                $favorites = auth()->user()->favoritesTeam->whereIn('id', $teams_id)->pluck('id');
                if (count($favorites) > 0) {
                    $collection = $collection->whereIn('id', $favorites);
                }
            }
            if ($attributes['category_type'] == 'previous_team') {
            }
            if ($attributes['category_type'] == 'following_team') {
                // Teams I Follow, type: team, category_type: following_team


                $followingTeams = auth()->user()->followingsTeams()->whereIn('follow_id', $teams_id);

                if ($followingTeams->count() > 0) {
                    $collection = $collection->whereIn('id', $followingTeams->pluck('follow_id'));
                }

            }
            if ($attributes['category_type'] == 'top_team') {
            }
            if ($attributes['category_type'] == 'nlf_team') {

                $collection = $collection->where('is_nlf', 1)->whereIn('id', $teams_id);

            }
        }

        if (isset($attributes['sport_id'])) {
            $collection = $this->model->where('sport_id', $attributes['sport_id']);
        }

        if (isset($attributes['type']) && $attributes['type'] == 'favorite' && auth()->check()) {

            $teams_id = Favorite::where('type', 'team')->where('user_id', auth()->user()->id)->orderByDesc('created_at')->pluck('favorite_id')->toArray();
            if (count($teams_id) > 0) {
                $ids_teams = implode(',', $teams_id);
                $collection = $collection->whereIn('id', $teams_id)->orderByRaw("FIELD(id, $ids_teams)");
            } else {
                $collection = $collection->whereIn('id', $teams_id);
            }

        } else {
            //new
            $collection = $collection->orderByDesc('created_at');
        }


        $count = $collection->count();

        if (isset($attributes['is_all']) && $attributes['is_all']) {
            return response_api(true, 200, null, $collection->get(), 1, 0, $count);
        }
        $page_count = page_count($count, $page_size);
        $page_number = $page_number - 1;
        $page_number = $page_number > $page_count ? $page_number = $page_count - 1 : $page_number;

        $object = $collection->take($page_size)->skip((int)$page_number * $page_size)->get();

        if (request()->segment(1) == 'api' || request()->ajax()) {
            return response_api(true, 200, null, $object, $page_count, $page_number, $count);
        }
    }

    function getAll(array $attributes)
    {

        $page_size = isset($attributes['page_size']) ? $attributes['page_size'] : max_pagination();
        $page_number = isset($attributes['page_number']) ? $attributes['page_number'] : 1;
        $collection = $this->model->where('is_active', 1)->where('status', 'published');

        $user_id = (isset($attributes['user_id'])) ? $attributes['user_id'] : null;//((auth()->check()) ? auth()->user()->id : null)

        if (isset($attributes['type']) && isset($user_id)) {
            if ($attributes['type'] == 'my_team') {
                // all teams user id is owner and published or user id is player in that teams
                $teams_id = TeamPlayer::where('user_id', $user_id)->where('status', 'accepted')->pluck('team_id');
                $collection = $collection->where(function ($query) use ($attributes, $teams_id, $user_id) {
                    $query->where('user_id', $user_id)->orWhereIn('id', $teams_id);
                });

            } elseif ($attributes['type'] == 'invite') {
                $teams_id = TeamPlayer::where('user_id', $user_id)->where('status', 'invited')->pluck('team_id');
                $collection = $collection->whereIn('id', $teams_id);

            } elseif ($attributes['type'] == 'request') {
                $teams_id = TeamPlayer::where('owner_id', $user_id)->where('status', 'joined')->pluck('team_id');
                $collection = $collection->whereIn('id', $teams_id);


            }
        }

        $count = $collection->count();
        $page_count = page_count($count, $page_size);
        $page_number = $page_number - 1;
        $page_number = $page_number > $page_count ? $page_number = $page_count - 1 : $page_number;

        $object = $collection->take($page_size)->skip((int)$page_number * $page_size)->get();

        if (request()->segment(1) == 'api' || request()->ajax()) {
            return response_api(true, 200, null, $object, $page_count, $page_number, $count);
        }
    }

    function getTopAllTeams(array $attributes)
    {

        $page_size = isset($attributes['page_size']) ? $attributes['page_size'] : max_pagination();
        $page_number = isset($attributes['page_number']) ? $attributes['page_number'] : 1;
        $collection = $this->model->where('is_active', 1)->where('status', 'published');

        $count = $collection->count();
        $page_count = page_count($count, $page_size);
        $page_number = $page_number - 1;
        $page_number = $page_number > $page_count ? $page_number = $page_count - 1 : $page_number;

        $all_teams = $collection->take($page_size)->skip((int)$page_number * $page_size)->orderByDesc('created_at')->get();

        // get top teams (top rate) // not complete [5 records]
        $top_teams = [];
        if (request()->segment(1) == 'api' || request()->ajax()) {
            return response_api(true, 200, null, ['all_teams' => ['data' => $all_teams, 'total_pages' => $page_count, 'current_page' => $page_number + 1, 'total_records' => $count], 'top_teams' => $top_teams]);
        }
    }

    function getSavedPublishTeams(array $attributes)
    {

        $page_size = isset($attributes['page_size']) ? $attributes['page_size'] : max_pagination();
        $page_number = isset($attributes['page_number']) ? $attributes['page_number'] : 1;
        $collection = $this->model->where('is_active', 1);

        $user_id = (isset($attributes['user_id'])) ? $attributes['user_id'] : auth()->user()->id;

        // all teams user id is owner and published or user id is player in that teams
        $teams_id = TeamPlayer::where('user_id', $user_id)->where('status', 'accepted')->pluck('team_id');

        $collection = $collection->where('status', 'published')->where(function ($query) use ($user_id, $teams_id) {
            $query->where('user_id', $user_id)->orWhereIn('id', $teams_id);
        });


        $count = $collection->count();
        $page_count = page_count($count, $page_size);
        $page_number = $page_number - 1;
        $page_number = $page_number > $page_count ? $page_number = $page_count - 1 : $page_number;

        $published_teams = $collection->take($page_size)->skip((int)$page_number * $page_size)->get();

        // get auth saved teams
        $saved_teams = $this->model->with('Sponsors')->where('is_active', 1)->where('status', 'saved')->where('user_id', auth()->user()->id)->get();
        if (request()->segment(1) == 'api' || request()->ajax()) {
            return response_api(true, 200, null, ['published_teams' => ['data' => $published_teams, 'total_pages' => $page_count, 'current_page' => $page_number + 1, 'total_records' => $count], 'saved_teams' => $saved_teams, 'invites_num' => auth()->user()->invites_num, 'requests_num' => auth()->user()->requests_num, 'my_teams_num' => auth()->user()->my_teams_num]);
        }
    }

    function getById($id)
    {
        if (request()->segment(1) == 'api') {
            $team = $this->model->where('is_active', 1)->find($id);
            if (isset($team))
                return response_api(true, 200, null, $team);
            return response_api(false, 422, trans('app.not_data_found'), []);
        }
        return $this->model->findOrFail($id);


    }

    function create(array $attributes)
    {
        // TODO: Implement create() method.

        $team = new Team();

        $team->user_id = auth()->user()->id;
        $team->name = $attributes['name'];
        if (isset($attributes['bio']))
            $team->bio = $attributes['bio'];
        $team->city_id = $attributes['city_id'];
        $team->sport_id = $attributes['sport_id'];
        if (isset($attributes['status']))
            $team->status = $attributes['status'];
        if ($team->save()) {

            if (isset($attributes['logo']) || isset($attributes['bg_image'])) {
                if (isset($attributes['logo']))
                    $team->logo = $this->storeImageThumb('teams', $team->id, $attributes['logo']);

                sleep(1);

                if (isset($attributes['bg_image']))
                    $team->bg_image = $this->storeImageThumb('teams', $team->id, $attributes['bg_image']);
                $team->save();
            }

            if (isset($attributes['sponsors']) && count($attributes['sponsors']) > 0) {

                TeamSponsor::where('team_id', $team->id)->forceDelete();
                foreach ($attributes['sponsors'] as $sponsor_id) {
                    $team_sponsor = new TeamSponsor();
                    $team_sponsor->team_id = $team->id;
                    $team_sponsor->sponsor_id = $sponsor_id;
                    $team_sponsor->save();
                }

                if (isset($attributes['main_sponsor_id'])) {
                    $main_sponsor = TeamSponsor::where('team_id', $team->id)->where('sponsor_id', $attributes['main_sponsor_id'])->first();
                    if (isset($main_sponsor)) {
                        $main_sponsor->is_main = 1;
                        $main_sponsor->save();
                    }
                }
            }

            $team = $this->model->find($team->id);
            return response_api(true, 200, trans('app.created'), $team);//
        }
        return response_api(false, 422, null, []);

    }

    function update(array $attributes, $id = null)
    {
        // TODO: Implement update() method.
        $team = $this->model->where('user_id', auth()->user()->id)->find($id);
        if (!isset($team))
            return response_api(false, 422, null, []);

        $team->user_id = auth()->user()->id;
        if (isset($attributes['name']))
            $team->name = $attributes['name'];
        if (isset($attributes['bio']))
            $team->bio = $attributes['bio'];
        if (isset($attributes['city_id']))
            $team->city_id = $attributes['city_id'];
        if (isset($attributes['sport_id']))
            $team->sport_id = $attributes['sport_id'];
        if (isset($attributes['status']))
            $team->status = $attributes['status'];
        if ($team->save()) {

            if (isset($attributes['logo']) || isset($attributes['bg_image'])) {
                if (isset($attributes['logo']))
                    $team->logo = $this->storeImageThumb('teams', $team->id, $attributes['logo']);

                sleep(1);

                if (isset($attributes['bg_image']))
                    $team->bg_image = $this->storeImageThumb('teams', $team->id, $attributes['bg_image']);
                $team->save();
            }

//            if (array_key_exists('sponsors', $attributes)) {

            TeamSponsor::where('team_id', $team->id)->forceDelete();
            if (array_key_exists('sponsors', $attributes) && count($attributes['sponsors']) > 0)
                foreach ($attributes['sponsors'] as $sponsor_id) {
                    $team_sponsor = new TeamSponsor();
                    $team_sponsor->team_id = $team->id;
                    $team_sponsor->sponsor_id = $sponsor_id;
                    $team_sponsor->save();
                }

            if (isset($attributes['main_sponsor_id'])) {
                $main_sponsor = TeamSponsor::where('team_id', $team->id)->where('sponsor_id', $attributes['main_sponsor_id'])->first();
                if (isset($main_sponsor)) {
                    $main_sponsor->is_main = 1;
                    $main_sponsor->save();
                }
            }
//            }
            $team = $this->model->find($team->id);

            return response_api(true, 200, trans('app.updated'), $team);//

        }
        return response_api(false, 422, null, []);

    }


    function delete($id)
    {
        // TODO: Implement delete() method.
        $team = $this->model->where('user_id', auth()->user()->id)->find($id);
        if (isset($team) && $team->delete()) {
            return response_api(true, 200, trans('app.deleted'), []);
        }
        return response_api(false, 422, null, []);

    }

    function teamActivate(array $attributes)
    {
        $team = $this->model->find($attributes['team_id']);
        if (isset($team)) {
            $team->is_active = $attributes['is_active'];

            if ($team->save()) {
                if (!$team->is_active) {

                    return response_api(true, 200);

                }
                return response_api(true, 200);
            }
        }
        return response_api(false, 422);
    }

    function count()
    {
        return $this->model->count();
    }
}
