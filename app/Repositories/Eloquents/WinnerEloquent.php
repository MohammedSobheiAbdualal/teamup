<?php
/**
 * Created by PhpStorm.
 * UserRequest: mohammedsobhei
 * Date: 5/2/18
 * Time: 11:43 PM
 */

namespace App\Repositories\Eloquents;

use App\Models\Winner;
use App\Repositories\Interfaces\Repository;

class WinnerEloquent implements Repository
{

    private $model;

    public function __construct(Winner $model)
    {
        $this->model = $model;

    }

    // for cpanel
    function anyData()
    {

    }

    function export()
    {


    }

    function getAll(array $attributes)
    {
        // TODO: Implement getAll() method.
        return $this->model->all();
    }

    function getById($id)
    {
        return $this->model->find($id);
    }

    function create(array $attributes)
    {
        // TODO: Implement create() method.

        $winner = new Winner();
        $winner->title_ar = $attributes['title_ar'];
        $winner->title_en = $attributes['title_en'];
        if ($winner->save())
            return response_api(true, 200, trans('app.created'), $winner);
        return response_api(false, 422, null, []);

    }

    function update(array $attributes, $id = null)
    {
        // TODO: Implement update() method.

        $winner = $this->model->find($id);
        $winner->title_ar = $attributes['title_ar'];
        $winner->title_en = $attributes['title_en'];
        if ($winner->save())
            return response_api(true, 200, trans('app.updated'), $winner);
        return response_api(false, 422, null, []);

    }

    function delete($id)
    {
        // TODO: Implement delete() method.
        $winner = $this->model->find($id);
        if (isset($winner) && $winner->delete()) {
            return response_api(true, 200, trans('app.deleted'), $winner);
        }
        return response_api(false, 422, null, []);

    }
}
