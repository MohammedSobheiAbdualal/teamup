<?php
/**
 * Created by PhpStorm.
 * UserRequest: mohammedsobhei
 * Date: 5/2/18
 * Time: 11:43 PM
 */

namespace App\Repositories\Eloquents;

use App\Http\Resources\League\PlayersLeagueResource;
use App\Http\Resources\League\PlayersResource;
use App\Http\Resources\PlayersLeagueCategoryResource;
use App\Models\Friend;
use App\Models\League;
use App\Models\LeagueTeam;
use App\Models\LeagueTeamPlayer;
use App\Models\Payment;
use App\Models\Team;
use App\Models\TeamManager;
use App\Models\TeamPlayer;
use App\Models\User;
use App\Repositories\Interfaces\Repository;
use DB;

class LeagueTeamEloquent implements Repository
{

    private $model, $notification;

    public function __construct(LeagueTeam $model, NotificationSystemEloquent $notificationSystemEloquent)
    {
        $this->model = $model;
        $this->notification = $notificationSystemEloquent;

    }

    // for cpanel
    function anyData()
    {

    }

    function export()
    {


    }

    function getAll(array $attributes)
    {

        // TODO: Implement getAll() method.
    }

    function getLeaguePlayersTeam(array $attributes)
    {
        $page_size = isset($attributes['page_size']) ? $attributes['page_size'] : max_pagination();
        $page_number = isset($attributes['page_number']) ? $attributes['page_number'] : 1;

        $league_teams = LeagueTeam::where('league_id', $attributes['league_id'])->where('team_id', $attributes['team_id'])->where('status', 'confirmed')->pluck('id')->unique();//
//
        $league_team_players = LeagueTeamPlayer::whereIn('league_team_id', $league_teams)->pluck('player_id');

        $collection = User::whereIn('id', $league_team_players);

        $count = $collection->count();
        $page_count = page_count($count, $page_size);
        $page_number = $page_number - 1;
        $page_number = $page_number > $page_count ? $page_number = $page_count - 1 : $page_number;

        $object = $collection->take($page_size)->skip((int)$page_number * $page_size)->get();

        if (request()->segment(1) == 'api' || request()->ajax()) {
            return response_api(true, 200, null, $object, $page_count, $page_number, $count);
        }

    }

    function getLeaguePlayersCategoryTeam(array $attributes)
    {
        // TODO: Implement getAll() method.

        $league_team = LeagueTeam::where('league_id', $attributes['league_id'])->where('status', 'confirmed')->pluck('id')->unique();
        $teams = LeagueTeam::where('league_id', $attributes['league_id'])->where('status', 'confirmed')->pluck('team_id')->unique();
//
//
        $league_team_players = LeagueTeamPlayer::whereIn('league_team_id', $league_team)->pluck('player_id');

        $team_players = TeamPlayer::whereIn('user_id', $league_team_players)->whereIn('team_id', $teams)->select('team_id', DB::raw('count(DISTINCT(user_id)) as total_records'))
            ->groupBy('team_id')
            ->get();

        return response_api(true, 200, null, PlayersLeagueCategoryResource::collection($team_players));

//        foreach ($team_players as $team_player) {
//            $players = TeamPlayer::where('team_id', $team_player->team_id)->pluck('user_id')->unique();
////            $team_player->total_records = User::whereIn('id', $players)->count();
//            $team_player->players = User::whereIn('id', $players)->get();
//        }

//        return response_api(true, 200, null, $team_players);
    }

    function getLeagueCategoryPlayers(array $attributes)
    {
        // TODO: Implement getAll() method.


//        $users = User::where('is_active', 1)->pluck('id');
//        $team_players = TeamPlayer::where('team_id', $attributes['team_id'])->whereIn('user_id', $users)->where('status', 'accepted')->select('position_id', DB::raw('count(DISTINCT(user_id)) as total_records'))
//            ->groupBy('position_id')
//            ->get();
//
//        foreach ($team_players as $team_player) {
//            $players = TeamPlayer::where('position_id', $team_player->position_id)->where('team_id', $attributes['team_id'])->where('status', 'accepted')->whereIn('user_id', $users)->pluck('user_id')->unique();
////            $team_player->total_records = User::whereIn('id', $players)->count();
//            $team_player->players = User::whereIn('id', $players)->get();
//        }
//        return response_api(true, 200, null, $team_players);


//        $users = User::where('is_active', 1)->pluck('id');
        $league_teams = LeagueTeam::where('team_id', request()->get('team_id'))->where('status', 'accepted')->pluck('id')->unique();
//
//
        $league_team_players = LeagueTeamPlayer::whereIn('league_team_id', $league_teams)->select('position_id', DB::raw('count(DISTINCT(player_id)) as total_records'))
            ->groupBy('position_id')
            ->get();

        return response_api(true, 200, null, PlayersLeagueResource::collection($league_team_players));

//        foreach ($team_players as $team_player) {
//            $players = TeamPlayer::where('position_id', $team_player->position_id)->where('team_id', $attributes['team_id'])->where('status', 'accepted')->whereIn('user_id', $users)->pluck('user_id')->unique();
////            $team_player->total_records = User::whereIn('id', $players)->count();
//            $team_player->players = User::whereIn('id', $players)->get();
//        }

//        return response_api(true, 200, null, $team_players);
    }

    function getById($id)
    {

    }

    function changeStatus(array $attributes)
    {
        $league = League::find($attributes['league_id']);
        $team = Team::find($attributes['team_id']);

        if ($attributes['status'] == 'accepted') { // if auth == team owner check if status invited
            if (isset($team) && ($team->user_id == auth()->user()->id)) { //

                $league_teams = $this->model->where('team_id', $attributes['team_id'])->where('league_id', $attributes['league_id'])->where('status', 'invited')->where('type', 'league')->get();//->update(['status' => $attributes['status']]);
                foreach ($league_teams as $league_team) {
                    $league_team->status = 'accepted';
                    $league_team->save();

//                $this->notification->sendNotification(auth()->user()->id, $team_player->owner_id, $team_player->team_id, 'accept', 'player', auth()->user()->id, auth()->user()->full_name);
                }
            } else if (isset($league) && ($league->user_id == auth()->user()->id)) { // if auth == league owner check if status invited

                $league_teams = $this->model->where('team_id', $attributes['team_id'])->where('league_id', $attributes['league_id'])->where('status', 'joined')->where('type', 'team')->get();//->update(['status' => $attributes['status']]);
                foreach ($league_teams as $league_team) {
                    $league_team->status = 'accepted';
                    $league_team->save();

//                $this->notification->sendNotification(auth()->user()->id, $team_player->owner_id, $team_player->team_id, 'accept', 'player', auth()->user()->id, auth()->user()->full_name);
                }
            }

            // in accept team check if (accept+confirm) teams > team specify num. if league specify

            if ($league->type == 'specify') {
                $league_teams = $league->Teams->where(function ($query) {
                    $query->where('status', 'accepted')->orWhere('status', 'confirmed');
                })->pluck('team_id')->unique();
                dd($league_teams);

            }

        } else if ($attributes['status'] == 'confirmed' && ($team->user_id == auth()->user()->id)) {

            // check if subscription free or paid
            // if paid then go to payment gateway and then confirmed by callback url
            if ($league->subscription_type == 'paid') {
                // waiting payment
                // save payment transaction

                $payment = new Payment();
                $payment->type = $attributes['type'];
                $payment->gateway = $attributes['gateway'];
                $payment->league_id = $league->id;
                $payment->team_id = $team->id;
                $payment->cost = $league->fees;
                $payment->save();
            }

            // if free then confirmed immediately
            // add players' (participants) team to league

            $league_team = $this->model->where('team_id', $attributes['team_id'])->where('league_id', $attributes['league_id'])->where('status', 'accepted')->first();
            foreach ($attributes['players_'] as $player) {

                $league_player = new LeagueTeamPlayer();
                $league_player->league_team_id = $league_team->id;
                $league_player->player_id = $player->player_id;
                if (isset($attributes['captain_id']) && $attributes['captain_id'] == $player->player_id)
                    $league_player->is_captain = 1;
                $league_player->league_id = $league->id;
                $league_player->team_id = $team->id;
                $league_player->player_no = $player->player_no;
                $league_player->position_id = $player->position_id;
                $league_player->save();
            }

            $league_teams = $this->model->where('team_id', $attributes['team_id'])->where('league_id', $attributes['league_id'])->where('status', 'accepted')->update(['status' => $attributes['status'], 'is_confirmed' => true, 'team_phone' => $attributes['team_phone']]);//->update(['status' => $attributes['status']]);

        } else if ($attributes['status'] == 'rejected') {
            $league_teams = $this->model->where('team_id', $attributes['team_id'])->where('league_id', $attributes['league_id'])->where(function ($query) {
                $query->where('status', 'joined')->orWhere('status', 'invited');
            })->get();//->update(['status' => $attributes['status']]);

            foreach ($league_teams as $league_team) {
                $league_team->status = 'rejected';
                $league_team->save();

//                $this->notification->sendNotification(auth()->user()->id, $team_player->owner_id, $team_player->team_id, 'accept', 'player', auth()->user()->id, auth()->user()->full_name);
            }

        } else if ($attributes['status'] == 'withdraw') {
            $league_teams = $this->model->where('team_id', $attributes['team_id'])->where('league_id', $attributes['league_id'])->where(function ($query) {
                $query->where('status', 'accepted')->orWhere('status', 'confirmed');
            })->get();//->update(['status' => $attributes['status']]);

            foreach ($league_teams as $league_team) {

                if ($league_team->status == 'confirmed') {

                    if ($league->subscription_type == 'paid' && $league->refundable) {
                        // check if before FRCD or not
                        //    check if refundable or not
                        $payment = Payment::where('league_id', $league->id)->where('team_id', $team->id)->first();
                        $payment->is_refunded = 1;
                        $payment->save();

                    }
                }

                $league_team->status = 'withdraw';
                $league_team->save();

//                $this->notification->sendNotification(auth()->user()->id, $team_player->owner_id, $team_player->team_id, 'accept', 'player', auth()->user()->id, auth()->user()->full_name);
            }

        } else {
            return response_api(false, 422, null, []);// Team player who can able to confirm request
        }


        return response_api(true, 200, trans('app.created'), $league);//
    }

    function create(array $attributes)
    {
        // TODO: Implement create() method.

        $league = League::find($attributes['league_id']);

//        if (!isset($attributes['status']) || $attributes['status'] != 'join')
//            if (auth()->user()->id != $team->user_id && auth()->user()->id != $team->manager_id)
//                return response_api(false, 422, trans('app.no_permission'), []);

        if (isset($attributes['team_id']) && count($attributes['team_id']) > 0) {

            foreach ($attributes['team_id'] as $team_id) {

                $league_team = new LeagueTeam();
                $league_team->user_id = auth()->user()->id;
                $league_team->team_id = $team_id;
                $league_team->league_id = $attributes['league_id'];
                $league_team->type = 'league';

                if (isset($attributes['status']) && $attributes['status'] == 'joined') {
                    $league_team->status = 'joined';
                    $league_team->type = 'team';
                }

                $sport = Team::find($team_id)->Sport->where('id', $league->sport_id)->first();
                if (!isset($sport)) continue;
//                if (isset($sport->primary_position))
//                    $team_player->position_id = $sport->primary_position->id;

                if ($league_team->save()) {
                    /*
                                        $status = ($league_team->status == 'joined') ? 'join' : 'invite';
                                        if ($league_team->status == 'joined') {
                                            $receiver = $league->user_id;
                                            $title = auth()->user()->full_name;
                                            $type = 'player';
                                            $data_id = auth()->user()->id;

                                        } else {

                                            $team = Team::find($team_id);
                                            $receiver = $team->user_id;
                                            $title = trans('app.league', ['name' => $league->name]);
                                            $type = 'league';
                                            $data_id = $attributes['team_id'];
                                        }*/

                    // send notification
//                    $this->notification->sendNotification(auth()->user()->id, $receiver, $receiver, $status, $type, $data_id, $title);
                }
            }
            return response_api(true, 200, trans('app.invited'), []);//
        }
        return response_api(false, 422, null, []);

    }


    function update(array $attributes, $id = null)
    {
        // TODO: Implement update() method.
    }

    function delete($attributes)
    {
        // TODO: Implement delete() method.

        $league = League::find($attributes['league_id']);
        $team = Team::find($attributes['team_id']);

        if (isset($league)) {

            if (auth()->user()->id == $team->user_id || auth()->user()->id == $league->user_id)  // team_owner will reject (after invited) an invitation or remove (after accept) or withdraw (after confirmed)
                if ($attributes['status'] == 'removed') {
//                if ($attributes['status'] == 'rejected' || $attributes['status'] == 'removed' || $attributes['status'] == 'withdraw') {
                    /*
                                        if ($attributes['status'] == 'withdraw') {
                                            // (subscription paid) check if before FRCD or not if before FRCD -> refund and then delete
                                            if ($league->subscription_type == 'paid') {
                                                // check if before FRCD or not
                                                //    check if refundable or not
                                            }
                                            // (subscription free) delete
                                            $this->model->where('league_id', $league->id)->where('team_id', $team->id)->where('status', 'confirmed')->where('type', 'league')->delete();
                                        } else
                                            $this->model->where('league_id', $league->id)->where('team_id', $team->id)->where('status', '<>', 'confirmed')->delete();
                    */
                    $this->model->where('league_id', $league->id)->where('team_id', $team->id)->delete();

                    return response_api(true, 200, null, $league);

                }
//            } elseif (auth()->user()->id == $league->user_id) { // league owner will reject (after invited) an invitation or remove (after accept) or withdraw (after confirmed)
//                if ($attributes['status'] == 'rejected' || $attributes['status'] == 'removed' || $attributes['status'] == 'withdraw') {
//                    if ($attributes['status'] == 'rejected') {
//                        $league_teams = $this->model->where('league_id', $league->id)->where('team_id', $team->id)->where('status', 'joined')->where('type', 'team')->update(['status' => 'rejected']);
//
//                    }
////                    if ($attributes['status'] == 'withdraw') {
////                        // (subscription paid) check if before FRCD or not if before FRCD -> refund and then delete
////                        // (subscription free) delete
////                    }
////                    $this->model->where('league_id', $league->id)->where('team_id', $team->id)->where('status', '<>', 'confirmed')->where('type', 'team')->delete();
//
//                    return response_api(true, 200, null, $league);
//                }
//            }

        }
        return response_api(false, 422, null, []);

    }
}
