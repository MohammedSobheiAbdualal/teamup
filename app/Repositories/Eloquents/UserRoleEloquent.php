<?php
/**
 * Created by PhpStorm.
 * User: mohammedsobhei
 * Date: 1/7/18
 * Time: 12:29 PM
 */
namespace App\Repositories\Eloquents;

use App\Repositories\Interfaces\Repository;
//use App\Models\RoleUser;

class UserRoleEloquent implements Repository
{

    private $model;

//    public function __construct(RoleUser $model)
//    {
//        $this->model = $model;
//    }

    function getAll(array $attributes)
    {
        // TODO: Implement getAll() method.
        return $this->model->all();
    }

    function getById($id)
    {
        // TODO: Implement getById() method.
        return $this->model->find($id);
    }

    function create(array $attributes)
    {
        // TODO: Implement create() method.

        $user_role = new RoleUser();
        $user_role->user_id = $attributes['user_id'];
        $user_role->role_id = $attributes['role_id'];
        return $user_role->save();
    }

    function update( array $attributes,$id = null)
    {
        // TODO: Implement update() method.
        return $this->model->find($id)->update($attributes);
    }

    function delete($id)
    {
        // TODO: Implement delete() method.
        return $this->model->find($id)->delete();
    }

    function getRolesByUser($user_id)
    {
        // TODO: Implement delete() method.
        return $this->model->where('user_id', $user_id)->pluck('role_id')->toArray();
    }
}
