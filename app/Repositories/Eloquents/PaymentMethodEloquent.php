<?php
/**
 * Created by PhpStorm.
 * UserRequest: mohammedsobhei
 * Date: 5/2/18
 * Time: 11:43 PM
 */

namespace App\Repositories\Eloquents;

use App\Models\PaymentMethod;
use App\Repositories\Interfaces\Repository;


class PaymentMethodEloquent implements Repository
{

    private $model;

    public function __construct(PaymentMethod $model)
    {
        $this->model = $model;
    }

    // for cpanel
    function anyData()
    {
        $payments = $this->model->orderByDesc('id');
        return datatables()->of($payments)
            ->filter(function ($query) {
            })
            ->addColumn('action', function ($payment) {

                $edit = '<a href="' . route('payments.edit', $payment->id) . '"  class="btn btn-outline-warning btn-elevate btn-circle btn-icon edit-payment-mdl" title="Edit" ><i class="flaticon-edit"></i></a>';

                return $edit;
            })->addIndexColumn()
            ->rawColumns(['action'])->toJson();

    }

    function export()
    {

    }

    function getAll(array $attributes)
    {

        // TODO: Implement getAll() method.

        return $this->model->all();
    }

    function getById($id)
    {
        return $this->model->find($id);
    }

    function create(array $attributes)
    {
        // TODO: Implement create() method.

    }

    function update(array $attributes, $id = null)
    {
        // TODO: Implement update() method.
       $payment=$this->model->find($id);
        $payment->description_ar=$attributes['description_ar'];
        $payment->description_en=$attributes['description_en'];

        if ( $payment->save()) {

            return response_api(true, 200, trans('app.payment_updated'), $payment);

        }
        return response_api(false, 422, trans('app.not_updated'));
    }

    function delete($id)
    {
        // TODO: Implement delete() method.

    }
}
