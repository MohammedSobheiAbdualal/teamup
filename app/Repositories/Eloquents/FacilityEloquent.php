<?php
/**
 * Created by PhpStorm.
 * UserRequest: mohammedsobhei
 * Date: 5/2/18
 * Time: 11:43 PM
 */

namespace App\Repositories\Eloquents;

use App\Models\Facility;
use App\Repositories\Interfaces\Repository;
use Excel;

class FacilityEloquent implements Repository
{

    private $model;

    public function __construct(Facility $model)
    {
        $this->model = $model;

    }

    // for cpanel
    function anyData()
    {

    }

    function export()
    {


    }

    function getAll(array $attributes)
    {
        // TODO: Implement getAll() method.
    }

    function getById($id)
    {
        return $this->model->find($id);

    }

    function create(array $attributes)
    {
        // TODO: Implement create() method.

    }

    function update(array $attributes, $id = null)
    {
        // TODO: Implement update() method.
    }

    function delete($id)
    {
        // TODO: Implement delete() method.

    }
}
