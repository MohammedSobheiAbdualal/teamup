<?php
/**
 * Created by PhpStorm.
 * UserRequest: mohammedsobhei
 * Date: 5/2/18
 * Time: 11:43 PM
 */

namespace App\Repositories\Eloquents;

use App\Repositories\Interfaces\Repository;
use App\Models\Instruction;
use Excel;

class InstructionEloquent implements Repository
{

    private $model;

    public function __construct(Instruction $model)
    {
        $this->model = $model;

    }

    // for cpanel
    function anyData()
    {

    }

    function export()
    {

    }

    function getAll(array $attributes)
    {

        // TODO: Implement getAll() method.

        $instructions = $this->model->all();
        if (request()->segment(1) == 'api') {
            return response_api(true, 200, null,  $instructions);
        }
        return  $instructions;
    }

    function getById($id)
    {
        if (request()->segment(1) == 'api') {
            // TODO: Implement getById() method.
            $setting = $this->model->where('lang_id', config()->get('app.lang_id'))->find($id);
            if (isset($setting))
                return response_api(true, 200, null, $setting);
            return response_api(false, 422, trans('app.not_data_found'), []);
        }
        return $this->model->find($id);

    }

    function getByTitle($title)
    {
        // TODO: Implement getById() method.
        $setting = $this->model->where('lang_id', config()->get('app.lang_id'))->where('title', $title)->get();

        if (isset($setting))
            return response_api(true, 200, null, $setting);
        return response_api(false, 422, trans('app.not_data_found'), []);

    }


    function create(array $attributes)
    {
        // TODO: Implement create() method.
        $setting = $this->model->where('title', $attributes['title'])->where('lang_id', $attributes['lang_id'])->first();
        if (!isset($setting))
            $setting = new Setting();

        $setting->title = $attributes['title'];
        $setting->s_id = Setting::max('s_id') + 1;
        $setting->description = $attributes['description'];
        $setting->lang_id = $attributes['lang_id'];
        if ($setting->save())
            return response_api(true, 200, trans('app.setting_saved'));
        return response_api(false, 422);
    }

    function update(array $attributes, $id = null)
    {
        // TODO: Implement update() method.
        $setting = $this->model->where('lang_id', $attributes['lang_id'])->where('s_id', $id)->where('field_type', $attributes['field_type'])->first();
        if (!isset($setting)) {
            $setting = new Setting();
            $setting->s_id = $id;
        }

//        $setting->title = $attributes['title'];
        $setting->description = $attributes['description'];
        $setting->lang_id = $attributes['lang_id'];
        if ($setting->save())
            return response_api(true, 200, trans('app.setting_saved'));
        return response_api(false, 422);
    }

    function delete($id)
    {
        // TODO: Implement delete() method.
        $setting = $this->model->find($id);

        if (isset($setting) && $setting->delete())
            return response_api(true, 200, trans('app.deleted'), []);
        return response_api(false, 422, null, []);
    }
}
