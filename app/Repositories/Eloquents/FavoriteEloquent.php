<?php
/**
 * Created by PhpStorm.
 * UserRequest: mohammedsobhei
 * Date: 5/2/18
 * Time: 11:43 PM
 */

namespace App\Repositories\Eloquents;

use App\Models\Favorite;
use App\Models\League;
use App\Models\Team;
use App\Models\User;
use App\Repositories\Interfaces\Repository;
use Excel;

class FavoriteEloquent implements Repository
{

    private $model;

    public function __construct(Favorite $model)
    {
        $this->model = $model;

    }

    // for cpanel
    function anyData()
    {

    }

    function export()
    {


    }

    function getAll(array $attributes)
    {

        // TODO: Implement getAll() method.
    }

    function getById($id)
    {
        return $this->model->find($id);

    }

    function createRemoveFavorite(array $attributes)
    {
        // TODO: Implement create() method.

        $favorite = $this->model->where('user_id', auth()->user()->id)->where('favorite_id', $attributes['favorite_id'])->where('type', $attributes['type'])->first();
        if (isset($favorite))
            return $this->delete($favorite->id);

        $favorite = new Favorite();
        $favorite->user_id = auth()->user()->id;
        $favorite->favorite_id = $attributes['favorite_id'];
        $favorite->type = $attributes['type'];
        if ($favorite->save()) {
            $object = null;
            if ($favorite->type == 'player') {
                $object = User::find($attributes['favorite_id']);
            } elseif ($favorite->type == 'team') {
                $object = Team::find($attributes['favorite_id']);
            } elseif ($favorite->type == 'league') {
                $object = League::find($attributes['favorite_id']);
            }
            return response_api(true, 200, trans('app.favorite'), $object);
        }//

        return response_api(false, 422, null, []);

    }

    function create(array $attributes)
    {
        // TODO: Implement create() method.

        $favorite = new Favorite();
        $favorite->user_id = auth()->user()->id;
        $favorite->favorite_id = $attributes['favorite_id'];
        $favorite->type = $attributes['type'];
        if ($favorite->save()) {
            $object = null;
            if ($attributes['type'] == 'player') {
                $object = User::find($attributes['favorite_id']);
            } elseif ($attributes['type'] == 'team') {
                $object = Team::find($attributes['favorite_id']);
            }
            return response_api(true, 200, trans('app.favorite'), $object);
        }//

        return response_api(false, 422, null, []);

    }

    function update(array $attributes, $id = null)
    {
        // TODO: Implement update() method.


    }

    function delete($id)
    {
        // TODO: Implement delete() method.
        $favorite = $this->model->where('user_id', auth()->user()->id)->find($id);

        if ($favorite->type == 'player') {
            $deleted = User::find($favorite->favorite_id);
        } else if ($favorite->type == 'team') {
            $deleted = Team::find($favorite->favorite_id);
        } else if ($favorite->type == 'league') {
            $deleted = League::find($favorite->favorite_id);
        }
        if (isset($favorite) && $favorite->delete()) {
            return response_api(true, 200, trans('app.unfavorite'), $deleted);
        }
        return response_api(false, 422, null, []);

    }
}
