<?php
/**
 * Created by PhpStorm.
 * UserRequest: mohammedsobhei
 * Date: 5/2/18
 * Time: 11:43 PM
 */

namespace App\Repositories\Eloquents;

use App\Http\Resources\User\PlayerTeamResource;
use App\Models\Friend;
use App\Models\League;
use App\Models\LeagueTeam;
use App\Models\LeagueTeamPlayer;
use App\Models\Team;
use App\Models\TeamManager;
use App\Models\TeamPlayer;
use App\Models\User;
use App\Repositories\Interfaces\Repository;
use Carbon\Carbon;
use DB;

class TeamPlayerEloquent implements Repository
{

    private $model, $notification;

    public function __construct(TeamPlayer $model, NotificationSystemEloquent $notificationSystemEloquent)
    {
        $this->model = $model;
        $this->notification = $notificationSystemEloquent;

    }

    // for cpanel
    function anyData()
    {

    }

    function export()
    {


    }

    function getAll(array $attributes)
    {

        // TODO: Implement getAll() method.
    }

    function getCategoryPlayers(array $attributes)
    {
        // TODO: Implement getAll() method.

        $users = User::where('is_active', 1)->pluck('id');
        $team_players = TeamPlayer::where('team_id', $attributes['team_id'])->whereIn('user_id', $users)->where('status', 'accepted')->select('position_id', DB::raw('count(DISTINCT(user_id)) as total_records'))
            ->groupBy('position_id')
            ->get();

        foreach ($team_players as $team_player) {
            $players = TeamPlayer::where('position_id', $team_player->position_id)->where('team_id', $attributes['team_id'])->where('status', 'accepted')->whereIn('user_id', $users)->pluck('user_id')->unique();
//            $team_player->total_records = User::whereIn('id', $players)->count();
            $team_player->players = User::whereIn('id', $players)->get();
        }

        return response_api(true, 200, null, $team_players);
    }

    function getTeamCategoryPlayers(array $attributes)
    {
        // TODO: Implement getAll() method.
        $users = User::where('is_active', 1)->pluck('id');
        $team_players = TeamPlayer::where('team_id', $attributes['team_id'])->whereIn('user_id', $users)->where('status', 'accepted')->select('position_id', DB::raw('count(DISTINCT(user_id)) as total_records'))
            ->groupBy('position_id')
            ->get();


        return response_api(true, 200, null, PlayerTeamResource::collection($team_players));
    }

    function getPlayers(array $attributes)
    {
        // TODO: Implement getAll() method.

        $page_size = isset($attributes['page_size']) ? $attributes['page_size'] : max_pagination();
        $page_number = isset($attributes['page_number']) ? $attributes['page_number'] : 1;

        $collection = User::where('is_active', 1);

        if (isset($attributes['type'])) {
            if ($attributes['type'] == 'invite') {

                $users_id = TeamPlayer::where('team_id', $attributes['team_id'])->where('status', 'invited')->pluck('user_id');
                $collection = $collection->whereIn('id', $users_id);
            } elseif ($attributes['type'] == 'request') {
                $users_id = TeamPlayer::where('team_id', $attributes['team_id'])->where('status', 'joined')->pluck('user_id');
                $collection = $collection->whereIn('id', $users_id);
            }
        }

        $count = $collection->count();
        $page_count = page_count($count, $page_size);
        $page_number = $page_number - 1;
        $page_number = $page_number > $page_count ? $page_number = $page_count - 1 : $page_number;

        $object = $collection->take($page_size)->skip((int)$page_number * $page_size)->get();

        if (request()->segment(1) == 'api' || request()->ajax()) {
            return response_api(true, 200, null, $object, $page_count, $page_number, $count);
        }
    }

    function getById($id)
    {

    }

    function changeStatus(array $attributes)
    {
        if ($attributes['status'] == 'accepted') {
            $team_players = $this->model->where('user_id', auth()->user()->id)->where('team_id', $attributes['team_id'])->get();//->update(['status' => $attributes['status']]);

            foreach ($team_players as $team_player) {
                $team_player->status = 'accepted';
                $team_player->save();

                $this->notification->sendNotification(auth()->user()->id, $team_player->owner_id, $team_player->team_id, 'accept', 'player', auth()->user()->id, auth()->user()->full_name);
            }

        }
        if ($attributes['status'] == 'leaved') {
            $team_players = $this->model->where('user_id', auth()->user()->id)->where('status', 'accepted')->where('team_id', $attributes['team_id'])->get();//->update(['status' => $attributes['status']]);
            foreach ($team_players as $team_player) {
                $team_player->status = 'leaved';
                $team_player->save();

                $this->notification->sendNotification(auth()->user()->id, $team_player->owner_id, $team_player->team_id, 'leave', 'player', auth()->user()->id, auth()->user()->full_name);
            }

        }


        return response_api(true, 200, trans('app.created'), []);//
    }

    function changePlayerStatus(array $attributes)
    {
        $team = Team::find($attributes['team_id']);
        if (isset($team) && ($team->user_id == auth()->user()->id || $team->manager->id == auth()->user()->id)) {
            if ($attributes['status'] == 'accepted') {
                $team_players = $this->model->where('user_id', $attributes['player_id'])->where('team_id', $attributes['team_id'])->get();//->update(['status' => $attributes['status']]);

                foreach ($team_players as $team_player) {
                    $team_player->status = 'accepted';
                    $team_player->save();

//                    $this->notification->sendNotification(auth()->user()->id, $team_player->user_id, $team_player->team_id, 'accept', $team->name);
                }
                $this->notification->sendNotification(auth()->user()->id, $attributes['player_id'], $attributes['team_id'], 'accept', 'team', $team->id, trans('app.team', ['name' => $team->name]));

                return response_api(true, 200, trans('app.invitation_accept'), []);//
            }
            if ($attributes['status'] == 'rejected' || $attributes['status'] == 'canceled') {

                if ($attributes['status'] == 'rejected') {
                    $team_players = $this->model->where('user_id', $attributes['player_id'])->where('team_id', $attributes['team_id'])->where('status', 'joined')->get();
                    $message = 'invitation_reject';

                } elseif ($attributes['status'] == 'canceled') {
                    $team_players = $this->model->where('user_id', $attributes['player_id'])->where('team_id', $attributes['team_id'])->where('status', 'invited')->get();
                    $message = 'invitation_cancel';

                }
                foreach ($team_players as $team_player) {
                    $team_player->delete();

//                    $this->notification->sendNotification(auth()->user()->id, $team_player->user_id, $team_player->team_id, $status, $team->name);
                }
                $status = ($attributes['status'] == 'rejected') ? 'reject' : 'cancel';

                $this->notification->sendNotification(auth()->user()->id, $attributes['player_id'], $attributes['team_id'], $status, 'team', $attributes['team_id'], trans('app.team', ['name' => $team->name]));

                return response_api(true, 200, trans('app.' . $message), []);//

            }

        }
        return response_api(false, 422, 'permission denied!', []);

    }

    function create(array $attributes)
    {
        // TODO: Implement create() method.

        $team = Team::find($attributes['team_id']);

//        if (!isset($attributes['status']) || $attributes['status'] != 'join')
//            if (auth()->user()->id != $team->user_id && auth()->user()->id != $team->manager_id)
//                return response_api(false, 422, trans('app.no_permission'), []);

        if (isset($attributes['user_id']) && count($attributes['user_id']) > 0) {

            foreach ($attributes['user_id'] as $user_id) {

                $team_player = $this->model->where('user_id', $user_id)->where('team_id', $attributes['team_id'])->first();//where('owner_id', auth()->user()->id)->
                if (isset($team_player)) continue;

                $team_player = new TeamPlayer();
                $team_player->owner_id = auth()->user()->id;
                $team_player->user_id = $user_id;
                $team_player->team_id = $attributes['team_id'];

                if (isset($attributes['status']) && $attributes['status'] == 'joined')
                    $team_player->status = 'joined';


                $sport = User::find($user_id)->Sports->where('id', $team->sport_id)->first();

                if (!isset($sport)) continue;
                if (isset($sport->primary_position)) {
                    $team_player->position_id = $sport->primary_position->id;
                    $team_player->secondary_position_id = $sport->secondary_position->id;
                }

                if (isset($sport) && $team_player->save()) {

                    $status = ($team_player->status == 'joined') ? 'join' : 'invite';
                    if ($team_player->status == 'joined') {
                        $receiver = $team->user_id;
                        $title = auth()->user()->full_name;
                        $type = 'player';
                        $data_id = auth()->user()->id;

                    } else {
                        $receiver = $user_id;
                        $title = trans('app.team', ['name' => $team->name]);
                        $type = 'team';
                        $data_id = $attributes['team_id'];
                    }

                    if (isset($attributes['manager_id']) && $attributes['manager_id'] == $user_id) {
                        $team->manager_id = $attributes['manager_id'];
                        if ($team->save()) {
                            $team_manager = new TeamManager();
                            $team_manager->team_id = $team->id;
                            $team_manager->manager_id = $team->manager_id;
                            $team_manager->save();
                        }
                    }
                    $this->notification->sendNotification(auth()->user()->id, $receiver, $receiver, $status, $type, $data_id, $title);
                }
            }
            return response_api(true, 200, trans('app.invited'), []);//
        }
        return response_api(false, 422, null, []);

    }


    function update(array $attributes, $id = null)
    {
        // TODO: Implement update() method.
    }

    function delete($id)
    {
        // TODO: Implement delete() method.

        $team = Team::find($id);
        if (request()->get('status') == 'canceled') {
            $team_players = $this->model->where('user_id', auth()->user()->id)->where('status', 'joined')->where('team_id', $id);
            $message = 'invitation_cancel';
        } else // reject
        {
            $team_players = $this->model->where('user_id', auth()->user()->id)->where('status', 'invited')->where('team_id', $id);
            $message = 'invitation_reject';
        }

        $players = $team_players->get();
        if (isset($team_players) && $team_players->delete()) {

            foreach ($players as $player) {
                $player->delete();
            }

            $status = (request()->get('status') == 'rejected') ? 'reject' : 'cancel';
            $this->notification->sendNotification(auth()->user()->id, $team->user_id, $id, $status, 'player', auth()->user()->id, auth()->user()->full_name);

            return response_api(true, 200, trans('app.' . $message), []);
        }
        return response_api(false, 422, null, []);

    }


    public function updateTeamPlayer(array $attributes)
    {

//        [{"player_id":3,"is_captain":1,"player_no":5,"position_id":1}]
        //`user_id`, `owner_id`, `team_id`, `position_id`, `secondary_position_id`, `status`, `player_no`, `is_captain`, `is_injured`,
        foreach ($attributes['players_'] as $player) {
            $team_player = $this->model->find($player->team_player_id); // pk team player table

            if (!isset($team_player)) continue;
            $team_player->position_id = $player->position_id;
            $team_player->secondary_position_id = $player->secondary_position_id;
            $team_player->player_no = $player->player_no;
            $team_player->is_captain = $player->is_captain;
            $team_player->is_injured = $player->is_injured;
            $team_player->save();
        }
//
        return response_api(true, 200, null, []);


    }

    public function setInjured(array $attributes)
    {
        $team_player = $this->model->where('owner_id', auth()->user()->id)->find($attributes['team_player_id']); // pk team player table

        if (isset($team_player)) {
            $team_player->is_injured = !$team_player->is_injured;
            $team_player->save();
        }

        return response_api(true, 200, null, []);

    }

    public function deletePlayerTeam(array $attributes)
    {
        // check if player in league and league is not ended
        $teamPlayer = $this->model->find($attributes['team_player_id']);

        // check if auth is owner team to remove player
        if (isset($teamPlayer)) {
            $own_team = Team::where('user_id', auth()->user()->id)->find($teamPlayer->team_id);

            if (isset($own_team)) {
                $leagueTeamIds = LeagueTeam::where('status', 'confirmed')->where('team_id', $teamPlayer->team_id)->pluck('id'); //league teams id

                if (count($leagueTeamIds) > 0) {
                    $leagueTeamPlayerIds = LeagueTeamPlayer::whereIn('league_team_id', $leagueTeamIds)->where('player_id', $teamPlayer->user_id)->pluck('league_team_id');

                    if (count($leagueTeamPlayerIds) > 0) {
                        $leagueIds = LeagueTeam::whereIn('id', $leagueTeamPlayerIds)->pluck('league_id'); //leagues id

                        if (count($leagueIds) > 0) {
                            $endedLeague = League::whereDate('end_date', '>', Carbon::now()->format('Y-m-d'))->whereIn('id', $leagueIds)->first();
//                            $upcomingLeague = League::whereDate('start_date', '>', Carbon::now()->format('Y-m-d'))->whereIn('id', $leagueIds)->first();

//                            // check if total players participates > main_lineup + bench + reserve_num
//                            if (isset($upcomingLeague)) {
//
//                            }

                            if (isset($endedLeague)) { //|| (isset($upcomingLeague) && $upcomingLeague)
                                // you can't remove this player from team
                                return response_api(false, 422, 'you can\'t remove this player from team', []);
                            }
                        }
                    }
                }
            }
        }

        //
        if (isset($own_team) && isset($teamPlayer) && $teamPlayer->delete()) {
            return response_api(true, 200, 'Deleted Successfully', []);
        }
        return response_api(false, 422, 'you can\'t remove this player from team', []);

    }
}
