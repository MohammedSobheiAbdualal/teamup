<?php
/**
 * Created by PhpStorm.
 * UserRequest: mohammedsobhei
 * Date: 5/2/18
 * Time: 11:43 PM
 */

namespace App\Repositories\Eloquents;

use App\Models\Follower;
use App\Models\League;
use App\Models\Team;
use App\Models\User;
use App\Repositories\Interfaces\Repository;
use Excel;

class FollowEloquent implements Repository
{

    private $model, $user, $notification;

    public function __construct(Follower $model, User $user, NotificationSystemEloquent $notificationSystemEloquent)
    {
        $this->model = $model;
        $this->user = $user;
        $this->notification = $notificationSystemEloquent;
    }

    // for cpanel
    function anyData()
    {

    }

    function export()
    {


    }

    function getAll(array $attributes)
    {

        // TODO: Implement getAll() method.
    }

    function getById($id)
    {
        return $this->model->find($id);

    }

    public function getFollowers(array $attributes)
    {
        $page_size = isset($attributes['page_size']) ? $attributes['page_size'] : max_pagination();
        $page_number = isset($attributes['page_number']) ? $attributes['page_number'] : 1;

        $collection = $this->user->find($attributes['user_id'])->followers();

        if (isset($attributes['name'])) {
            $collection = $collection->where('username', 'LIKE', '%' . $attributes['name'] . '%')->orWhereRaw('CONCAT(first_name,last_name)', 'LIKE', '%' . $attributes['name'] . '%');
        }

//        $collection = collect($collection);
        $count = $collection->count();
        $page_count = page_count($count, $page_size);
        $page_number = $page_number - 1;
        $page_number = $page_number > $page_count ? $page_number = $page_count - 1 : $page_number;

        $object = $collection->take($page_size)->skip((int)$page_number * $page_size)->get();

        if (request()->segment(1) == 'api' || request()->ajax()) {
            return response_api(true, 200, null, $object, $page_count, $page_number, $count);
        }


    }


    public function getFollowings(array $attributes)
    {
        $page_size = isset($attributes['page_size']) ? $attributes['page_size'] : max_pagination();
        $page_number = isset($attributes['page_number']) ? $attributes['page_number'] : 1;

        if ($attributes['type'] == 'player')
            $collection = $this->user->find($attributes['user_id'])->followings();
        if ($attributes['type'] == 'team')
            $collection = $this->user->find($attributes['user_id'])->followingsTeams();
        if ($attributes['type'] == 'league')
            $collection = $this->user->find($attributes['user_id'])->followingsLeagues();

        if (isset($attributes['name'])) {
            if ($attributes['type'] == 'player')
                $collection = $collection->where('username', 'LIKE', '%' . $attributes['name'] . '%')->orWhereRaw('CONCAT(first_name,last_name)', 'LIKE', '%' . $attributes['name'] . '%');
            if ($attributes['type'] == 'team')
                $collection = $collection->where('name', 'LIKE', '%' . $attributes['name'] . '%');
            if ($attributes['type'] == 'league')
                $collection = $collection->where('name', 'LIKE', '%' . $attributes['name'] . '%');

        }

        $count = $collection->count();
        $page_count = page_count($count, $page_size);
        $page_number = $page_number - 1;
        $page_number = $page_number > $page_count ? $page_number = $page_count - 1 : $page_number;

        $object = $collection->take($page_size)->skip((int)$page_number * $page_size)->get();

        if (request()->segment(1) == 'api' || request()->ajax()) {
            return response_api(true, 200, null, $object, $page_count, $page_number, $count);
        }

    }

    function createRemoveFollow(array $attributes)
    {
        // TODO: Implement create() method.
        $follow = $this->model->where('user_id', auth()->user()->id)->where('follow_id', $attributes['follow_id'])->where('type', $attributes['type'])->first();
        if (isset($follow))
            return $this->delete($follow->id);

        $follow = new Follower();
        $follow->user_id = auth()->user()->id;
        $follow->follow_id = $attributes['follow_id'];
        $follow->type = $attributes['type'];
        if ($follow->save()) {
            $object = null;
            $receiver_id = null;
            if ($attributes['type'] == 'player') {
                $object = User::find($attributes['follow_id']);
                $receiver_id = $object->id;
                $action = 'follow';
                $action_id = auth()->user()->id;
                $type = 'player';

            } elseif ($attributes['type'] == 'team') {
                $object = Team::find($attributes['follow_id']);
                $receiver_id = $object->user_id;
                $action = 'follow_team';
                $action_id = $object->id;
                $type = 'team';
            } elseif ($attributes['type'] == 'league') {
                $object = League::find($attributes['follow_id']);
                $receiver_id = $object->user_id;
                $action = 'follow_league';
                $action_id = $object->id;
                $type = 'league';
            }

            $this->notification->sendNotification(auth()->user()->id, $receiver_id, $action_id, $action, $type, auth()->user()->id, auth()->user()->full_name);
            return response_api(true, 200, trans('app.follow'), $object);
        }//
        return response_api(false, 422, null, []);

    }

    function create(array $attributes)
    {
        // TODO: Implement create() method.
        $follow = new Follower();
        $follow->user_id = auth()->user()->id;
        $follow->follow_id = $attributes['follow_id'];
        $follow->type = $attributes['type'];
        if ($follow->save()) {
            $object = null;
            if ($attributes['type'] == 'player') {
                $object = User::find($attributes['follow_id']);
            } elseif ($attributes['type'] == 'team') {
                $object = Team::find($attributes['follow_id']);
            }
            return response_api(true, 200, trans('app.follow'), $object);

        }//
        return response_api(false, 422, null, []);

    }

    function update(array $attributes, $id = null)
    {
        // TODO: Implement update() method.
    }

    function delete($id)
    {
        // TODO: Implement delete() method.
        $follow = $this->model->where('user_id', auth()->user()->id)->find($id);

        if ($follow->type == 'player') {
            $deleted = User::find($follow->follow_id);
        } else if ($follow->type == 'team') {
            $deleted = Team::find($follow->follow_id);
        } else if ($follow->type == 'league') {
            $deleted = League::find($follow->follow_id);
        }

        if (isset($follow) && $follow->delete()) {
            return response_api(true, 200, trans('app.unfollow'), $deleted);
        }
        return response_api(false, 422, null, []);

    }
}
