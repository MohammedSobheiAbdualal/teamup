<?php
/**
 * Created by PhpStorm.
 * UserRequest: mohammedsobhei
 * Date: 5/2/18
 * Time: 11:43 PM
 */

namespace App\Repositories\Eloquents;

use App\Models\Position;
use App\Models\Sport;
use App\Models\Team;
use App\Repositories\Interfaces\Repository;
use App\Repositories\Uploader;
use Excel;

class SportEloquent extends Uploader implements Repository
{

    private $model;

    public function __construct(Sport $model)
    {
        $this->model = $model;

    }

    // for cpanel
    function anyData()
    {
        $sports = $this->model->with('Icon')->orderByDesc('id');
        return datatables()->of($sports)
            ->filter(function ($query) {
                if (request()->filled('sport_name')) {
                    $query->where(function ($q) {
                        $q->where('name_ar', 'like', '%' . request()->get('sport_name') . '%')->orWhere('name_en', 'like', '%' . request()->get('sport_name') . '%');
                    });
                }
                if (request()->filled('is_active')) {
                    $query->where('is_active', request()->get('is_active'));
                }
            })
            ->editColumn('image', function ($sport) {
                return $sport->image ? '<img src="' . $sport->image100 . '" class="img-circle" alt="Empty Image">' : '';
            })
            ->editColumn('icon', function ($sport) {
                return '<span class="' . $sport->Icon()->first()->title . '" ></span>';
            })
            ->addColumn('position', function ($sport) {
                return $sport->Positions()->count();
            })
            ->editColumn('is_active', function ($sport) {
                return ($sport->is_active == 1) ? '<span class="m-badge m-badge--success m-badge--wide">active</span>' : '<span class="m-badge m-badge--danger m-badge--wide">inactive</span>';
            })
            ->addColumn('action', function ($sport) {
                $teams = Team::where('sport_id', $sport->id)->first();
                $delete = '';
                if (!$teams) {
                    $delete = '<button type="button" class="btn btn-outline-danger btn-elevate btn-circle btn-icon btn-delete" title="Delete" data-id="' . $sport->id . '"><i class="flaticon-delete"></i></button>';

                }
                $checked='';
                if($sport->is_active)
                    $checked='checked="checked"';
                $activate= '<span class="m-switch m-switch m-switch--sm m-switch--outline m-switch--icon m-switch--danger m-switch--warning" style="margin-left:3px;vertical-align: middle;"><label><input type="checkbox"'.$checked .'name="is_active" class="is_active" data-id="'.$sport->id.'"><span></span></label></span>';

                $edit = '<a href="' . route('sports.edit', $sport->id) . '"  class="btn btn-outline-warning btn-elevate btn-circle btn-icon" title="Edit" ><i class="flaticon-edit"></i></a>';

                return $activate . $edit . $delete;
            })->addIndexColumn()
            ->rawColumns(['is_active', 'action', 'icon', 'image'])->toJson();

    }

    function export()
    {


    }

    function getAll(array $attributes)
    {

        // TODO: Implement getAll() method.

        $sports = $this->model->with('positions')->get();
        if (request()->segment(1) == 'api') {
            return response_api(true, 200, null, $sports);
        }
        return $sports;
    }

    function getById($id)
    {
        if (request()->segment(1) == 'api') {
            // TODO: Implement getById() method.
            $sport = $this->model->find($id);
            if (isset($sport))
                return response_api(true, 200, null, $sport);
            return response_api(false, 422, trans('app.not_data_found'), []);
        }
        return $this->model->findOrFail($id);

    }

    function getByTitle($title)
    {
        // TODO: Implement getById() method.


    }


    function create(array $attributes)
    {
        // TODO: Implement create() method.

        $sport = new Sport();
        $sport->name_en = $attributes['name_en'];
        $sport->name_ar = $attributes['name_ar'];
        if (isset($attributes['has_position']) and $attributes['has_position'] == 1)
            $sport->has_position = 1;
        else
            $sport->has_position = 0;

        $sport->icon_id = $attributes['icon'];

        if ($sport->save()) {
            if (isset($attributes['image']) and $attributes['image'] != '')
                $sport->image = $this->storeImageThumb('sports', $sport->id, $attributes['image']);
            $sport->save();

            if (isset($attributes['has_position']) and $attributes['has_position'] == 1) {

                $count = count($attributes['position'][0]);
                for ($i = 0; $i < $count; $i++) {
                    $new_position = new Position();
                    $new_position->title_en = $attributes['position'][0][$i];
                    $new_position->title_ar = $attributes['position'][1][$i];
                    $new_position->shortcut = $attributes['position'][2][$i];
                    $new_position->sport_id = $sport->id;
                    $new_position->save();
                }
            }
           // return response_api(true, 200, trans('app.created'), $sport);
            return response_api(true, 200, trans('app.created'), [ 'url' => url(admin_sports_url() . '/upload-media/' . $sport->id . '?_token=' . csrf_token())]);

        }

        return response_api(false, 422, trans('app.not_created'));
    }

    function update(array $attributes, $id = null)
    {
        // TODO: Implement update() method.

        $sport = $this->model->find($id);
        $sport->name_en = $attributes['name_en'];
        $sport->name_ar = $attributes['name_ar'];
        if (isset($attributes['has_position']) and $attributes['has_position'] == 1)
            $sport->has_position = 1;
        else
            $sport->has_position = 0;

        $sport->icon_id = $attributes['icon'];
        $sport->image=null;
        if ($sport->save()) {
            $sport->Positions()->delete();
            if (isset($attributes['has_position']) and $attributes['has_position'] == 1) {

                $count = count($attributes['position'][0]);

                for ($i = 0; $i < $count; $i++) {
                    $new_position = new Position();
                    $new_position->title_en = $attributes['position'][0][$i];
                    $new_position->title_ar = $attributes['position'][1][$i];
                    $new_position->shortcut = $attributes['position'][2][$i];
                    $new_position->sport_id = $sport->id;
                    $new_position->save();
                }
            }
           // return response_api(true, 200, trans('app.updated'), $sport);
            return response_api(true, 200, trans('app.updated'), ['sport_id' => $sport->id, 'url' => url(admin_sports_url() . '/upload-media/' . $sport->id . '?_token=' . csrf_token())]);
        }

        return response_api(false, 422, trans('app.not_created'));


    }


    function uploadMedia(array $attributes, $id = null){
        $sport =  $this->model->find($id);
        $sport->image=$this->storeImageThumb('sports', $sport->id, $attributes['file']);
        $sport->save();
        return response_api(true, 200 );
    }

    function removeMedia(array $attributes, $id = null){
        $sport =  $this->model->find($id);

        $this->deleteImage('sports', $sport->id, $attributes['filename']);
        $sport->image=null;
        $sport->save();
        return response_api(true, 200 );
    }


    function delete($id)
    {
        // TODO: Implement delete() method.
        $sport = $this->model->find($id);
        if (isset($sport) && $sport->delete()) {
            return response_api(true, 200, trans('app.deleted'), []);
        }
        return response_api(false, 422, null, []);

    }
    function activation(array $attributes)
    {
        $sport = $this->model->find($attributes['sport_id']);
        if (isset($sport)) {
            $sport->is_active = $attributes['is_active'];

            if ($sport->save()) {
                if (!$sport->is_active) {

                    return response_api(true, 200);

                }
                return response_api(true, 200);
            }
        }
        return response_api(false, 422);
    }
}
