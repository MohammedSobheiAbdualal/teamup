<?php
/**
 * Created by PhpStorm.
 * UserRequest: mohammedsobhei
 * Date: 5/2/18
 * Time: 11:43 PM
 */

namespace App\Repositories\Eloquents;

use App\Models\League;
use App\Models\LeagueTeam;
use App\Models\LeagueTeamPlayer;
use App\Models\Team;
use App\Models\TeamPlayer;
use App\Models\User;
use App\Models\UserLocation;
use App\Models\UserSport;
use function Composer\Autoload\includeFile;

class CategorizeEloquent
{
    function getTeamCategorizes(array $attributes)
    {
        // TODO: Implement getAll() method.

        $array = [];

        $collection = User::where('is_active', 1)->where('id', '<>', auth()->user()->id);
        $active_user_ids = $collection->where('signup_level', 3)->pluck('id');


        $team = Team::find($attributes['action_id']);
        if (!isset($team))
            return response_api(true, 200, null, $array);

        $team_players = isset($team->players) ? $team->Players()->where('status', 'accepted')->whereIn('user_id', $active_user_ids)->pluck('user_id')->toArray() : [];
        $user_in_same_team_id = UserSport::where('sport_id', $team->sport_id)->whereNotIn('user_id', $team_players)->pluck('user_id')->toArray();


        $players_id = (array_diff($user_in_same_team_id, $team_players));

        // Friends, type: player, category_type: friend

        $friends = auth()->user()->mergeFriends()->whereIn('id', $players_id)->pluck('id')->unique();
        if (count($friends) > 0) {

            $friend_category = new \stdClass();
            $friend_category->total_records = count($friends);
            $friend_category->title = 'Friends';
            $friend_category->category_type = 'friend';
            $friend_category->type = 'player';
            $friend_category->data = User::whereIn('id', $friends)->take(3)->get();

            $array[] = $friend_category;
        }

        $favorites = auth()->user()->favoritesUser->whereIn('id', $players_id)->pluck('id')->unique();

        // Favorite players, type: player, category_type: favorite_player
        if (count($favorites) > 0) {
            $favorite_category = new \stdClass();
            $favorite_category->total_records = count($favorites);
            $favorite_category->title = 'Favorite players';
            $favorite_category->category_type = 'favorite_player';
            $favorite_category->type = 'player';
            $favorite_category->data = User::whereIn('id', $favorites)->take(3)->get();

            $array[] = $favorite_category;
        }

        // Team mates, type: team, category_type: team_mate
        if (auth()->user()->my_teams->count() > 0) {

            $my_teams_id = TeamPlayer::where('user_id', auth()->user()->id)->where('team_players.status', 'accepted')->pluck('team_players.team_id');
            $teams = Team::where(function ($query) use ($my_teams_id) {
                $query->where('user_id', auth()->user()->id)->orWhereIn('id', $my_teams_id);
            })->where('status', 'published')->get();

            $teams_list = [];

            foreach ($teams as $team) {
//                if (count($teams_list) == 5) break;
                if (!isset($team->players) || count($team->players) == 0) continue;
                $teams_list[] = $team;
            }
            $team_category = new \stdClass();
            $team_category->total_records = count($teams_list);
            $team_category->title = 'Team Mates';
            $team_category->category_type = 'team_mate';
            $team_category->type = 'team';
            $team_category->data = collect($teams_list)->take(5);

            if (count($teams_list) > 0)
                $array[] = $team_category;
        }

        // Top players, type: player, category_type: top_player

        // Nearby Players, type: player, category_type: near_by_player default 5K
        $user_locations = UserLocation::where('user_id', '<>', auth()->user()->id)->get();
        $players = [];
        if (isset(auth()->user()->Location))
            foreach ($user_locations as $location) {

                $user = User::where('is_active', 1)->find($location->user_id);
                if (!isset($user) || $user->signup_level != 3) continue;

                $distance = distance($location->latitude, $location->longitude, auth()->user()->Location->latitude, auth()->user()->Location->longitude);
                if ($distance <= 5000) {
                    $players[] = $location->user_id;
                }
            }
        if (count($players) > 0) {

            $players = array_intersect($players_id, $players);

            $collection = User::whereIn('id', $players);
            $players_count = $collection->count();
            $players = $collection->take(3)->get();

            $near_by_category = new \stdClass();
            $near_by_category->total_records = $players_count;
            $near_by_category->title = 'Nearby Players';
            $near_by_category->category_type = 'near_by_player';
            $near_by_category->type = 'player';
            $near_by_category->data = $players;

            $array[] = $near_by_category;
        }


        // Previous Players, type: matches, category_type: previous_players


        // Players I Follow, type: player, category_type: following

        $followingPlayers = auth()->user()->followings()->where('followers.type', 'player')->whereIn('follow_id', $players_id);

        if ($followingPlayers->count() > 0) {
            $following_category = new \stdClass();
            $following_category->total_records = $followingPlayers->count();
            $following_category->title = 'Players I Follow';
            $following_category->category_type = 'following';
            $following_category->type = 'player';
            $following_category->data = $followingPlayers->take(3)->get();

            $array[] = $following_category;
        }

        // Followers, type: player, category_type: follower
        $followerPlayers = auth()->user()->followers()->whereIn('user_id', $players_id)->where('type', 'player');

        if ($followerPlayers->count() > 0) {

            $follower_category = new \stdClass();
            $follower_category->total_records = $followerPlayers->count();
            $follower_category->title = 'Followers';
            $follower_category->category_type = 'follower';
            $follower_category->type = 'player';
            $follower_category->data = $followerPlayers->take(3)->get();

            $array[] = $follower_category;
        }

        // In a Team, type: player, category_type: in_team

        // Played In a League, type: player, category_type: played_league

        // NLF League, type: player, category_type: nlf_league
        $leagues_nlf = League::where('is_nlf', 1)->where('status', 'published')->where('is_active', 1)->pluck('id');
        $league_teams_id = LeagueTeam::whereIn('league_id', $leagues_nlf)->where('status', 'accepted')->pluck('id');
        $nlfLeague = LeagueTeamPlayer::whereIn('league_team_id', $league_teams_id)->whereIn('player_id', $players_id);

        if ($nlfLeague->count() > 0) {
            $players_id = $nlfLeague->pluck('player_id')->unique();
            $players = $collection->whereIn('id', $players_id)->take(3)->get();

            $nlf_league_category = new \stdClass();
            $nlf_league_category->total_records = count($players_id);
            $nlf_league_category->title = 'NLF League';
            $nlf_league_category->category_type = 'nlf_league';
            $nlf_league_category->type = 'player';
            $nlf_league_category->data = $players;

            $array[] = $nlf_league_category;
        }
        return response_api(true, 200, null, $array);

    }

    function getLeagueCategorizes(array $attributes)
    {
        // TODO: Implement getAll() method.

        $array = [];

        $collection = Team::where('is_active', 1)->where('status', 'published');
        $league = League::find($attributes['action_id']);
        if (!isset($league))
            return response_api(true, 200, null, $array);

        $team_in_same_league_id = $collection->where('sport_id', $league->sport_id)->pluck('id')->toArray();

        $league_teams = isset($league->teams) ? $league->Teams()->where('status', 'accepted')->whereIn('team_id', $team_in_same_league_id)->pluck('team_id')->toArray() : [];
        // exclude teams already invited in the league
        $teams_id = (array_diff($team_in_same_league_id, $league_teams));
        $favorites = auth()->user()->favoritesTeam->whereIn('id', $teams_id)->pluck('id');
        // Favorite teams, type: team, category_type: favorite_team
        if (count($favorites) > 0) {
            $favorite_category = new \stdClass();
            $favorite_category->total_records = count($favorites);
            $favorite_category->title = 'Favorite teams';
            $favorite_category->category_type = 'favorite_team';
            $favorite_category->type = 'team';
            $favorite_category->data = Team::whereIn('id', $favorites)->take(3)->get();

            $array[] = $favorite_category;
        }

        // Teams I Follow, type: team, category_type: following_team
        $followingTeams = auth()->user()->followingsTeams()->whereIn('follow_id', $teams_id);

        if ($followingTeams->count() > 0) {
            $following_category = new \stdClass();
            $following_category->total_records = $followingTeams->count();
            $following_category->title = 'Teams I Follow';
            $following_category->category_type = 'following_team';
            $following_category->type = 'team';
            $following_category->data = $followingTeams->take(3)->get();

            $array[] = $following_category;
        }

        // NLF Team, type: team, category_type: nlf_team
        $teams_nlf = Team::where('is_nlf', 1)->whereIn('id', $teams_id);

        if ($teams_nlf->count() > 0) {
            $nlf_team_category = new \stdClass();
            $nlf_team_category->total_records = $teams_nlf->count();
            $nlf_team_category->title = 'NLF Team';
            $nlf_team_category->category_type = 'nlf_team';
            $nlf_team_category->type = 'team';
            $nlf_team_category->data = $teams_nlf->take(3)->get();

            $array[] = $nlf_team_category;
        }

        return response_api(true, 200, null, $array);

    }
}
