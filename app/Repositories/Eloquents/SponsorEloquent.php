<?php
/**
 * Created by PhpStorm.
 * UserRequest: mohammedsobhei
 * Date: 5/2/18
 * Time: 11:43 PM
 */

namespace App\Repositories\Eloquents;

use App\Models\Sponsor;
use App\Models\SponsorSocial;
use App\Repositories\Interfaces\Repository;
use App\Models\Sport;
use App\Repositories\Uploader;
use Carbon\Carbon;
use Excel;

class SponsorEloquent extends Uploader implements Repository
{

    private $model;

    public function __construct(Sponsor $model)
    {
        $this->model = $model;

    }

    // for cpanel
    function anyData()
    {

    }

    function export()
    {


    }


    function getAll(array $attributes)
    {

        $page_size = isset($attributes['page_size']) ? $attributes['page_size'] : max_pagination();
        $page_number = isset($attributes['page_number']) ? $attributes['page_number'] : 1;
        $collection = $this->model->where('is_active', 1);

        if (isset($attributes['name'])) {
            $collection = $collection->where('name', 'LIKE', '%' . $attributes['name'] . '%')->orWhere('description', 'LIKE', '%' . $attributes['name'] . '%');
        }

        $count = $collection->count();
        $page_count = page_count($count, $page_size);
        $page_number = $page_number - 1;
        $page_number = $page_number > $page_count ? $page_number = $page_count - 1 : $page_number;

        $object = $collection->take($page_size)->skip((int)$page_number * $page_size)->get();

        if (request()->segment(1) == 'api' || request()->ajax()) {
            return response_api(true, 200, null, $object, $page_count, $page_number, $count);
        }
    }

    function getById($id)
    {

    }

    function create(array $attributes)
    {
        // TODO: Implement create() method.
        $sponsor = new Sponsor();
//        `user_id`, `creator_id`, `name`, `logo`, `description`, `details`,`sponsor_id`, `social_id`, `link`
        if (isset($attributes['user_id']))
            $sponsor->user_id = $attributes['user_id'];
        $sponsor->creator_id = auth()->user()->id;
        $sponsor->name = $attributes['name'];
        $sponsor->description = $attributes['description'];
        if (isset($attributes['details']))
            $sponsor->details = $attributes['details'];
        if ($sponsor->save()) {

            if (isset($attributes['logo'])) {
                $sponsor->logo = $this->storeImageThumb('sponsors', $sponsor->id, $attributes['logo']);
                $sponsor->save();
            }


            if (isset($attributes['social_media_']) && count($attributes['social_media_']) > 0) {
//
                SponsorSocial::where('sponsor_id', $sponsor->id)->forceDelete();
                foreach ($attributes['social_media_'] as $media) {
                    $sponsor_media = new SponsorSocial();
                    $sponsor_media->sponsor_id = $sponsor->id;
                    $sponsor_media->social_id = $media->social_id;
                    $sponsor_media->link = $media->link;
                    $sponsor_media->save();
                }
            }
            $sponsor = $this->model->find($sponsor->id);

            return response_api(true, 200, trans('app.created'), $sponsor);//
        }
        return response_api(false, 422, null, []);

    }

    function update(array $attributes, $id = null)
    {
        // TODO: Implement update() method.
        $sponsor = $this->model->where('creator_id', auth()->user()->id)->find($id);
        if (!isset($sponsor))
            return response_api(false, 422, null, []);

//        `user_id`, `creator_id`, `name`, `logo`, `description`, `details`
        if (isset($attributes['user_id']))
            $sponsor->user_id = $attributes['user_id'];
        $sponsor->creator_id = auth()->user()->id;
        if (isset($attributes['name']))
            $sponsor->name = $attributes['name'];
        if (isset($attributes['description']))
            $sponsor->description = $attributes['description'];
        if (isset($attributes['details']))
            $sponsor->details = $attributes['details'];
        if ($sponsor->save()) {

            if (isset($attributes['logo'])) {
                $sponsor->logo = $this->storeImageThumb('sponsors', $sponsor->id, $attributes['logo']);
                $sponsor->save();
            }

            if (isset($attributes['social_media_']) && count($attributes['social_media_']) > 0) {
//                `sponsor_id`, `social_id`, `link`
                SponsorSocial::where('sponsor_id', $sponsor->id)->forceDelete();
                foreach ($attributes['social_media_'] as $media) {
                    $sponsor_media = new SponsorSocial();
                    $sponsor_media->sponsor_id = $sponsor->id;
                    $sponsor_media->social_id = $media->social_id;
                    $sponsor_media->link = $media->link;
                    $sponsor_media->save();
                }
            }
            $sponsor = $this->model->find($sponsor->id);

            return response_api(true, 200, trans('app.updated'), $sponsor);//
        }
        return response_api(false, 422, null, []);

    }


    function delete($id)
    {
        // TODO: Implement delete() method.
        $sponsor = $this->model->where('creator_id', auth()->user()->id)->find($id);
        if (isset($sponsor) && $sponsor->delete()) {
            return response_api(true, 200, trans('app.deleted'), []);
        }
        return response_api(false, 422, null, []);

    }
}
