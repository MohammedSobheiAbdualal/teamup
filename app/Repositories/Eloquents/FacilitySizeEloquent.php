<?php
/**
 * Created by PhpStorm.
 * UserRequest: mohammedsobhei
 * Date: 5/2/18
 * Time: 11:43 PM
 */

namespace App\Repositories\Eloquents;

use App\Models\FacilitySize;
use App\Repositories\Interfaces\Repository;

class FacilitySizeEloquent implements Repository
{

    private $model;

    public function __construct(FacilitySize $model)
    {
        $this->model = $model;

    }

    // for cpanel
    function anyData()
    {

    }

    function export()
    {


    }

    function getAll(array $attributes)
    {

        // TODO: Implement getAll() method.
        return $this->model->all();
    }

    function getById($id)
    {
        return $this->model->find($id);
    }

    function create(array $attributes)
    {
        // TODO: Implement create() method.

        $facility_size = new FacilitySize();
        $facility_size->title = $attributes['title'];
        if ($facility_size->save())
            return response_api(true, 200, trans('app.created'), $facility_size);
        return response_api(false, 422, null, []);

    }

    function update(array $attributes, $id = null)
    {
        // TODO: Implement update() method.

        $facility_size = $this->model->find($id);
        $facility_size->title = $attributes['title'];
        if ($facility_size->save())
            return response_api(true, 200, trans('app.updated'), $facility_size);
        return response_api(false, 422, null, []);

    }

    function delete($id)
    {
        // TODO: Implement delete() method.
        $facility_size = $this->model->find($id);
        if (isset($facility_size) && $facility_size->delete()) {
            return response_api(true, 200, trans('app.deleted'), $facility_size);
        }
        return response_api(false, 422, null, []);

    }
}
