<?php
/**
 * Created by PhpStorm.
 * UserRequest: mohammedsobhei
 * Date: 5/2/18
 * Time: 11:43 PM
 */

namespace App\Repositories\Eloquents;

use App\Models\Friend;
use App\Models\Team;
use App\Models\User;
use App\Repositories\Interfaces\Repository;
use Excel;

class FriendEloquent implements Repository
{

    private $model, $notification, $user;

    public function __construct(Friend $model, User $user, NotificationSystemEloquent $notificationSystemEloquent)
    {
        $this->model = $model;
        $this->user = $user;
        $this->notification = $notificationSystemEloquent;
    }

    // for cpanel
    function anyData()
    {

    }

    function export()
    {


    }

    function getAll(array $attributes)
    {

        // TODO: Implement getAll() method.
    }

    function getById($id)
    {
        return $this->model->find($id);

    }

    public function getFriends(array $attributes)
    {
        $page_size = isset($attributes['page_size']) ? $attributes['page_size'] : max_pagination();
        $page_number = isset($attributes['page_number']) ? $attributes['page_number'] : 1;

        $collection = $this->user->find($attributes['user_id'])->mergeFriends();

        if (isset($attributes['name'])) {
            $collection = $collection->where('username', 'LIKE', '%' . $attributes['name'] . '%')->orWhereRaw('CONCAT(first_name,last_name)', 'LIKE', '%' . $attributes['name'] . '%');
        }

        $count = $collection->count();
        $page_count = page_count($count, $page_size);
        $page_number = $page_number - 1;
        $page_number = $page_number > $page_count ? $page_number = $page_count - 1 : $page_number;

        $object = $collection->take($page_size)->skip((int)$page_number * $page_size);//->get();

        if (request()->segment(1) == 'api' || request()->ajax()) {
            return response_api(true, 200, null, $object, $page_count, $page_number, $count);
        }

    }


    function createRemoveFriend(array $attributes)
    {
        // TODO: Implement create() method.

        $friend = $this->model->where(function ($query) use ($attributes) {
            $query->where('user_id', auth()->user()->id)->where('friend_id', $attributes['friend_id']);
        })->orWhere(function ($query) use ($attributes) {
            $query->where('user_id', $attributes['friend_id'])->where('friend_id', auth()->user()->id);
        })->first();

        if (isset($friend))
            return $this->delete($friend->id);


        if ($attributes['friend_id'] == auth()->user()->id) { // you can't friend himself
            return response_api(false, 422, null, []);
        }
        $friend = new Friend();
        $friend->user_id = auth()->user()->id;
        $friend->friend_id = $attributes['friend_id'];
        if ($friend->save()) {
            $object = User::find($attributes['friend_id']);

            $this->notification->sendNotification(auth()->user()->id, $object->id, auth()->user()->id, 'friend', 'player', auth()->user()->id, auth()->user()->full_name);
            return response_api(true, 200, trans('app.friend'), $object);

        }//

        return response_api(false, 422, null, []);

    }

    function create(array $attributes)
    {
        // TODO: Implement create() method.

        //  `user_id`, `friend_id`,
        $friend = new Friend();
        $friend->user_id = auth()->user()->id;
        $friend->friend_id = $attributes['friend_id'];
        if ($friend->save())
            return response_api(true, 200, trans('app.friend'), $friend);//

        return response_api(false, 422, null, []);

    }

    function update(array $attributes, $id = null)
    {
        // TODO: Implement update() method.


    }

    function delete($id)
    {
        // TODO: Implement delete() method.
        $friend = $this->model->find($id);

        $user_id = ($friend->friend_id == auth()->user()->id) ? $friend->user_id : $friend->friend_id;
        $deleted = User::find($user_id);
        if (isset($friend) && $friend->delete()) {
            return response_api(true, 200, trans('app.unfriend'), $deleted);
        }
        return response_api(false, 422, null, []);

    }
}
