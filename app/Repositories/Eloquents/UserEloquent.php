<?php
/**
 * Created by PhpStorm.
 * UserRequest: mohammedsobhei
 * Date: 5/2/18
 * Time: 11:43 PM
 */

namespace App\Repositories\Eloquents;

use App\Mail\VerifyMail;
use App\Models\CelebritiesGuest;
use App\Models\DeviceToken;
use App\Models\League;
use App\Models\LeagueTeam;
use App\Models\LeagueTeamPlayer;
use App\Models\Position;
use App\Models\Sponsor;
use App\Models\Team;
use App\Models\TeamPlayer;
use App\Models\UserLocation;
use App\Models\UserSport;
use App\Models\Sport;
//use App\Models\VerifyUser;
use App\Repositories\Interfaces\UserRepository;
use App\Repositories\Uploader;
use App\Models\User;
use Carbon\Carbon;
use DB;
use Excel;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Route;
use Lcobucci\JWT\Parser;
use Mail;
use stdClass;

class UserEloquent extends Uploader implements UserRepository
{

    private $model, $deviceToken, $notificationSystem;

    public function __construct(User $model, DeviceToken $deviceToken, NotificationSystemEloquent $notificationSystemEloquent)
    {
        $this->model = $model;
        $this->deviceToken = $deviceToken;

        $this->notificationSystem = $notificationSystemEloquent;
    }

    // generate access token

    public function login(array $attributes)
    {

        return $this->access_token($attributes['username']);
    }

    public function refresh_token()
    {

        $proxy = Request::create(
            'oauth/token',
            'POST'
        );

        $response = Route::dispatch($proxy);
        $data = json_decode($response->getContent());
        $statusCode = json_decode($response->getStatusCode());

        if (isset($data->error)) {
            return [
                'status' => false,
                'statusCode' => $statusCode,
                'message' => $data->message,
                'items' => []
            ];
        }
        return [
            'status' => true,
            'statusCode' => 200,
            'message' => trans('app.success'),
            'items' => [
                'token_type' => $data->token_type,
                'expires_in' => $data->expires_in,
                'access_token' => $data->access_token,
                'refresh_token' => $data->refresh_token,
            ]
        ];
    }

    // generate refresh token

    function anyData()
    {

        $users = $this->model->orderByDesc('created_at');
        return datatables()->of($users)
            ->filter(function ($query) {
                if (request()->filled('full_name')) {
                    $full_name = request()->get('full_name');
                    $query->where(function ($query) use ($full_name) {
                        $query->where('username', 'LIKE', '%' . $full_name . '%')->orWhereRaw("concat(first_name, ' ', last_name) like '%?%'", $full_name);
                    });
                    // $query->where('username', 'LIKE', '%' .  request()->get('full_name'). '%')->whereRaw("concat(first_name, ' ', last_name) like '%?%'", request()->get('full_name') );
                }
                if (request()->filled('email')) {
                    $query->where('email', 'LIKE', '%' . request()->get('email') . '%');
                }

                if (request()->filled('is_active')) {
                    $query->where('is_active', request()->get('is_active'));
                }

            })
            ->editColumn('avatar100', function ($user) {
                $image=$user->avatar? $user->avatar100 : url('assets/img/player.svg');
                return '<img src="'.$image.'" alt="Empty Image" class="img-circle">';
            })
            ->addColumn('full_name', function ($users) {
                return $users->first_name ? $users->first_name . ' ' . $users->last_name : $users->username;
            })
            ->editColumn('has_league',function($user){
                $checked='';
                if($user->has_league)
                    $checked='checked="checked"';
                 return '<div class="col-3"><span class="m-switch m-switch--sm m-switch--icon"><label><input type="checkbox"'.$checked .'name="has_league" class="has_league" data-id="'.$user->id.'"><span></span></label></span></div>';
             })
            ->editColumn('is_active', function ($users) {
                return ($users->is_active == 1) ? '<span class="m-badge m-badge--success m-badge--wide">active</span>' : '<span class="m-badge m-badge--danger m-badge--wide">inactive</span>';
            })->addColumn('action', function ($user) {
                $checked='';
                if($user->is_active)
                    $checked='checked="checked"';
                $activate= '<span class="m-switch m-switch m-switch--outline m-switch--icon m-switch--danger m-switch--danger" style="margin-left:3px;vertical-align: middle;"><label><input type="checkbox"'.$checked .'name="is_active" class="is_active" data-id="'.$user->id.'"><span></span></label></span>';

                return '<a href="' . url(admin_manage_url() . '/user-details/' . $user->id) . '" class="btn btn-primary m-btn m-btn--icon m-btn--icon-only m-btn--pill" title="User details"><i class="fa flaticon-medical"></i></a>' . $activate;
            })->addIndexColumn()
            ->rawColumns(['is_active', 'action', 'avatar100','has_league'])->toJson();
    }

    // for cpanel

    function export()
    {
    }

    function userActive(array $attributes)
    {

        $user = $this->model->find($attributes['user_id']);

        if (isset($user)) {
            $user->is_active = $attributes['is_active'];

            if ($user->save()) {
                if (!$user->is_active) {
                    //$action = 'user_deactivate';
                    //$this->notificationSystem->sendNotification(null, $user->id,'player', $user->id, $action, 'player');
                    $this->logout($user->id);
                    return response_api(true, 200);

                }
                return response_api(true, 200);
            }
        }
        return response_api(false, 422);

    }

    public function logout($user_id = null)
    {
        if (!isset($user_id)) {
            $user_id = auth()->user()->id;

            $value = \request()->bearerToken();
            $id = (new Parser())->parse($value)->getHeader('jti');
            $token = DB::table('oauth_access_tokens')
                ->where('id', '=', $id)
                ->update(['revoked' => true]);
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $id)
                ->update(['revoked' => true]);
        } else {
            $access_token_id = DB::table('oauth_access_tokens')
                ->where('user_id', '=', $user_id)->pluck('id');

            $token = DB::table('oauth_access_tokens')
                ->where('user_id', '=', $user_id)
                ->update(['revoked' => true]);

            DB::table('oauth_refresh_tokens')
                ->whereIn('access_token_id', $access_token_id)
                ->update(['revoked' => true]);
        }
        // token device
        // turn off mobile // registerId : mac address code
        $device_reset = false;
        if (\request()->filled('device_id'))
            $device_reset = $this->deviceToken->where('user_id', $user_id)->where('device_id', \request()->get('device_id'))->update(['status' => 'off']);
        if (\request()->filled('device_type'))
            $device_reset = $this->deviceToken->where('user_id', $user_id)->where('device_type', \request()->get('device_type'))->update(['status' => 'off']);

        if (!$device_reset)
            $this->deviceToken->where('user_id', $user_id)->update(['status' => 'off']);

        if ($token)
            return response_api(true, 200, null, []);
        return response_api(false, 422, null, []);
    }

    function verifyEmail($id)
    {

        $user = $this->model->find($id['user_id']);

        if (isset($user)) {

            if (!isset($user->email_verified_at))
                $user->email_verified_at = Carbon::now();
            else
                $user->email_verified_at = null;

            if ($user->save()) {
                return response_api(true, 200);
            }
        }
        return response_api(false, 422);

    }


    // get all users

    function getAll(array $attributes)
    {
        // TODO: Implement getAll() method.

        $page_size = isset($attributes['page_size']) ? $attributes['page_size'] : max_pagination();
        $page_number = isset($attributes['page_number']) ? $attributes['page_number'] : 1;
        $collection = $this->model->where('is_active', 1)->where('id', '<>', auth()->user()->id);
        $ids = $collection->where('signup_level', 3)->pluck('id');

        $collection = $collection->whereIn('id', $ids);


        if (isset($attributes['team_id'])) {

            $team = Team::find($attributes['team_id']);
            $players_id = TeamPlayer::where('status', 'accepted')->where('team_id', $attributes['team_id'])->pluck('user_id')->toArray();
            $players_id = UserSport::whereNotIn('user_id', $players_id)->where('sport_id', $team->sport_id)->pluck('user_id')->toArray();
            $collection = $collection->whereIn('id', $players_id);
            /*


        $team = Team::find($attributes['action_id']);
        if (!isset($team))
            return response_api(true, 200, null, $array);

        $user_in_same_team_id = UserSport::where('sport_id', $team->sport_id)->whereIn('user_id', $active_user_ids)->pluck('user_id')->toArray();
        $team_players = isset($team->players) ? $team->players->where('status', 'accepted')->whereIn('id', $active_user_ids)->pluck('id')->toArray() : [];

        $players_id = (array_diff($user_in_same_team_id, $team_players));

            */
        }
        if (isset($attributes['name'])) {
            $collection = $collection->where('username', 'LIKE', '%' . $attributes['name'] . '%')->orWhereRaw('CONCAT(first_name,last_name)', 'LIKE', '%' . $attributes['name'] . '%');
        }

        if (isset($attributes['category_type'])) {

//            $team = Team::find($attributes['team_id']);
//            $user_in_same_team_id = UserSport::where('sport_id', $team->sport_id)->pluck('user_id')->toArray();
//            $team_players = isset($team->players) ? $team->players->pluck('id')->toArray() : [];
//
//            $players_id = (array_diff($user_in_same_team_id, $team_players));

            // Friends, type: player, category_type: friend
            if ($attributes['category_type'] == 'friend') {

                $friends = auth()->user()->mergeFriends()->whereIn('id', $players_id)->pluck('id');
                if (count($friends) > 0) {
                    $collection = $collection->whereIn('id', $friends);
                }


            }

            // Favorite players, type: player, category_type: favorite_player
            if ($attributes['category_type'] == 'favorite_player') {
                $favorites = auth()->user()->favoritesUser->whereIn('id', $players_id)->pluck('id');
                if (count($favorites) > 0) {
                    $collection = $collection->whereIn('id', $favorites);
                }
            }

            // Team mates, type: team, category_type: team_mate

            if ($attributes['category_type'] == 'team_mate') {
                if (auth()->user()->my_teams->count() > 0) {

                    $my_teams_id = TeamPlayer::where('user_id', auth()->user()->id)->where('team_players.status', 'accepted')->pluck('team_players.team_id');
                    if (isset($attributes['category_team_id']))
                        $team_mate = Team::where(function ($query) use ($my_teams_id) {
                            $query->where('user_id', auth()->user()->id)->orWhereIn('id', $my_teams_id);
                        })->where('status', 'published')->find($attributes['category_team_id']);

                    if (isset($team_mate) && isset($team_mate->players)) {
                        $players_list = $team_mate->players->pluck('id');
                    } else {
                        $players_list = [];
                    }
                    $collection = $collection->whereIn('id', $players_list);
                }
            }
            // Top players, type: player, category_type: top_player

            // Nearby Players, type: player, category_type: near_by_player default 5K
            if ($attributes['category_type'] == 'near_by_player') {
                $user_locations = UserLocation::where('user_id', '<>', auth()->user()->id)->get();
                $players = [];
                if (isset(auth()->user()->Location))
                    foreach ($user_locations as $location) {
                        $user = User::where('is_active', 1)->find($location->user_id);
                        if (!isset($user) || $user->signup_level != 3) continue;
                        $distance = distance($location->latitude, $location->longitude, auth()->user()->Location->latitude, auth()->user()->Location->longitude);
                        if ($distance <= 5000) {
                            $players[] = $location->user_id;
                        }
                    }

                if (count($players) > 0) {

                    $players = array_intersect($players_id, $players);

                    $collection = $collection->whereIn('id', $players);
                }
            }
            // Previous Players, type: matches, category_type: previous_players
            // Players I Follow, type: player, category_type: following
            if ($attributes['category_type'] == 'following') {
                $followingPlayers = auth()->user()->followings()->whereIn('follow_id', $players_id)->where('type', 'player');
                if ($followingPlayers->count() > 0) {
                    $collection = $collection->whereIn('id', $followingPlayers->pluck('follow_id'));
                }
            }

            // Followers, type: player, category_type: follower
            if ($attributes['category_type'] == 'follower') {
                $followerPlayers = auth()->user()->followers()->whereIn('user_id', $players_id)->where('type', 'player');
                if ($followerPlayers->count() > 0) {
                    $collection = $collection->whereIn('id', $followerPlayers->pluck('user_id'));
                }
            }

            // In a Team, type: player, category_type: in_team

            // Played In a League, type: player, category_type: played_league

            // NLF League, type: player, category_type: nlf_league
            if ($attributes['category_type'] == 'nlf_league') {

                $leagues_nlf = League::where('is_nlf', 1)->where('status', 'published')->where('is_active', 1)->pluck('id');

                $league_teams_id = LeagueTeam::whereIn('league_id', $leagues_nlf)->where('status', 'accepted')->pluck('id');
                $nlfLeague = LeagueTeamPlayer::whereIn('league_team_id', $league_teams_id)->whereIn('player_id', $players_id);

                $players_id_ = $nlfLeague->pluck('player_id')->unique()->toArray();

                if (count($players_id_) > 0) {
                    $collection = $collection->whereIn('id', $players_id_);
                }
            }

        }


        if (isset($attributes['exclude_sponsor']) && $attributes['exclude_sponsor']) {

            $users_sponsor_id = Sponsor::whereNotNull('user_id')->pluck('user_id')->unique()->toArray();
            $collection = $collection->whereNotIn('id', $users_sponsor_id);

        }
        if (isset($attributes['exclude_guest']) && $attributes['exclude_guest']) {

            $users_guest_id = CelebritiesGuest::whereNotNull('user_id')->pluck('user_id')->unique()->toArray();
            $collection = $collection->whereNotIn('id', $users_guest_id);

        }
        $count = $collection->count();

        if ($attributes['is_all']) {
            return response_api(true, 200, null, $collection->get(), 1, 0, $count);
        }

        $page_count = page_count($count, $page_size);
        $page_number = $page_number - 1;
        $page_number = $page_number > $page_count ? $page_number = $page_count - 1 : $page_number;

        $object = $collection->take($page_size)->skip((int)$page_number * $page_size)->get();

        if (request()->segment(1) == 'api' || request()->ajax()) {

            return response_api(true, 200, null, $object, $page_count, $page_number, $count);
        }
    }

    //search for trainer in list of trainers

    function getByEmail($email)
    {
        // TODO: Implement getAll() method.
        return $this->model->where('email', $email)->first();
    }

    //user profile view

    function getById($id)
    {
        // TODO: Implement getById() method.
        if (!isset($id) && auth()->check())
            $user = auth()->user();
        else
            $user = $this->model->find($id);

        if (\request()->segment(1) == 'api' || \request()->ajax()) {
            if (isset($user)) {
                return response_api(true, 200, null, $user);
            }
            return response_api(false, 422, trans('app.not_data_found'), []);
        }
        return $user;
    }

    // sign up user

    function create(array $attributes)
    {
        // TODO: Implement create() method.
        $user = new User();
        $user->username = $attributes['username'];
        $user->mobile = $attributes['mobile'];
        $user->password = bcrypt($attributes['password']);
        $user->country_code = $attributes['country_code'];
        $user->verification_code = 1234;
        $user->lang_id = config()->get('app.lang_id');
        if ($user->save()) {

            if (\request()->has('device_type')) {
                $device = $this->deviceToken->where('user_id', $user->id)->where('device_id', \request()->get('device_id'))->where('type', \request()->get('device_type'))->first();

                if (!isset($device))
                    // register device token
                    $device = new DeviceToken();
                $device->user_id = $user->id;

                if (\request()->has('device_id'))
                    $device->device_id = \request()->get('device_id');
                $device->device_token = \request()->get('device_token');
                $device->type = \request()->get('device_type');
                $device->status = 'off';
                $device->save();
            }
            $text = 'your verification code is: ' . $user->verification_code;
            send_sms($user->mobile, $text);
//            return $this->access_token();
            $user_data = $this->model->find($user->id);
            return response_api(true, 200, trans('app.user_created') . ',' . trans('app.sent_code_verification'), $user_data);//
        }
        return response_api(false, 422, null, []);

    }

    function putMobile(array $attributes)
    {
        $user = $this->model->find(auth()->user()->id);
        if ($user) {
            if ($user->verification_code != $attributes['code'])
                return response_api(false, 422, trans('app.code'), []);

            $user->mobile = $attributes['mobile'];
            $user->country_code = $attributes['country_code'];
            $user->is_verified = 1;
            $user->save();
            return response_api(true, 200, trans('app.updated'), $user);//

        } else {
            return response_api(false, 422, trans('app.not_data_found'), []);
        }
    }

    //update user

    function verifyUser(array $attributes)
    {

        if (!isset($attributes['code'])) {
            return response_api(false, 422, trans('app.code'), []);
        }

        $user = $this->model->find($attributes['user_id']);

        if (!$user) {
            return response_api(false, 422, trans('app.user_notfound'), []);
        }

//        if ($user->country_code.$user->mobile != $attributes['mobile']) {
//            return response_api(false, 422, 'Incorrect Mobile', []);
//        }

        if ($user->verification_code == $attributes['code']) {

            $user->is_verified = 1;
            $user->country_code = $attributes['country_code'];
            $user->mobile = $attributes['mobile'];
            $user->lang_id = config()->get('app.lang_id');

            $user->save();

            return $this->access_token($user->username);
        } else
            return response_api(false, 422, 'Incorrect code', []);

    }

    public function access_token($username)
    {
        \request()->request->add(['username' => $username]);
        $proxy = Request::create(
            'oauth/token',
            'POST'
        );

        $response = Route::dispatch($proxy);
        $token_obj = json_decode($response->getContent());
        $statusCode = json_decode($response->getStatusCode());


        if (isset($token_obj->error)) {
            return [
                'status' => false,
                'statusCode' => $statusCode,
                'message' => $token_obj->message,
                'items' => []
            ];
        }

        if (!isset($token_obj->access_token))
            return [
                'status' => false,
                'statusCode' => 422,
                'message' => 'error',
                'items' => []
            ];
        \request()->headers->set('Accept', 'application/json');
        \request()->headers->set('Authorization', 'Bearer ' . $token_obj->access_token);
//

        $request = \request()->create(
            'api/v1/user',
            'GET'
        );
//
        $response = Route::dispatch($request);

        $data = json_decode($response->getContent());

        $statusCode = json_decode($response->getStatusCode());

        if ($statusCode == 200) {
            $user = $data->items;

        }

        if (!isset($user)) {
            return response_api(false, 422, trans('auth.failed'), []);
        }

        if (!$user->is_active)
            return response_api(false, 422, 'This account is not active', []);

        if (!($user->is_verified))
            return [
                'status' => true,
                'statusCode' => 200,
                'message' => 'Your account is not verified.',
                'items' => ['token' => null, 'user' => $user]
            ];

        $user = $this->model->find($user->id);
        //Update last login
        $user->last_login_date = Carbon::now()->toDateTimeString();
        $user->lang_id = config()->get('app.lang_id');
        $user->save();
        $token = new stdClass();
//
        $token->token_type = $token_obj->token_type;
        $token->expires_in = $token_obj->expires_in;
        $token->access_token = $token_obj->access_token;
        $token->refresh_token = $token_obj->refresh_token;


        if (\request()->filled('device_type')) {
            $device = $this->deviceToken->where('user_id', $user->id)->where('device_id', \request()->get('device_id'))->first();
            if (!isset($device))
                // register device token
                $device = new DeviceToken();
            $device->user_id = $user->id;

            if (\request()->filled('device_id'))
                $device->device_id = \request()->get('device_id');
            $device->device_token = \request()->get('device_token');
            $device->type = \request()->get('device_type');
            $device->status = 'on';

            $device->save();

        }
//        $user->signup_level = $this->get_signup_step($user->id);
        if ($user->sports()->count() > 0) {
            foreach ($user->sports as $sport) {

                if ($sport->pivot->primary_position_id) {

                    $sport->primary_position = Position::find($sport->pivot->primary_position_id);
                }
                if ($sport->pivot->secondary_position_id) {

                    $sport->secondary_position = Position::find($sport->pivot->secondary_position_id);
                }
            }
        }
        return [
            'status' => true,
            'statusCode' => 200,
            'message' => trans('app.success'),
            'items' => ['token' => $token, 'user' => $user]
        ];
    }

    // delete user
    function update(array $attributes, $id = null)
    {
        // TODO: Implement create() method.

        $user = isset($id) ? $this->model->find($id) : auth()->user();
        $message = trans('app.user_updated');


        if (isset($attributes['first_name'])) {
            $user->first_name = $attributes['first_name'];
        }
        if (isset($attributes['last_name']))
            $user->last_name = $attributes['last_name'];

        if (isset($attributes['bio']))
            $user->bio = $attributes['bio'];


//        if (!empty($attributes['email']))
//            $user->email = $attributes['email'];
//        else

        if (array_key_exists('email', $attributes)) {
            $user->email = $attributes['email'];
        }

        if (array_key_exists('birthdate', $attributes)) {
            if (!empty($attributes['birthdate']))
                $user->birthdate = Carbon::parse($attributes['birthdate'])->format('Y-m-d');
            else
                $user->birthdate = null;

        }

        if ($user->signup_level == 1) {
            $user->signup_level = 2;
        }

        if (isset($attributes['city_id']))
            $user->city_id = $attributes['city_id'];

        if (isset($attributes['password'])) {

            if (Hash::check($attributes['old_password'], $user->password)) {
                $user->password = bcrypt($attributes['password']);
//                $user->last_password_change_date = Carbon::now()->toDateTimeString();
                $message = trans('app.password_updated');
            } else {
                return response_api(false, 422, trans('app.password_not_match'), []);
            }

        }


        if (isset($attributes['avatar'])) {

            if (isset($user->avatar) && $user->avatar != '') {

                $this->deleteImage('users', $user->getOriginal('avatar'));
            }
            $user->avatar = $this->storeImageThumb('users', $user->id, $attributes['avatar']);
        }

        sleep(1);
        if (isset($attributes['bg_image'])) {

            if (isset($user->bg_image) && $user->bg_image != '') {
                $this->deleteImage('users', $user->getOriginal('bg_image'));
            }
            $user->bg_image = $this->storeImageThumb('users', $user->id, $attributes['bg_image']);
        }

        $user->lang_id = config()->get('app.lang_id');

        if ($user->save()) {
            $user = $this->model->find($user->id);
            if ($user->Sports()->count() > 0) {
                foreach ($user->sports as $sport) {

                    if ($sport->pivot->primary_position_id) {

                        $sport->primary_position = Position::find($sport->pivot->primary_position_id);
                    }
                    if ($sport->pivot->secondary_position_id) {

                        $sport->secondary_position = Position::find($sport->pivot->secondary_position_id);
                    }
                }
            }

            return response_api(true, 200, $message, $user);
        }
        return response_api(false, 422, null, []);

    }

    // logout
    function delete($id)
    {
        // TODO: Implement delete() method.
        $user = $this->model->find($id);
        return isset($user) && $user->delete();
    }


    // count users
    function count()
    {
        return $this->model->count();
    }

    function resendCode(array $attributes)
    {

        $user = $this->model->find($attributes['user_id']);

        if (!$user)
            return response_api(false, 422, trans('app.not_found_user'), []);

        $check_mobile_is_exist = User::where('id', '<>', $attributes['user_id'])->whereRaw('CONCAT(country_code,mobile) =?', $attributes['mobile'])->first();
        if ($check_mobile_is_exist)
            return response_api(false, 422, trans('app.mobile_exist'), []);

//        $user->is_verified = 0;
//        $user->save();
        $code = '1234';
        $text = 'your verification code is: ' . $code;
        send_sms($attributes['mobile'], $text);
        return response_api(true, 200, trans('app.sent_code_verification'), []);

    }

    function postUserSports(array $attributes, $id = null)
    {
        if (count($attributes['sports']) > 3 || count($attributes['sports']) == 0)
            return response_api(false, 422, trans('app.max_sport'), []);
        $user = auth()->user();
        if ($user->Sports()->count() > 0) {
            $user->Sports()->detach();
        }


        foreach ($attributes['sports'] as $sport) {
            $sports = Sport::find($sport['sport_id']);

            if ($sports->has_position) {
                if (!isset($sport['positions']['primary_position']) || !isset($sport['positions']['secondary_position']))
                    return response_api(false, 422, trans('app.required_positions'), []);
            }

            $user_sport = new UserSport();
            $user_sport->user_id = $user->id;

            if ($sports->has_position && isset($sport['positions']) && !empty($sport['positions'])) {

                $user_sport->sport_id = $sport['sport_id'];

                if (isset($sport['positions']['primary_position']) && $sport['positions']['primary_position'] != '') {
                    $position = Position::where('sport_id', $sport['sport_id'])->find($sport['positions']['primary_position']);

                    if (!$position)
                        return response_api(false, 422, trans('app.not_position'), []);

                    $user_sport->primary_position_id = $sport['positions']['primary_position'];

                }

                if (isset($sport['positions']['secondary_position']) && $sport['positions']['secondary_position'] != '') {
                    $position = Position::where('sport_id', $sport['sport_id'])->find($sport['positions']['secondary_position']);
                    if (!$position)
                        return response_api(false, 422, trans('app.not_position'), []);
                    $user_sport->secondary_position_id = $sport['positions']['secondary_position'];
                }

                $user_sport->save();

            } else {
                $user_sport->sport_id = $sport['sport_id'];
                $user_sport->save();
            }

        }
//        $user->signup_level = $this->get_signup_step($user->id);
//        $user->save();
        if ($user->sports()->count() > 0) {
            foreach ($user->sports as $sport) {

                if ($sport->pivot->primary_position_id) {

                    $sport->primary_position = Position::find($sport->pivot->primary_position_id);
                }
                if ($sport->pivot->secondary_position_id) {

                    $sport->secondary_position = Position::find($sport->pivot->secondary_position_id);
                }
            }
        }
        $user->lang_id = config()->get('app.lang_id');
        $user->signup_level = 3;
        $user->save();
        return response_api(true, 200, trans('app.success'), $user);
    }

    public function forget(array $attributes)
    {
        if (filter_var($attributes['email'], FILTER_VALIDATE_EMAIL)) {
//            $emailErr = "Invalid email format";
//        }
//        if (!is_numeric($attributes['email'])) {
            $user = $this->model->where('email', $attributes['email'])->first();

            if (isset($user)) {
                $response = Password::sendResetLink($attributes);

                switch ($response) {
                    case Password::RESET_LINK_SENT:
                        return response_api(true, 200, 'Email was sent', []);
                    case Password::INVALID_USER:
                        return response_api(false, 422, 'Send reset password was failed', []);
                }
                return response_api(false, 422, 'Send reset password was failed', []);
            } else {
                return response_api(false, 422, trans('app.mobile_email_not_found'), []);
            }
        } else {
            $user = $this->model->whereRaw('CONCAT(country_code,mobile) =?', $attributes['email'])->first();
            if ($user) {
                $password = '123456';
                $text = 'New Password is: ' . $password;
                send_sms($attributes['email'], $text);
                $user->password = bcrypt($password);
                $user->save();
                return response_api(true, 200, trans('app.sent_forget_sms'), []);
            } else {
                return response_api(false, 422, trans('app.mobile_email_not_found'), []);
            }
        }
    }

    public function hasLeague(array $attributes){
       $user= $this->model->find($attributes['user_id']);
       $user->has_league=$attributes['is_league'];
       $user->save();
        return response_api(true, 200);
    }

}
