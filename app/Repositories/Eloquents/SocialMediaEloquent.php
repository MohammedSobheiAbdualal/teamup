<?php
/**
 * Created by PhpStorm.
 * UserRequest: mohammedsobhei
 * Date: 5/2/18
 * Time: 11:43 PM
 */

namespace App\Repositories\Eloquents;

use App\Models\SocialMedia;
use App\Repositories\Interfaces\Repository;

class SocialMediaEloquent implements Repository
{

    private $model;

    public function __construct(SocialMedia $model)
    {
        $this->model = $model;

    }

    function getAll(array $attributes)
    {

        // TODO: Implement getAll() method.

        $socials = $this->model->where('status', 1)->get();
        if (request()->segment(1) == 'api') {
            return response_api(true, 200, null, $socials);
        }
        return $socials;
    }

    function getById($id)
    {
        if (request()->segment(1) == 'api') {
            // TODO: Implement getById() method.
            $social = $this->model->find($id);
            if (isset($social))
                return response_api(true, 200, null, $social);
            return response_api(false, 422, trans('app.not_data_found'), []);
        }
        return $this->model->find($id);

    }


    function create(array $attributes)
    {
        // TODO: Implement create() method.
    }

    function update(array $attributes, $id = null)
    {
        // TODO: Implement update() method.
    }

    function delete($id)
    {
        // TODO: Implement delete() method.
    }
}