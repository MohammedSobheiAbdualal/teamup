<?php
/**
 * Created by PhpStorm.
 * UserRequest: mohammedsobhei
 * Date: 5/2/18
 * Time: 11:43 PM
 */

namespace App\Repositories\Eloquents;

use App\Models\CelebritiesGuest;
use App\Repositories\Interfaces\Repository;
use App\Repositories\Uploader;
use Excel;

class CelebritiesGuestEloquent extends Uploader implements Repository
{

    private $model;

    public function __construct(CelebritiesGuest $model)
    {
        $this->model = $model;

    }

    // for cpanel
    function anyData()
    {

    }

    function export()
    {


    }

    function getAll(array $attributes)
    {

        $page_size = isset($attributes['page_size']) ? $attributes['page_size'] : max_pagination();
        $page_number = isset($attributes['page_number']) ? $attributes['page_number'] : 1;
        $collection = $this->model;

        if (isset($attributes['guest_name'])) {
            $collection = $collection->where('guest_name', 'LIKE', '%' . $attributes['guest_name'] . '%');
        }

        $count = $collection->count();
        $page_count = page_count($count, $page_size);
        $page_number = $page_number - 1;
        $page_number = $page_number > $page_count ? $page_number = $page_count - 1 : $page_number;

        $object = $collection->take($page_size)->skip((int)$page_number * $page_size)->get();

        if (request()->segment(1) == 'api' || request()->ajax()) {
            return response_api(true, 200, null, $object, $page_count, $page_number, $count);
        }
    }


    function getById($id)
    {
        return $this->model->find($id);
    }

    function create(array $attributes)
    {
        // TODO: Implement create() method.

        $guest = new CelebritiesGuest();
        if (isset($attributes['user_id']))
            $guest->user_id = $attributes['user_id'];
        $guest->creator_id = auth()->user()->id;
        if (isset($attributes['guest_name']))
            $guest->guest_name = $attributes['guest_name'];
        if (isset($attributes['description']))
            $guest->description = $attributes['description'];
        if ($guest->save()) {
            if (isset($attributes['guest_picture'])) {
                $guest->guest_picture = $this->storeImageThumb('celebrities_guest', $guest->id, $attributes['guest_picture']);
                $guest->save();
            }

            $guest = $this->model->find($guest->id);
            return response_api(true, 200, trans('app.created'), $guest);//
        }
        return response_api(false, 422, null, []);

    }

    function update(array $attributes, $id = null)
    {
        // TODO: Implement update() method.
        $guest = $this->model->where('creator_id', auth()->user()->id)->find($id);
        if (!isset($guest))
            return response_api(false, 422, null, []);
        if (isset($attributes['user_id']))
            $guest->user_id = $attributes['user_id'];
        $guest->creator_id = auth()->user()->id;
        if (isset($attributes['guest_name']))
            $guest->guest_name = $attributes['guest_name'];
        if (isset($attributes['description']))
            $guest->description = $attributes['description'];
        if ($guest->save())
            if (isset($attributes['guest_picture'])) {
                $guest->guest_picture = $this->storeImageThumb('celebrities_guest', $guest->id, $attributes['guest_picture']);
                $guest->save();
            }
        $guest = $this->model->find($guest->id);
        return response_api(true, 200, trans('app.updated'), $guest);//
    }

    function delete($id)
    {
        // TODO: Implement delete() method.
        $guest = $this->model->where('creator_id', auth()->user()->id)->find($id);
        if (isset($guest) && $guest->delete()) {
            return response_api(true, 200, trans('app.deleted'), []);
        }
        return response_api(false, 422, null, []);

    }
}
