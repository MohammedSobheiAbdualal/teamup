<?php
/**
 * Created by PhpStorm.
 * UserRequest: mohammedsobhei
 * Date: 5/2/18
 * Time: 11:43 PM
 */

namespace App\Repositories\Eloquents;

use App\Language;
use App\Repositories\Interfaces\Repository;
use App\UserLanguage;
class LanguageEloquent implements Repository
{

    private $model, $userLanguage;

    public function __construct(Language $model, UserLanguage $userLanguage)
    {

        $this->model = $model;
        $this->userLanguage = $userLanguage;

    }

    function getAll(array $attributes)
    {

        // TODO: Implement getAll() method.
        $object = $this->model->orderBy('name', 'ASC')->get();
        if (request()->segment(1) == 'api' || request()->ajax()) {
            if (count($object) > 0) {
                return response_api(true, 200, null, $object);
            }
            return response_api(false, 422, trans('app.not_data_found'), []);
        }
        return $object;
    }

    function getById($id)
    {

    }


    function create(array $attributes)
    {
        $object = new Language();
        $object->name = $attributes['name'];
        if ($object->save())
            return response_api(true, 200, trans('app.language_created'), $object);
        return response_api(false, 422, null, []);
    }

    function update(array $attributes, $id = null)
    {
        // TODO: Implement update() method.
        $object = $this->model->find($id);
        if (isset($attributes['name']))
            $object->name = $attributes['name'];
        if ($object->save())
            return response_api(true, 200, trans('app.language_updated'), $object);
        return response_api(false, 422, null, []);
    }

    function delete($id)
    {
        // TODO: Implement delete() method.
        $object = $this->model->find($id);
        if (isset($object) && $object->delete())
            return response_api(true, 200, trans('app.language_deleted'), []);
        return response_api(false, 422, null, []);
    }

    //UserLanguage
    function getUserLanguages()
    {
        $langs_id = $this->userLanguage->where('user_id', auth()->user()->id)->pluck('lang_id');
        $objects = $this->model->whereIn('id', $langs_id)->get();
        return response_api(true, 200, null, $objects);

    }

    function createUserLanguage(array $attributes)
    {
        $object = $this->userLanguage->where('user_id', auth()->user()->id)->where('lang_id', $attributes['lang_id'])->first();
        if (!isset($object)) {
            $object = new UserLanguage();
            $object->user_id = auth()->user()->id;
            $object->lang_id = $attributes['lang_id'];
            if ($object->save())
                return response_api(true, 200, trans('app.language_user_created'), $object);
        }
        return response_api(false, 422, trans('app.language_user_exist'), []);
    }

    function deleteUserLanguage($id)
    {
        // TODO: Implement delete() method.
        $object = $this->userLanguage->where('user_id', auth()->user()->id)->where('lang_id', $id)->first();
        if (isset($object) && $object->delete())
            return response_api(true, 200, trans('app.language_user_deleted'), []);
        return response_api(false, 422, null, []);
    }
}