<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sponsor extends Model
{
    //
    use SoftDeletes;

    protected $hidden = ['pivot'];
    protected $appends = ['user', 'logo100', 'logo300', 'social_media', 'is_main'];

    public function SocialMedia()
    {
        return $this->hasMany(SponsorSocial::class, 'sponsor_id', 'id');
    }

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id');
    }


    public function getLogo100Attribute()
    {

        if ($this->getOriginal('logo') != null)
            return url('storage/app/sponsors/' . $this->id) . '/100/' . $this->getOriginal('logo');
        if (isset($this->user_id)) {
            $user = $this->User()->first();
            return $user->avatar100;
        }
        return null;
    }

    public function getLogo300Attribute()
    {

        if ($this->getOriginal('logo') != null)
            return url('storage/app/sponsors/' . $this->id) . '/300/' . $this->getOriginal('logo');
        if (isset($this->user_id)) {
            $user = $this->User()->first();
            return $user->avatar300;
        }
        return null;
    }

    function getLogoAttribute($value)
    {

        if (isset($value))
            return url('storage/app/sponsors/' . $this->id) . '/' . $value;
        if (isset($this->user_id)) {
            $user = $this->User()->first();
            return $user->avatar;
        }
        return null;
    }


    public function getSocialMediaAttribute()
    {
        return $this->SocialMedia()->get();
    }

    public function getUserAttribute()
    {
        return $this->User()->first();
    }

    function getIsMainAttribute()
    {
        if (isset($this->pivot))
            return $this->pivot->is_main;
        return 0;
    }

    function getNameAttribute($value)
    {
        if (isset($this->user_id)) {
            $user = $this->User()->first();
            return $user->first_name . ' ' . $user->last_name;
        }
        return $value;
    }


}
