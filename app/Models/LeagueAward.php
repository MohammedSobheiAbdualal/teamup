<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeagueAward extends Model
{
    //
    use SoftDeletes;

    protected $appends = ['award_name'];

    public function Award()
    {
        return $this->belongsTo(Award::class, 'award_id');
    }

    public function getAwardNameAttribute()
    {
        $award = $this->Award()->first();
        return (isset($award)) ? $award->title : $this->other_award;
    }

}
