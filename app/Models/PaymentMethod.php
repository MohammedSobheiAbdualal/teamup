<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentMethod extends Model
{
    //
    use SoftDeletes;

    protected $appends = ['icon', 'title', 'description'];

    public function getIconAttribute()
    {
        return url('assets/icon/' . $this->type . '.png');
    }

    public function getTitleAttribute()
    {
        if (request()->hasHeader('lang') && request()->header('lang') == 'ar') {
            return $this->title_ar;
        }
        return $this->title_en;
    }

    public function getDescriptionAttribute()
    {
        if (request()->hasHeader('lang') && request()->header('lang') == 'ar') {
            return $this->description_ar;
        }
        return $this->description_en;
    }
}
