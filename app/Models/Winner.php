<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Winner extends Model
{
    //
    use SoftDeletes;
    protected $appends = ['title'];

    public function getTitleAttribute()
    {
        if (request()->hasHeader('lang') && request()->header('lang') == 'ar') {
            return $this->title_ar;
        }
        return $this->title_en;
    }

}
