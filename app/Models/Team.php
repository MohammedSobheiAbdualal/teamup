<?php

namespace App\Models;

use App\Http\Resources\SportResource;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use phpDocumentor\Reflection\Types\Null_;

class Team extends Model
{
    //
    use SoftDeletes;

<<<<<<< HEAD
    protected $appends = ['league_team_phone', 'invites_num', 'requests_num', 'accepts_num', 'invites_league_num', 'requests_league_num', 'my_leagues_num', 'rate', 'rate_num', 'my_status', 'is_favorite', 'is_follow', 'followers_num', 'won_percent', 'total_players', 'total_awards', 'logo100', 'logo300', 'bg_image100', 'bg_image300', 'owner', 'manager', 'top_players', 'players', 'city', 'sport', 'sponsors'];
=======
    protected $appends = ['invites_num', 'requests_num', 'accepts_num', 'invites_league_num', 'requests_league_num', 'my_leagues_num', 'rate', 'rate_num', 'my_status', 'is_favorite', 'is_follow', 'followers_num', 'won_percent', 'total_players', 'total_awards', 'logo100', 'logo300', 'bg_image100', 'bg_image300', 'owner', 'manager', 'top_players', 'players', 'city', 'sport', 'sponsors'];
>>>>>>> Cpanel

    protected $user_hidden = ['email', 'email_verified_at', 'birthdate',
        'mobile', 'bio', 'country_code', 'bg_image', 'lang_id', 'city_id', 'verification_code', 'is_active', 'is_verified', 'last_login_date',
        'deleted_at', 'created_at', 'updated_at'];

    public function Sponsors()
    {
        return $this->belongsToMany(Sponsor::class, 'team_sponsors', 'team_id', 'sponsor_id', 'id', 'id')->withPivot('is_main');
    }

    public function City()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function Sport()
    {
        return $this->belongsTo(Sport::class, 'sport_id');
    }

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    // One manager for each team
    public function Manager()
    {
        return $this->belongsTo(User::class, 'manager_id');
    }

    public function Leagues()
    {
        return $this->belongsToMany(League::class, 'league_teams', 'team_id', 'league_id')->whereNull('league_teams.deleted_at');
    }

    public function Players()
    {
        return $this->belongsToMany(User::class, 'team_players', 'team_id', 'user_id')->whereNull('team_players.deleted_at');
    }

    public function followers()
    {
        return $this->belongsToMany(User::class, 'followers', 'follow_id', 'user_id')->where('followers.type', 'team')->whereNull('followers.deleted_at');
    }

    public function favorites()
    {
        return $this->belongsToMany(User::class, 'favorites', 'favorite_id', 'user_id')->where('favorites.type', 'team')->whereNull('favorites.deleted_at');
    }

    public function getFollowersNumAttribute()
    {
        return $this->followers()->count();
    }

    public function getSportAttribute()
    {
        $sport = $this->Sport()->first();
        if (isset($sport))
            return new SportResource($sport);
//
        return null;
    }

    public function getLeagueTeamPhoneAttribute()
    {
        $leagueTeam = LeagueTeam::where('team_id', $this->id)->where('status', 'confirmed')->first();

        if (isset($leagueTeam))
            return $leagueTeam->team_phone;
//
        return null;
    }

    public function getOwnerAttribute()
    {
        $owner = $this->User()->first();
        if (isset($owner))
            return $owner->makeHidden($this->user_hidden);
        return null;
    }

    public function getSponsorsAttribute()
    {
        if (preg_match('/teams/i', url()->current()))
            return null;
        return $this->Sponsors()->get();
    }

    public function getInvitesNumAttribute()
    {
        return $this->Players()->where('status', 'invited')->pluck('user_id')->unique()->count();
    }

    public function getRequestsNumAttribute()
    {
        return $this->Players()->where('status', 'joined')->pluck('user_id')->unique()->count();
    }

    public function getAcceptsNumAttribute()
    {
        return $this->Players()->where('status', 'accepted')->pluck('user_id')->unique()->count();
    }

    // not complete according to rate
    public function getTopPlayersAttribute()
    {
        /*if (request()->segment(3) == 'categorizes') {
            $top_player = $this->Players()->where('status', 'accepted')->orderByDesc('team_players.team_id')->take(3)->get();
            if (count($top_player) > 0) {
                $this->user_hidden[] = 'my_teams';
                return $top_player->makeHidden('pivot')->makeHidden($this->user_hidden);
            }
        }*/
//        return [];
        return null;
    }

    public function getPlayersAttribute()
    {

        if (request()->has('team_id') && request()->segment(3) == 'categorizes' || request()->has('category_team_id')) {
            $team_players = $this->Players()->where('status', 'accepted')->pluck('users.id')->toArray();

            $team_id = request()->has('category_team_id') && request()->get('category_team_id') != 0 ? request()->get('category_team_id') : request()->get('team_id');
            $team = Team::find($team_id);

            $user_in_same_team_id = UserSport::where('sport_id', $team->sport_id)->pluck('user_id')->toArray();
//            $team_players = isset($team->players) ? $team->players->pluck('id')->toArray() : [];

            $team_players[] = auth()->user()->id;
            $players_id = array_unique(array_diff($user_in_same_team_id, $team_players));
            $players = $this->Players()->where('status', 'accepted')->whereIn('team_players.user_id', $players_id)->orderByDesc('team_players.team_id')->take(3)->get();

        } else {
            $players = $this->Players()->where('status', 'accepted')->orderByDesc('team_players.team_id')->take(3)->get()->unique();//->toArray();
        }

        if (count($players) > 0) {
            $this->user_hidden[] = 'my_teams';
            return $players->makeHidden('pivot')->makeHidden($this->user_hidden);
        }

//        return [];
        return null;
    }

    public function getCityAttribute()
    {
        $city = $this->City()->first();
        if (isset($city))
            return $city->title;
        return null;
    }

    public function getManagerAttribute()
    {

        $manager = $this->Manager()->first();

        if (isset($manager))
            $manager_accepted = $this->Players()->where('user_id', $manager->id)->where('status', 'accepted')->first();
        if (isset($manager_accepted) && isset($manager))
            return $manager->makeHidden($this->user_hidden);
        return null;
    }

    public function getLogo100Attribute()
    {

        if ($this->getOriginal('logo') != null)
            return url('storage/app/teams/' . $this->id) . '/100/' . $this->getOriginal('logo');

        return null;
    }

    public function getLogo300Attribute()
    {

        if ($this->getOriginal('logo') != null)
            return url('storage/app/teams/' . $this->id) . '/300/' . $this->getOriginal('logo');
        return null;
    }

    function getLogoAttribute($value)
    {

        if (isset($value))
            return url('storage/app/teams/' . $this->id) . '/' . $value;
        return null;
    }

    public function getBgImage100Attribute()
    {

        if ($this->getOriginal('bg_image') != null)
            return url('storage/app/teams/' . $this->id) . '/100/' . $this->getOriginal('bg_image');

        return null;
    }

    public function getBgImage300Attribute()
    {

        if ($this->getOriginal('bg_image') != null)
            return url('storage/app/teams/' . $this->id) . '/300/' . $this->getOriginal('bg_image');
        return null;
    }

    function getBgImageAttribute($value)
    {

        if (isset($value))
            return url('storage/app/teams/' . $this->id) . '/' . $value;
        return null;
    }

    public function getRateAttribute()
    {
        return 0;
    }

    public function getRateNumAttribute()
    {
        return 0;
    }

    // Wons/games
    public function getWonPercentAttribute()
    {
        return '0/0';
    }

    public function getTotalAwardsAttribute()
    {
        return 0;
    }

    public function getTotalPlayersAttribute()
    {
        return $this->Players()->where('status', 'accepted')->pluck('user_id')->unique()->count();
    }

    public function getIsFavoriteAttribute()
    {
        if (auth()->check())
            return ($this->favorites()->where('favorite_id', $this->id)->where('user_id', auth()->user()->id)->where('type', 'team')->count() > 0) ? 1 : 0;
        return 0;
    }

    public function getIsFollowAttribute()
    {
        if (auth()->check())
            return ($this->followers()->where('follow_id', $this->id)->where('user_id', auth()->user()->id)->where('type', 'team')->count() > 0) ? 1 : 0;
        return 0;
    }

    public function getMyStatusAttribute()
    {
        // not completed
        if (auth()->check()) {
            if ($this->User()->where('id', auth()->user()->id)->count() > 0) {
                return trans('app.owner');
            } elseif ($this->Manager()->where('id', auth()->user()->id)->count() > 0) {
                return trans('app.manager');
            } elseif ($this->Players()->where('status', 'accepted')->where('user_id', auth()->user()->id)->count() > 0) {
                return trans('app.player');
            }
        }
        return 'guest';
    }

    public function getInvitesLeagueNumAttribute()
    {
<<<<<<< HEAD
        return $this->Leagues()->where('league_teams.type', 'league')->where('league_teams.team_id', $this->id)->where(function ($query) {
            $query->where('league_teams.status', 'invited')->orWhere('league_teams.status', 'accepted');
        })->pluck('league_teams.league_id')->unique()->count();
=======
        return $this->Leagues()->where('league_teams.team_id', $this->id)->where('league_teams.status', 'invited')->pluck('league_teams.league_id')->unique()->count();
>>>>>>> Cpanel
    }

    public function getRequestsLeagueNumAttribute()
    {
<<<<<<< HEAD
        return $this->Leagues()->where('league_teams.team_id', $this->id)->where('league_teams.type', 'team')->where(function ($query) {
            $query->where('league_teams.status', 'joined')->orWhere('league_teams.status', 'rejected')->orWhere('league_teams.status', 'accepted');
        })->pluck('league_teams.league_id')->unique()->count();
=======
        return $this->Leagues()->where('league_teams.team_id', $this->id)->where('league_teams.status', 'joined')->pluck('league_teams.league_id')->unique()->count();
>>>>>>> Cpanel
    }

    public function getMyLeaguesNumAttribute()
    {
        $leagues_id = $this->Leagues()->where('league_teams.team_id', $this->id)->where('league_teams.status', 'confirmed')->pluck('league_teams.league_id')->unique();
        return League::whereIn('id', $leagues_id)->where('status', 'published')->count();
<<<<<<< HEAD
=======

>>>>>>> Cpanel
    }

}
