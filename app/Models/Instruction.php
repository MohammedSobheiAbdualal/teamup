<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Instruction extends Model
{
    //
    use SoftDeletes;

    protected $appends = ['title', 'content'];

    public function getTitleAttribute()
    {
        if (request()->hasHeader('lang') && request()->header('lang') == 'ar') {
            return $this->title_ar;
        }
        return $this->title_en;
    }

    public function getContentAttribute()
    {
        if (request()->hasHeader('lang') && request()->header('lang') == 'ar') {
            return $this->content_ar;
        }
        return $this->content_en;
    }

}
