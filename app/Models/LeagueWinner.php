<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeagueWinner extends Model
{
    //
    use SoftDeletes;

    protected $appends = ['winner_name', 'awards'];

    public function Winner()
    {
        return $this->belongsTo(Winner::class, 'winner_id');
    }

    public function Awards()
    {
        return $this->hasMany(LeagueAward::class, 'league_winner_id', 'id');
    }

    public function getWinnerNameAttribute()
    {
        $winner = $this->Winner()->first();
        return (isset($winner)) ? $winner->title : $this->other_winner;
    }

    public function getAwardsAttribute()
    {
        return $this->Awards()->get();
    }
}
