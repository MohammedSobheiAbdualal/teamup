<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserSport extends Model
{
    //
    use SoftDeletes;

    public function PrimaryPosition()
    {
        return $this->belongsTo(Position::class, 'primary_position_id');
    }

    public function SecondaryPosition()
    {
        return $this->belongsTo(Position::class, 'secondary_position_id');
    }
}
