<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Setting extends Model
{
    //
    use SoftDeletes;

    protected $appends = ['description'];

    public function getDescriptionAttribute()
    {
        if (request()->hasHeader('lang') && request()->header('lang') == 'ar') {
            return $this->description_ar;
        }
        return $this->description_en;
    }
}
