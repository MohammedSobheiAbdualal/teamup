<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CelebritiesGuest extends Model
{
    //
    use SoftDeletes;

    protected $hidden = ['pivot'];
    protected $appends = ['user', 'guest_picture100', 'guest_picture300'];
    protected $user_hidden = ['email', 'email_verified_at', 'birthdate',
        'sports', 'my_teams', 'mobile', 'bio', 'country_code', 'bg_image', 'lang_id', 'city_id', 'verification_code', 'is_active', 'is_verified', 'last_login_date',
        'deleted_at', 'created_at', 'updated_at'];

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getGuestPicture100Attribute()
    {

        if ($this->getOriginal('guest_picture') != null)
            return url('storage/app/celebrities_guest/' . $this->id) . '/100/' . $this->getOriginal('guest_picture');
        if (isset($this->user_id)) {
            $user = $this->User()->first();
            return $user->avatar100;
        }
        return null;
    }

    public function getGuestPicture300Attribute()
    {

        if ($this->getOriginal('guest_picture') != null)
            return url('storage/app/celebrities_guest/' . $this->id) . '/300/' . $this->getOriginal('guest_picture');
        if (isset($this->user_id)) {
            $user = $this->User()->first();
            return $user->avatar300;
        }
        return null;
    }

    function getGuestPictureAttribute($value)
    {
        if (isset($this->user_id)) {
            $user = $this->User()->first();
            return $user->avatar;
        }
        if (isset($value))
            return url('storage/app/celebrities_guest/' . $this->id) . '/' . $value;

        return null;
    }

    public function getUserAttribute()
    {
        $user = $this->User()->first();
        if (isset($user))
            return $user->makeHidden($this->user_hidden);
    }

    function getGuestNameAttribute($value)
    {
        if (isset($this->user_id)) {
            $user = $this->User()->first();
            return $user->first_name . ' ' . $user->last_name;
        }
        return $value;
    }

}
