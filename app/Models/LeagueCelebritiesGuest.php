<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeagueCelebritiesGuest extends Model
{
    //
    use SoftDeletes;

    protected $appends = ['guest_picture100', 'guest_picture300'];

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getGuestPicture100Attribute()
    {
        $user = $this->User()->first();
        if (!isset($user))
            if ($this->getOriginal('guest_picture') != null)
                return url('storage/app/league_celebrities_guest/' . $this->id) . '/100/' . $this->getOriginal('avatar');
        return $user->avatar100;
    }

    public function getGuestPicture300Attribute()
    {
        $user = $this->User()->first();
        if (!isset($user))
            if ($this->getOriginal('guest_picture') != null)
                return url('storage/app/league_celebrities_guest/' . $this->id) . '/300/' . $this->getOriginal('avatar');
        return $user->avatar300;

    }

    public function getGuestPictureAttribute($value)
    {
        $user = $this->User()->first();
        if (!isset($user))
            if (isset($value))
                return url('storage/app/league_celebrities_guest/' . $this->id) . '/' . $value;
        return $user->avatar;
    }

}
