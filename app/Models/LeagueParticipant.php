<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeagueParticipant extends Model
{
    //
    use SoftDeletes;

    protected $appends = ['user'];
    protected $user_hidden = ['email', 'email_verified_at', 'birthdate',
        'sports', 'my_teams', 'bio', 'country_code', 'bg_image', 'lang_id', 'city_id', 'verification_code', 'is_active', 'is_verified', 'last_login_date',
        'deleted_at', 'created_at', 'updated_at'];

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getUserAttribute()
    {
        return $this->User()->first()->makeHidden($this->user_hidden);
    }
}
