<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FacilityImage extends Model
{
    //
    use SoftDeletes;

    protected $appends = ['image100', 'image300'];


    public function getImage100Attribute()
    {
        if ($this->getOriginal('image') != null)
            return url('storage/app/facility/' . $this->facility_id) . '/100/' . $this->getOriginal('image');
        return null;
    }

    public function getImage300Attribute()
    {
        if ($this->getOriginal('image') != null)
            return url('storage/app/facility/' . $this->facility_id) . '/300/' . $this->getOriginal('image');
        return null;

    }

    public function getImageAttribute($value)
    {
        if (isset($value))
            return url('storage/app/facility/' . $this->facility_id) . '/' . $value;
        return null;
    }
}
