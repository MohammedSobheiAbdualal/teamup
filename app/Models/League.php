<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use function Complex\theta;

class League extends Model
{
    //
    use SoftDeletes;

<<<<<<< HEAD
    protected $appends = ['is_favorite', 'is_follow', 'followers_num', 'total_awards', 'refundable', 'first_place_money', 'league_status', 'league_open_status', 'league_status_title', 'invitation_status', 'invitation_status_title', 'spectators', 'reg_team_num', 'lineup', 'payment_method', 'logo100', 'logo300', 'bg_image100', 'bg_image300',
=======
    protected $appends = ['total_awards', 'refundable', 'league_status', 'league_status_title', 'invitation_status', 'invitation_status_title', 'spectators', 'reg_team_num', 'lineup', 'payment_method', 'logo100', 'logo300', 'bg_image100', 'bg_image300',
>>>>>>> Cpanel
        'facility', 'matches', 'channels', 'organizers', 'committee_officials', 'guests', 'sponsors',
        'services', 'restrictions', 'rules',
        'creator', 'sport', 'location', 'winners', 'teams'];


    protected $user_hidden = ['email', 'email_verified_at', 'birthdate',
        'sports', 'my_teams', 'bio', 'country_code', 'bg_image', 'lang_id', 'city_id', 'verification_code', 'is_active', 'is_verified', 'last_login_date',
        'deleted_at', 'created_at', 'updated_at'];
    protected $team_hidden = ['top_players', 'players', 'city', 'sport', 'sponsors', 'deleted_at', 'created_at', 'updated_at'];

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function Sport()
    {
        return $this->belongsTo(Sport::class, 'sport_id');
    }

    public function PaymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class, 'payment_method_id');
    }

    public function Location()
    {
        return $this->hasOne(LeagueLocation::class, 'league_id', 'id');
    }

    public function Facility()
    {
        return $this->hasOne(Facility::class, 'league_id', 'id');
    }

    public function Winners()
    {
        return $this->hasMany(LeagueWinner::class, 'league_id', 'id');
    }

    public function Awards()
    {
        return $this->hasMany(LeagueAward::class, 'league_id', 'id');
    }

    public function Teams()
    {
        return $this->belongsToMany(Team::class, 'league_teams', 'league_id', 'team_id')->whereNull('league_teams.deleted_at');
    }

    public function Channels()
    {
        return $this->hasMany(LeagueParticipant::class, 'league_id', 'id')->where('type', 'channel');
    }

    public function Organizers()
    {
        return $this->hasMany(LeagueParticipant::class, 'league_id', 'id')->where('type', 'organizer');
    }

    public function CommitteeOfficials()
    {
        return $this->hasMany(LeagueParticipant::class, 'league_id', 'id')->where('type', 'committee_official');
    }

    public function Services()
    {
        return $this->hasMany(LeagueCondition::class, 'league_id', 'id')->where('type', 'service');
    }

    public function Restrictions()
    {
        return $this->hasMany(LeagueCondition::class, 'league_id', 'id')->where('type', 'restrict');
    }

    public function Rules()
    {
        return $this->hasMany(LeagueCondition::class, 'league_id', 'id')->where('type', 'rule');
    }

    public function Sponsors()
    {
        return $this->belongsToMany(Sponsor::class, 'league_sponsors', 'league_id', 'sponsor_id', 'id', 'id')->withPivot('is_main')->whereNull('league_sponsors.deleted_at');
    }

    public function Guests()
    {
        return $this->belongsToMany(CelebritiesGuest::class, 'league_celebrities_guests', 'league_id', 'celebrities_guest_id', 'id', 'id')->whereNull('league_celebrities_guests.deleted_at');
    }

<<<<<<< HEAD
    public function followers()
    {
        return $this->belongsToMany(User::class, 'followers', 'follow_id', 'user_id')->where('followers.type', 'league')->whereNull('followers.deleted_at');
    }

    public function favorites()
    {
        return $this->belongsToMany(User::class, 'favorites', 'favorite_id', 'user_id')->where('favorites.type', 'league')->whereNull('favorites.deleted_at');
    }

    public function getFollowersNumAttribute()
    {
        return $this->followers()->count();
    }

    public function getIsFavoriteAttribute()
    {
        if (auth()->check())
            return ($this->favorites()->where('favorite_id', $this->id)->where('user_id', auth()->user()->id)->where('type', 'league')->count() > 0) ? 1 : 0;
        return 0;
    }

    public function getIsFollowAttribute()
    {
        if (auth()->check())
            return ($this->followers()->where('follow_id', $this->id)->where('user_id', auth()->user()->id)->where('type', 'league')->count() > 0) ? 1 : 0;
        return 0;
    }

=======
>>>>>>> Cpanel
    public function getTotalAwardsAttribute()
    {
        return $this->Awards()->sum('money_value');
    }

    public function getRegTeamNumAttribute()
    {

        $confirmed_teams = $this->Teams()->where('league_teams.status', 'confirmed')->pluck('league_teams.team_id')->unique()->count();
        if ($this->type == 'specify')
            return $confirmed_teams . '/' . $this->team_num;
        else
            return $confirmed_teams . '';

    }

    public function getLineupAttribute()
    {
        return $this->main_lineup . '/' . $this->bench . '/' . $this->reserve_num;
    }

    public function getRefundableAttribute()
    {
        // check it tomorrow
        // Problem here

<<<<<<< HEAD
        return (Carbon::parse($this->deadline_date)->format('Y-m-d H:i:s') <= Carbon::now()->format('Y-m-d H:i:s') || Carbon::parse($this->deadline_date)->diffInDays(Carbon::now()->format('Y-m-d H:i:s')) <= $this->frcd) ? 0 : 1;
    }

    //
    public function getFirstPlaceMoneyAttribute()
    {
        $winner = $this->Winners()->where('winner_id', 1)->first();
        if (isset($winner)) {
            $award = $this->Awards()->where('league_winner_id', $winner->id)->where('award_id', 1)->first();

            if (isset($award))
                return $award->money_value;
        }
        return 0;
    }

    // Spectator // المشجعين
=======
        return ($this->deadline_date <= Carbon::now()->format('Y-m-d H:i:s') || Carbon::parse($this->deadline_date)->diffInDays(Carbon::now()->format('Y-m-d H:i:s')) <= $this->frcd) ? 0 : 1;
    }

    // Spectator
>>>>>>> Cpanel
    public function getSpectatorsAttribute()
    {
        return 0;
    }

    public function getMatchesAttribute()
    {
        return null;
    }

    public function getCreatorAttribute()
    {
        return $this->User()->first()->makeHidden($this->user_hidden);
    }

    public function getServicesAttribute()
    {
        return $this->Services()->get();
    }

    public function getRestrictionsAttribute()
    {
        return $this->Restrictions()->get();
    }

    public function getRulesAttribute()
    {
        return $this->Rules()->get();
    }

    public function getChannelsAttribute()
    {
        return $this->Channels()->get();
    }

    public function getOrganizersAttribute()
    {
        return $this->Organizers()->get();
    }

    public function getCommitteeOfficialsAttribute()
    {
        return $this->CommitteeOfficials()->get();
    }

    public function getGuestsAttribute()
    {
        return $this->Guests()->get();
    }

    public function getSportAttribute()
    {
        return $this->Sport()->first();
    }

    public function getLocationAttribute()
    {
        return $this->Location()->first();
    }

    public function getFacilityAttribute()
    {
        return $this->Facility()->first();
    }

    public function getSponsorsAttribute()
    {
//        if (preg_match('/leagues/i', url()->current()))
//            return null;
        return $this->Sponsors()->get();
    }

    public function getPaymentMethodAttribute()
    {
        if ($this->subscription_type == 'paid')
            return $this->PaymentMethod()->first()->title;
        return '';
    }

    public function getWinnersAttribute()
    {
        return $this->Winners()->get();
    }

    public function getLogo100Attribute()
    {

        if ($this->getOriginal('logo') != null)
            return url('storage/app/leagues/' . $this->id) . '/100/' . $this->getOriginal('logo');

        return null;
    }

    public function getLogo300Attribute()
    {

        if ($this->getOriginal('logo') != null)
            return url('storage/app/leagues/' . $this->id) . '/300/' . $this->getOriginal('logo');
        return null;
    }

    function getLogoAttribute($value)
    {

        if (isset($value))
            return url('storage/app/leagues/' . $this->id) . '/' . $value;
        return null;
    }

    public function getBgImage100Attribute()
    {

        if ($this->getOriginal('bg_image') != null)
            return url('storage/app/leagues/' . $this->id) . '/100/' . $this->getOriginal('bg_image');

        return null;
    }

    public function getBgImage300Attribute()
    {

        if ($this->getOriginal('bg_image') != null)
            return url('storage/app/leagues/' . $this->id) . '/300/' . $this->getOriginal('bg_image');
        return null;
    }

    function getBgImageAttribute($value)
    {

        if (isset($value))
            return url('storage/app/leagues/' . $this->id) . '/' . $value;
        return null;
    }

    public function getTeamsAttribute()
    {

        if (request()->has('league_id') && request()->segment(3) == 'categorizes' || request()->has('category_league_id')) {
            $league_teams = $this->Teams()->where('league_teams.status', 'accepted')->pluck('teams.id')->toArray();

            $league_id = request()->has('category_league_id') && request()->get('category_league_id') != 0 ? request()->get('category_league_id') : request()->get('league_id');
            $league = League::find($league_id);

            $team_in_same_league_id = Team::where('sport_id', $league->sport_id)->pluck('team_id')->toArray();
//            $team_players = isset($team->players) ? $team->players->pluck('id')->toArray() : [];

            $league_teams[] = auth()->user()->id;
            $teams_id = array_unique(array_diff($team_in_same_league_id, $league_teams));
            $teams = $this->Teams()->where('league_teams.status', 'accepted')->whereIn('league_teams.team_id', $teams_id)->orderByDesc('league_teams.team_id')->take(3)->get();

        } else {
            $teams = $this->Teams()->where('league_teams.status', 'accepted')->orderByDesc('league_teams.league_id')->take(3)->get()->unique();//->toArray();
        }

        if (count($teams) > 0) {
            $this->team_hidden[] = 'my_teams';
            return $teams->makeHidden('pivot')->makeHidden($this->team_hidden);
        }

//        return [];
        return null;
    }

    public function getLeagueStatusTitleAttribute()
    {
        return trans('app.leagues.status.' . $this->league_status);
    }

<<<<<<< HEAD
    public function getLeagueOpenStatusAttribute()
    {
        if ($this->deadline_date > Carbon::now()->format('Y-m-d H:i:s'))
            return 'Open';
        if ($this->deadline_date <= Carbon::now()->format('Y-m-d') && $this->end_date >= Carbon::now()->format('Y-m-d'))
            return $this->start_date;
        if ($this->end_date < Carbon::now()->format('Y-m-d'))
            return 'Ended';
    }

=======
>>>>>>> Cpanel
    public function getLeagueStatusAttribute()
    {
        if ($this->start_date > Carbon::now()->format('Y-m-d'))
            return 'upcoming';
        if ($this->start_date <= Carbon::now()->format('Y-m-d') && $this->end_date >= Carbon::now()->format('Y-m-d'))
            return 'ongoing';
        if ($this->end_date < Carbon::now()->format('Y-m-d'))
            return 'ended';
    }

    public function getInvitationStatusTitleAttribute()
    {
<<<<<<< HEAD
        if ($this->invitation_status == '-')
            return '-';
=======
>>>>>>> Cpanel
        return trans('app.leagues.invitation_status.' . $this->invitation_status);
    }

    public function getInvitationStatusAttribute()
    {
        //my_league, invite, request
        if (auth()->check() && request()->filled('team_id')) {
<<<<<<< HEAD

            $league = LeagueTeam::where('league_id', $this->id)->where('team_id', request()->get('team_id'))->first();//where('user_id', auth()->user()->id)->
            if (isset($league))
                return $league->status;
//            $my_league = LeagueTeam::where('user_id', auth()->user()->id)->where('league_id', $this->id)->where('team_id', request()->get('team_id'))->where('status', 'confirmed')->first();;
//            if (isset($my_league))
//                return 'my_league';
//            $invite = LeagueTeam::where('user_id', auth()->user()->id)->where('league_id', $this->id)->where('team_id', request()->get('team_id'))->where('status', 'invited')->first();;
//            if (isset($invite))
//                return 'invite';
//            $request = LeagueTeam::where('user_id', auth()->user()->id)->where('league_id', $this->id)->where('team_id', request()->get('team_id'))->where('status', 'joined')->first();;
//            if (isset($request))
//                return 'request';
=======
            $my_league = LeagueTeam::where('user_id', auth()->user()->id)->where('league_id', $this->id)->where('team_id', request()->get('team_id'))->where('status', 'confirmed')->first();;
            if (isset($my_league))
                return 'my_league';
            $invite = LeagueTeam::where('user_id', auth()->user()->id)->where('league_id', $this->id)->where('team_id', request()->get('team_id'))->where('status', 'invited')->first();;
            if (isset($invite))
                return 'invite';
            $request = LeagueTeam::where('user_id', auth()->user()->id)->where('league_id', $this->id)->where('team_id', request()->get('team_id'))->where('status', 'joined')->first();;
            if (isset($request))
                return 'request';
>>>>>>> Cpanel

        }
        return '-';
    }


}
