<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    //
    use SoftDeletes;

    protected $appends = ['title', 'logo'];

    public function getLogoAttribute()
    {
        if ($this->type == 'team') {
            return Team::withTrashed()->find($this->data_id)->logo100;
        } elseif ($this->type == 'player') {
            return User::withTrashed()->find($this->data_id)->avatar100;
        }
    }

    public function getTitleAttribute()
    {
        if ($this->type == 'team') {
            $team = Team::withTrashed()->find($this->data_id);
            return trans('app.team', ['name' => $team->name]);
        } elseif ($this->type == 'player') {
            $user = User::withTrashed()->find($this->data_id);
            return $user->full_name;

        }

        return '-';
    }

    public function getTextAttribute($value)
    {
        return trans(notification_trans() . '.' . $this->action);
    }
}
