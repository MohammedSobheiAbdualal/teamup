<?php

namespace App\Models;

use Carbon\Carbon;
use DB;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'mobile', 'password', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['full_name', 'invites_num', 'requests_num', 'my_teams_num', 'my_teams', 'num_games', 'rate', 'rate_num', 'top_teams', 'is_favorite', 'is_follow', 'is_friend', 'avatar100', 'avatar300', 'bg_image100', 'bg_image300', 'city', 'followings_num', 'followers_num', 'friends_num', 'sports', 'created_date'];

    protected $hidden_team = ['rate', 'rate_num', 'my_status', 'is_favorite', 'is_follow', 'won_percent', 'total_players', 'total_awards', 'logo100', 'logo300', 'owner', 'manager', 'top_players', 'city', 'sport', 'sponsors'];
    protected $sport_hidden = ['name_ar', 'name_en', 'has_position',
        'is_active', 'pivot',
        'deleted_at', 'created_at', 'updated_at'];


    public function Sports()
    {
        return $this->belongsToMany(Sport::class, 'user_sports', 'user_id', 'sport_id')->whereNull('user_sports.deleted_at');
    }

    public function followings()
    {
        return $this->belongsToMany(User::class, 'followers', 'user_id', 'follow_id')->where('users.signup_level', 3)->where('followers.type', 'player')->whereNull('followers.deleted_at')->orderByDesc('followers.created_at');
    }

    public function followingsTeams()
    {
        return $this->belongsToMany(Team::class, 'followers', 'user_id', 'follow_id')->where('followers.type', 'team')->whereNull('followers.deleted_at')->orderByDesc('followers.created_at');
<<<<<<< HEAD
    }

    public function followingsLeagues()
    {
        return $this->belongsToMany(League::class, 'followers', 'user_id', 'follow_id')->where('followers.type', 'league')->whereNull('followers.deleted_at')->orderByDesc('followers.created_at');
=======
>>>>>>> Cpanel
    }

    // users that follow this user
    public function followers()
    {
        return $this->belongsToMany(User::class, 'followers', 'follow_id', 'user_id')->where('users.signup_level', 3)->where('followers.type', 'player')->whereNull('followers.deleted_at')->orderByDesc('followers.created_at');
    }

//    public function friends()
//    {
//        return $this->belongsToMany(User::class, 'friends', 'user_id', 'friend_id')->whereNull('user_sports.deleted_at');
//    }

    // friendship that I started
    function friendsOfMine()
    {
        return $this->belongsToMany(User::class, 'friends', 'user_id', 'friend_id')
            ->where('users.signup_level', 3)->whereNull('friends.deleted_at');
    }

    // friendship that I was invited to
    function friendOf()
    {
        return $this->belongsToMany(User::class, 'friends', 'friend_id', 'user_id')
            ->where('users.signup_level', 3)->whereNull('friends.deleted_at');
    }


    public function mergeFriends()
    {
        return $this->friendsOfMine()->get()->merge($this->friendOf()->get());
    }

    public function favoritesUser()
    {
        return $this->belongsToMany(User::class, 'favorites', 'favorite_id', 'user_id')->where('favorites.type', 'player')->whereNull('favorites.deleted_at')->orderByDesc('favorites.created_at');
    }

    public function favoritesTeam()
    {
        return $this->belongsToMany(Team::class, 'favorites', 'user_id', 'favorite_id')->where('favorites.type', 'team')->whereNull('favorites.deleted_at')->orderByDesc('favorites.created_at');
    }

    public function City()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function Language()
    {
        return $this->belongsTo(Language::class, 'lang_id');
    }

    public function Location()
    {
        return $this->hasOne(UserLocation::class, 'user_id', 'id');
    }

    public function Teams()
    {
        return $this->belongsToMany(Team::class, 'team_players', 'user_id', 'team_id')->whereNull('team_players.deleted_at');
    }

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getInvitesNumAttribute()
    {
        if (auth()->check()) {
//            return TeamPlayer::where('team_players.user_id', auth()->user()->id)->where('status', 'invited')->pluck('team_id')->unique('team_id')->count();

            $teams_id = TeamPlayer::where('user_id', auth()->user()->id)->where('status', 'invited')->pluck('team_id')->unique();
//            $teams_id = $this->Teams()->where('team_players.status', 'invited')->pluck('team_id')->unique();

            return Team::whereIn('id', $teams_id)->count();
        }
        return 0;

    }

    public function getRequestsNumAttribute()
    {
        if (auth()->check()) {
//            $teams_id = $this->Teams()->where('team_players.status', 'joined')->pluck('team_id')->unique();
            $teams_id = TeamPlayer::where('user_id', auth()->user()->id)->where('status', 'joined')->pluck('team_id')->unique();

//            $teams_id = $this->Teams()->where('team_players.user_id', auth()->user()->id)->where('team_players.status', 'joined')->pluck('team_id')->unique();
            return Team::whereIn('id', $teams_id)->count();

        }
        return 0;
    }

    public function getMyTeamsNumAttribute()
    {
        $my_teams_id = $this->Teams()->where('team_players.user_id', $this->id)->where('team_players.status', 'accepted')->pluck('team_players.team_id');
        $my_teams_count = Team::where(function ($query) use ($my_teams_id) {
            $query->where('user_id', $this->id)->orWhereIn('id', $my_teams_id);
        })->where('status', 'published')->count();

        return $my_teams_count;

    }

    public function getMyTeamsAttribute()
    {
        $my_teams_id = $this->Teams()->where('team_players.user_id', $this->id)->where('team_players.status', 'accepted')->pluck('team_players.team_id');


        $my_teams = Team::where(function ($query) use ($my_teams_id) {
            $query->where('user_id', $this->id)->orWhereIn('id', $my_teams_id);
        })->where('status', 'published')->take(3)->get()->makeHidden($this->hidden_team);
        return $my_teams;

    }

    public function getNumGamesAttribute()
    {
        return 0;
    }

    public function getRateAttribute()
    {
        return 0;
    }

    public function getRateNumAttribute()
    {
        return 0;
    }

    public function getTopTeamsAttribute()
    {
        $teams = $this->Teams()->where('team_players.status', 'accepted')->get();
        if (count($teams) > 0)
            return $teams[0]->name . ((count($teams) > 1) ? ' +' . (count($teams) - 1) : '');
        return null;
    }

    public function getIsFavoriteAttribute()
    {
        if (auth()->check())
            return ($this->favoritesUser()->where('favorite_id', $this->id)->where('user_id', auth()->user()->id)->where('type', 'player')->count() > 0) ? 1 : 0;
        return 0;
    }

    public function getIsFollowAttribute()
    {
        if (auth()->check())
            return ($this->followers()->where('follow_id', $this->id)->where('user_id', auth()->user()->id)->where('type', 'player')->count() > 0) ? 1 : 0;
        return 0;
    }

    public function getIsFriendAttribute()
    {

        if (auth()->check()) {
            return $this->friendsOfMine()->where('friend_id', auth()->user()->id)->get()->merge($this->friendOf()->where('user_id', auth()->user()->id)->get())->count() > 0 ? 1 : 0;
        }
        return 0;
    }

//    public function getSignupLevelAttribute()
//    {
//        $flag = 1;
//        if ($this->first_name == null || $this->first_name == '')
//            $flag = 0;
//        if ($this->last_name == null || $this->last_name == '')
//            $flag = 0;
//
//        if ($this->city_id == null || $this->city_id == '')
//            $flag = 0;
//        if ($this->mobile == null || $this->mobile == '')
//            $flag = 0;
//
//        if ($flag == 1) {
//
//            if (count($this->sports) > 0)
//                $signup_step = 3;
//            else
//                $signup_step = 2;
//        } else {
//            $signup_step = 1;
//        }
//        return $signup_step;
//    }

    public function getCreatedDateAttribute()
    {
        return Carbon::parse($this->created_at)->diffForHumans();
    }

    public function getAvatar100Attribute()
    {
        if ($this->getOriginal('avatar') != null)
            return url('storage/app/users/' . $this->id) . '/100/' . $this->getOriginal('avatar');
        return null;
    }

    public function getAvatar300Attribute()
    {
        if ($this->getOriginal('avatar') != null)
            return url('storage/app/users/' . $this->id) . '/300/' . $this->getOriginal('avatar');
        return null;

    }

    public function getAvatarAttribute($value)
    {
        if (isset($value))
            return url('storage/app/users/' . $this->id) . '/' . $value;
        return null;
    }


    public function getCityAttribute()
    {
        $city = $this->City()->first();
        if (isset($city)) {
            return $city->title;
        }
        return null;
    }


    public function getBgImage100Attribute()
    {
        if ($this->getOriginal('bg_image') != null)
            return url('storage/app/users/' . $this->id) . '/100/' . $this->getOriginal('bg_image');
        return null;
    }

    public function getBgImage300Attribute()
    {
        if ($this->getOriginal('bg_image') != null)
            return url('storage/app/users/' . $this->id) . '/300/' . $this->getOriginal('bg_image');
        return null;
    }

    public function getBgImageAttribute($value)
    {
        if (isset($value))
            return url('storage/app/users/' . $this->id) . '/' . $value;
        return null;
    }


    public function getLastLoginDateAttribute($value)
    {
        return Carbon::parse($value)->diffForHumans();
    }


    public function getSportsAttribute()
    {
        if (preg_match('/team/i', url()->current()))
            return $this->Sports()->orderBy('id', 'ASC')->get()->makeHidden($this->sport_hidden);
        return $this->Sports()->orderBy('id', 'ASC')->get();
    }


//    public function verifyUser()
//    {
//        return $this->hasOne(VerifyUser::class, 'user_id', 'id')->orderByDesc('updated_at');
//    }


    public function getFollowingsNumAttribute()
    {
<<<<<<< HEAD
        return $this->followingsTeams()->count() + $this->followingsLeagues()->count() + $this->followings()->count();
=======
        return $this->followingsTeams()->count() + $this->followings()->count();
>>>>>>> Cpanel
    }

    public function getFollowersNumAttribute()
    {
        return $this->followers()->count();
    }

    public function getFriendsNumAttribute()
    {
        return $this->mergeFriends()->count();
    }

    public function findForPassport($username)
    {
        if (is_numeric($username)) {
            return $this->whereRaw('CONCAT(country_code,mobile) =?', $username)->first();
        } else {
            return $this->where('username', $username)->first();
        }

    }


}
