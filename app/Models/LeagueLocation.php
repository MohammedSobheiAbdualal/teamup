<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeagueLocation extends Model
{
    //
    use SoftDeletes;

    protected $appends = ['city_name', 'league_name', 'league_image'];
    protected $casts = ['latitude' => 'double', 'longitude' => 'double'];

    public function City()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function League()
    {
        return $this->belongsTo(League::class, 'league_id');
    }

    public function getCityNameAttribute()
    {
        return $this->City()->first()->title;
    }

    public function getLeagueNameAttribute()
    {
        $league = $this->League()->withTrashed()->first();
        return isset($league) ? $league->name : '';
    }

    public function getLeagueImageAttribute()
    {
        $league = $this->League()->withTrashed()->first();
        return isset($league) ? $league->logo : '';
    }
}
