<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    //
    use SoftDeletes;

    protected $appends = ['title'];

    protected $casts = ['latitude' => 'double', 'longitude' => 'double'];

    public function getTitleAttribute()
    {
        if (request()->hasHeader('lang') && request()->header('lang') == 'ar') {
            return $this->name_ar;
        }
        return $this->name_en;
    }

}
