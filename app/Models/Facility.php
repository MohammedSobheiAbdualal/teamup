<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Facility extends Model
{
    //
    use SoftDeletes;

    protected $appends = ['size_name', 'ground_name', 'images'];

    public function Size()
    {
        return $this->belongsTo(FacilitySize::class, 'size_id');
    }

    public function Ground()
    {
        return $this->belongsTo(FacilityGround::class, 'ground_id');
    }

    public function Images()
    {
        return $this->hasMany(FacilityImage::class, 'facility_id', 'id');
    }

    public function getSizeNameAttribute()
    {
        return $this->Size()->first()->title;
    }

    public function getGroundNameAttribute()
    {
        return $this->Ground()->first()->title;
    }

    public function getImagesAttribute()
    {
        return $this->Images()->get();
    }

}
