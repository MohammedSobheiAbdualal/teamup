<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TeamPlayer extends Model
{
    //
    use SoftDeletes;

    protected $appends = ['position_name', 'shortcut'];

    public function Position()
    {
        return $this->belongsTo(Position::class, 'position_id');
    }

    public function SecondaryPosition()
    {
        return $this->belongsTo(Position::class, 'secondary_position_id');
    }

    public function Team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }

    public function getPositionNameAttribute()
    {
        $position = $this->Position()->first();
        if (isset($position))
            return $position->title;
        return trans('app.all');

    }

    public function getShortCutAttribute()
    {
        $position = $this->Position()->first();
        if (isset($position))
            return $position->shortcut;
        return trans('app.all');

    }

}
