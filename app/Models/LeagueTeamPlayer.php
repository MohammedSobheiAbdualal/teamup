<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeagueTeamPlayer extends Model
{
    //
    use SoftDeletes;
<<<<<<< HEAD

    protected $appends = ['position_name', 'shortcut'];

    public function Position()
    {
        return $this->belongsTo(Position::class, 'position_id');
    }
    public function SecondaryPosition()
    {
        return $this->belongsTo(Position::class, 'secondary_position_id');
    }

    public function getPositionNameAttribute()
    {
        $position = $this->Position()->first();
        if (isset($position))
            return $position->title;
        return trans('app.all');

    }

    public function getShortCutAttribute()
    {
        $position = $this->Position()->first();
        if (isset($position))
            return $position->shortcut;
        return trans('app.all');

    }

    public function ActivePlayer()
    {
        return $this->belongsTo(User::class, 'player_id');//->where('users.is_active', 1);
    }

=======
>>>>>>> Cpanel
}
