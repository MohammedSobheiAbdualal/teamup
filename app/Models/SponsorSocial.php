<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SponsorSocial extends Model
{
    //
    use SoftDeletes;

    protected $appends = ['social'];

    public function Social()
    {
        return $this->belongsTo(SocialMedia::class, 'social_id');
    }

    public function getSocialAttribute()
    {
        return $this->Social()->first();
    }
}
