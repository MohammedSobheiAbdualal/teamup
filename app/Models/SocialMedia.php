<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SocialMedia extends Model
{
    //
    use SoftDeletes;

    public function getIconAttribute($value)
    {
        return url('assets/social/' . $value);

    }
}
