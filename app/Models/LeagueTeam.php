<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeagueTeam extends Model
{
    //
    use SoftDeletes;

    public function Players()
    {
        return $this->belongsToMany(User::class, 'league_team_players', 'league_team_id', 'player_id')->whereNull('league_team_players.deleted_at');
    }
}
