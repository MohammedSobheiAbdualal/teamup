<?php

namespace App\Models;

use function Complex\theta;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sport extends Model
{
    //
    use SoftDeletes;


    protected $appends = ['title', 'icon', 'primary_position', 'secondary_position'];
    protected $position_hidden = ['title_ar', 'title_en', 'sport_id', 'deleted_at', 'created_at', 'updated_at'];


    public function Icon()
    {
        return $this->belongsTo(Icon::class, 'icon_id');
    }

    public function getIconAttribute()
    {
        return $this->Icon()->first()->content;
    }

    public function getTitleAttribute()
    {
        if (request()->hasHeader('lang') && request()->header('lang') == 'ar') {
            return $this->name_ar;
        }
        return $this->name_en;
    }

    public function Positions()
    {
        return $this->hasMany(Position::class, 'sport_id', 'id')->orderBy('order_col_num', 'asc');
    }

    public function UserSport()
    {
        return $this->hasMany(UserSport::class, 'sport_id', 'id');
    }

    public function getPrimaryPositionAttribute()
    {
        if (auth()->check() && $this->has_position) {

            $user_position = $this->UserSport()->where('user_id', auth()->user()->id)->first();

            if (isset($user_position->PrimaryPosition)) {

                if (preg_match('/team/i', url()->current()))
                    return $user_position->PrimaryPosition->makeHidden($this->position_hidden);

                return $user_position->PrimaryPosition;
            }
        }
        return null;
    }

    public function getSecondaryPositionAttribute()
    {
        if (auth()->check() && $this->has_position) {
            $user_position = $this->UserSport()->where('user_id', auth()->user()->id)->first();

            if (isset($user_position->SecondaryPosition)) {
                if (preg_match('/team/i', url()->current()))
                    return $user_position->SecondaryPosition->makeHidden($this->position_hidden);

                return $user_position->SecondaryPosition;
            }
        }
        return null;
    }

}
