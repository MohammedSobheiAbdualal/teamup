<?php

namespace App\Providers;

use App\Services\SocialUserResolver;
use Carbon\Carbon;
use Hivokas\LaravelPassportSocialGrant\Resolvers\SocialUserResolverInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    public $bindings = [
        SocialUserResolverInterface::class => SocialUserResolver::class,
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $lang = 'en';
        if (request()->headers->has('lang')) {
            $lang = request()->headers->get('lang');
        }
        Carbon::setLocale($lang);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
