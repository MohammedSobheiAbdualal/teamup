<?php
/**
 * Created by PhpStorm.
 * User: mohammedsobhei
 * Date: 2/25/19
 * Time: 7:43 PM
 */


namespace App\Services;

use App\Repositories\Uploader;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Laravel\Socialite\Two\User as ProviderUser;

class SocialAccountsService extends Uploader
{
    /**
     *
     * Find or create user instance by provider user instance and provider name.
     *
     * @param ProviderUser $providerUser
     * @param string $provider
     *
     * @return User
     */
    public function findOrCreate(ProviderUser $providerUser, string $provider): User
    {

        $linkedSocialAccount = \App\LinkedSocialAccount::where('provider_name', $provider)
            ->where('provider_id', $providerUser->getId())
            ->first();
        if ($linkedSocialAccount) {
            return $linkedSocialAccount->user;
        } else {

            $user = null;
            if ($email = $providerUser->getEmail()) {
                $user = User::where('email', $email)->first();
            }
            if (!$user) {


                $url = $providerUser->getAvatar();

                $contents = file_get_contents($url);
                $path = upload_url();

                $file = $path . '/' . $providerUser->getId() . '.png';
                file_put_contents($file, $contents);

                $photo = null;
                $uploaded_file = new UploadedFile($file, $providerUser->getId());
                if ($uploaded_file) {
                    $photo = $providerUser->getId() . '.png';
                }

                $user = User::create([
                    'name' => $providerUser->getName(),
                    'email' => $providerUser->getEmail(),
                    'is_social' => true,
                    'email_verified_at' => Carbon::now(),
                    'photo' => $photo
                ]);
            }
            $user->linkedSocialAccounts()->create([
                'provider_id' => $providerUser->getId(),
                'provider_name' => $provider,
            ]);
            return $user;
        }
    }
}
