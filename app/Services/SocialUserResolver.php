<?php
/**
 * Created by PhpStorm.
 * User: mohammedsobhei
 * Date: 2/25/19
 * Time: 7:46 PM
 */


namespace App\Services;
use Exception;
use Hivokas\LaravelPassportSocialGrant\Resolvers\SocialUserResolverInterface;
use Illuminate\Contracts\Auth\Authenticatable;
use Laravel\Socialite\Facades\Socialite;
class SocialUserResolver implements SocialUserResolverInterface
{
    /**
     * Resolve user by provider credentials.
     *
     * @param string $provider
     * @param string $accessToken
     *
     * @return Authenticatable|null
     */
    public function resolveUserByProviderCredentials(string $provider, string $accessToken): ?Authenticatable
    {
        $providerUser = null;

        try {
            $providerUser = Socialite::driver($provider)->userFromToken($accessToken);
        } catch (Exception $exception) {}

        if ($providerUser) {
            $user =  (new SocialAccountsService())->findOrCreate($providerUser, $provider);
            return $user;
        }
        return null;
    }
}
