
<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key={{google_api_key()}}&libraries=places&callback=initialize"></script>
{{--<script src="{{url('/')}}/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>--}}
<div class="modal fade" id="{{$modal_id}}" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{$modal_title}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>

            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="portlet-body form">
                            {!! Form::open(['method'=>$form['method'],'id'=>$form['form_id'],'class'=>'form-horizontal form','url'=>$form['url'] ,'files'=>true]) !!}
                            <div class="alert alert-danger" role="alert" style="display: none"></div>
                            @include('layouts.flash-message')
                            <div class="form-body">
                                @foreach($form['fields'] as $key => $fields)
                                    @if($fields == 'image' || $fields == 'video')
                                        <div class="form-group m-form__group">
                                            <label class="control-label col-md-3">{{$form['fields_name'][$key]}}</label>
                                            <div class="col-md-9">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                                         style="width: 200px; height: 150px;">
                                                        @if(isset($form['values']))
                                                            @if($fields == 'video')
                                                                <video src="{{$form['values'][$key]}}" controls
                                                                       width="200"></video>
                                                            @else
                                                                <img src="{{$form['values'][$key]}}">

                                                            @endif
                                                        @else
                                                            <img
                                                                src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"
                                                                alt=""/>

                                                        @endif
                                                    </div>
                                                    <div>
                                                            <span class="btn red btn-outline btn-file">
                                                                <span class="fileinput-new"> Select </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="{{$key}}"
                                                                       id="{{$key}}"> </span>
                                                        <a href="javascript:" class="btn red fileinput-exists"
                                                           data-dismiss="fileinput">
                                                            Remove </a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    @endif

                                    @if($fields == 'ckeditor')
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{$form['fields_name'][$key]}}</label>

                                            <div class="col-md-12">
                                                <textarea class="summernote" name="{{$key}}" name="{{$key}}"
                                                          id="{{$key}}" rows="5"
                                                          placeholder="{{$form['fields_name'][$key]}}">@if(isset($form['values'])){{$form['values'][$key]}}@endif</textarea>

                                                {{--                                                          placeholder="{{$form['fields_name'][$key]}}"--}}
                                                {{--                                                          class="form-control {{$fields}}">@if(isset($form['values'])){{$form['values'][$key]}}@endif</textarea>--}}
                                                {{--                                           --}} </div>

                                        </div>
                                            <script
                                                src="{{url('/')}}/assets/demo/default/custom/crud/forms/widgets/summernote.js"
                                                type="text/javascript"></script>

                                        @endif
                                    @if($fields == 'text')
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{$form['fields_name'][$key]}}</label>
                                            <div class="col-md-9">
                                                <input type="text" name="{{$key}}" id="{{$key}}" class="form-control"
                                                       placeholder="{{$form['fields_name'][$key]}}"
                                                       @if(isset($form['values'])) value="{{$form['values'][$key]}}" @endif>
                                            </div>
                                        </div>
                                    @endif
                                    @if($fields == 'text_dis')
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{$form['fields_name'][$key]}}</label>
                                            <div class="col-md-12">
                                                <input type="text" name="{{$key}}" id="{{$key}}" class="form-control"
                                                       placeholder="{{$form['fields_name'][$key]}}" disabled
                                                       @if(isset($form['values'])) value="{{$form['values'][$key]}}" @endif>
                                            </div>
                                        </div>
                                    @endif
                                    @if($fields == 'time')
                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{$form['fields_name'][$key]}}</label>
                                            <div class="col-md-3">
                                                <div class="input-icon">
                                                    <i class="fa fa-clock-o"></i>
                                                    <input type="text" name="{{$key}}" id="{{$key}}"
                                                           class="form-control timepicker timepicker-24"
                                                           @if(isset($form['values'])) value="{{$form['values'][$key]}}" @endif>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if($fields == 'email')
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{$form['fields_name'][$key]}}</label>
                                            <div class="col-md-9">
                                                <input type="email" name="{{$key}}" id="{{$key}}"
                                                       class="form-control"
                                                       placeholder="{{$form['fields_name'][$key]}}"
                                                       @if(isset($form['values'])) value="{{$form['values'][$key]}}" @endif>
                                            </div>
                                        </div>
                                    @endif
                                    @if($fields == 'password')
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{$form['fields_name'][$key]}}</label>
                                            <div class="col-md-9">
                                                <input type="password" name="{{$key}}" id="{{$key}}"
                                                       class="form-control"
                                                       placeholder="{{$form['fields_name'][$key]}}">
                                            </div>
                                        </div>
                                    @endif
                                    @if($fields == 'textarea')
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{$form['fields_name'][$key]}}</label>
                                            <div class="col-md-12">
                                                <textarea name="{{$key}}" id="{{$key}}" rows="5"
                                                          placeholder="{{$form['fields_name'][$key]}}"
                                                          class="form-control">@if(isset($form['values'])){{$form['values'][$key]}}@endif</textarea>
                                            </div>
                                        </div>
                                    @endif

                                    @if($fields == 'map')
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{$form['fields_name'][$key]}}</label>
                                            <div class="col-md-12">
                                                <div class="col col-md-2"></div>
                                                <div class="col col-md-10 form_area">
                                                    <input id="searchInput" class="input-controls" type="text"
                                                           placeholder="Enter a location">
                                                    <div class="map" id="{{$key}}"
                                                         style="width: 90%; height: 300px;"></div>
                                                    <input type="hidden" @if(isset($form['values'])) value="{{$form['values']['lat']}}" @else value="{{getInitialLocation()['lat']}}" @endif name="lat" id="lat">
                                                    <input type="hidden" @if(isset($form['values'])) value="{{$form['values']['lng']}}" @else value="{{getInitialLocation()['lng']}}" @endif name="lng" id="lng">
                                                    <input type="hidden" value="Riyadh" name="location" id="location">
                                                </div>

                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                            function initialize() {
                                                    @if (isset($form['values']))
                                                var lat="{{$form['values']['lat']}}";
                                                var lng="{{$form['values']['lng']}}";
                                                @else
                                                var lat="{{getInitialLocation()['lat']}}";
                                                var lng="{{getInitialLocation()['lng']}}";
                                                    @endif

                                                var latlng = new google.maps.LatLng(lat,lng);

                                                var map = new google.maps.Map(document.getElementById('map'), {
                                                    center: latlng,
                                                    zoom: 10
                                                });
                                                var marker = new google.maps.Marker({
                                                    map: map,
                                                    position: latlng,
                                                    draggable: true,
                                                    anchorPoint: new google.maps.Point(0, -29)
                                                });
                                                var input = document.getElementById('searchInput');
                                                map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                                                var geocoder = new google.maps.Geocoder();
                                                var autocomplete = new google.maps.places.Autocomplete(input);
                                                autocomplete.bindTo('bounds', map);
                                                var infowindow = new google.maps.InfoWindow();
                                                autocomplete.addListener('place_changed', function () {
                                                    infowindow.close();
                                                    marker.setVisible(false);
                                                    var place = autocomplete.getPlace();
                                                    if (!place.geometry) {
                                                        window.alert("Autocomplete's returned place contains no geometry");
                                                        return;
                                                    }

                                                    // If the place has a geometry, then present it on a map.
                                                    if (place.geometry.viewport) {
                                                        map.fitBounds(place.geometry.viewport);
                                                    } else {
                                                        map.setCenter(place.geometry.location);
                                                        map.setZoom(17);
                                                    }

                                                    marker.setPosition(place.geometry.location);
                                                    marker.setVisible(true);

                                                    bindDataToForm(place.formatted_address, place.geometry.location.lat(), place.geometry.location.lng());
                                                    infowindow.setContent(place.formatted_address);
                                                    infowindow.open(map, marker);

                                                });
                                                // this function will work on marker move event into map
                                                google.maps.event.addListener(marker, 'dragend', function () {
                                                    geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                                                        if (status == google.maps.GeocoderStatus.OK) {
                                                            if (results[0]) {
                                                                bindDataToForm(results[0].formatted_address, marker.getPosition().lat(), marker.getPosition().lng());
                                                                infowindow.setContent(results[0].formatted_address);
                                                                infowindow.open(map, marker);
                                                            }
                                                        }
                                                    });
                                                });
                                            }

                                            function bindDataToForm(address, lat, lng) {
                                                // alert(document.getElementById('lat').value);
                                                document.getElementById('location').value = address;
                                                document.getElementById('lat').value = lat;
                                                document.getElementById('lng').value = lng;
                                                console.log('location = ' + address);
                                                console.log('lat = ' + lat);
                                                console.log('lng = ' + lng);
                                            }

                                            // google.maps.event.addDomListener(window, 'load', initialize);
                                        </script>
                                    @endif
                                    @if(is_array($fields))
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{$form['fields_name'][$key]}}</label>
                                            <div class="col-md-9">
                                                <div class="input-icon">
                                                    <select class="form-control select2 {{$key}}" name="{{$key}}"
                                                            @if($modal_id == 'add-admin' || $modal_id == 'edit-admin')
                                                            multiple
                                                            @endif
                                                            data-placeholder="Choose {{$form['fields_name'][$key]}}..."
                                                            id="{{$key}}">
                                                        @foreach($fields as $k=> $field)
                                                            <option value="{{$k}}"
                                                                    @if(isset($form['values']) && $form['values'][$key] == $k) selected @endif>{{ucfirst($field)}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(is_object($fields) && strpos($key,'[]') !== false)
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{$form['fields_name'][$key]}}</label>
                                            <div class="col-md-9">
                                                <div class="input-icon">
                                                    <select class="form-control select2 {{$key}}" name="{{$key}}"
                                                            @if(strpos($key,'[]') !== false) multiple
                                                            @endif data-placeholder="Choose {{$form['fields_name'][$key]}} ..."
                                                            id="{{$key}}"
                                                            style="    padding: 0;">
                                                        <option></option>

                                                        @if(strpos($key,'[]') !== false && isset($form['values'][$key]))
                                                            @foreach($form['values'][$key] as $item)
                                                                <option value="{{$item->id}}"
                                                                        @if(in_array($item->id,$roles_id)) selected @endif>{{ucfirst($item->display_name)}}</option>

                                                            @endforeach
                                                            @foreach($fields as  $k => $field)

                                                                @if(in_array($field->id,$form['values']['role_res[]'])) @continue @endif
                                                                <option
                                                                    value="{{$field->id}}">{{ucfirst($field->display_name)}}</option>
                                                            @endforeach
                                                        @else
                                                            @foreach($fields as $field)
                                                                <option value="{{$field->id}}"
                                                                        @if(isset($form['values']) && $form['values'][$key] == $field->id) selected @endif>{{ucfirst($field->display_name)}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(is_object($fields) && strpos($key,'[]') === false)
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{$form['fields_name'][$key]}}</label>
                                            <div class="col-md-9">
                                                <div class="input-icon">
                                                    <select class="form-control select2 {{$key}}" name="{{$key}}"
                                                            @if(strpos($key,'[]') !== false) multiple
                                                            @endif data-placeholder="Choose {{$form['fields_name'][$key]}} ..."
                                                            id="{{$key}}"
                                                            style="    padding: 0;">
                                                        <option></option>

                                                        @foreach($fields as $field)
                                                            @if($key == 'cat_id')
                                                                <option value="{{$field->category_id}}"
                                                                        @if(isset($form['values']) && $form['values'][$key] == $field->category_id) selected @endif>{{ucfirst($field->name)}}</option>
                                                            @else
                                                                <option value="{{$field->id}}"
                                                                        @if(isset($form['values']) && $form['values'][$key] == $field->id) selected @endif>{{ucfirst($field->name)}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <button type="submit" class="btn btn-primary closeForm save">Submit
                                                </button>
                                                <button type="button" class="btn btn-secondary closeForm"
                                                        data-dismiss="modal">Cancel
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{--                                                                            <div class="form-actions">--}}
                                {{--                                    <div class="row">--}}
                                {{--                                        <div class="col-md-12 text-center">--}}
                                {{--                                            <button type="submit" class="btn btn-circle green btn-md save"><i--}}
                                {{--                                                        class="fa fa-check"></i>--}}
                                {{--                                                Save--}}
                                {{--                                            </button>--}}
                                {{--                                            <button type="button" class="btn btn-circle btn-md red"--}}
                                {{--                                                    data-dismiss="modal">--}}
                                {{--                                                <i class="fa fa-times"></i>--}}
                                {{--                                                Cancel--}}
                                {{--                                            </button>--}}
                                {{--                                        </div>--}}
                                {{--                                    </div>--}}
                                {{--                                </div>--}}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
