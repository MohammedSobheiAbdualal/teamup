@extends(admin_layout_vw().'.index')

@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->

    <!-- END THEME GLOBAL STYLES -->
@endsection
@section('content')
    <div class="table-responsive">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">

                    </h3>
                </div>
            </div>

                <div class="m-portlet__head-tools text-right" style="margin-bottom:25px;">
                    <a href="{{url(admin_manage_url().'/admin/create')}}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air newUserBtn add-admin-mdl">
                   	<span>
										<i class="fa fa-plus"></i>
										<span>New Account</span>
									</span>
                    </a>
                </div>
                <div class="portlet-body" style="margin-top:25px;">
                    <div class="table-container">
{{--                        {!! Form::open(['method'=>'POST','url'=>url(admin_manage_url().'/admin/export')]) !!}--}}

                        <table class="table table-striped table-bordered table-hover table-checkable"
                               id="datatable_products">
                            <thead>
                            <tr role="row" class="heading">
                                <th width="1%">
                                    {{--<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">--}}
                                    {{--<input type="checkbox" class="group-checkable"--}}
                                    {{--data-set="#sample_2 .checkboxes"/>--}}
                                    {{--<span></span>--}}
                                    {{--</label>--}}
                                </th>

                                <th width="10%"> Name</th>
                                <th width="10%"> Email</th>
                                <th width="10%"> Level</th>
                                <th width="10%"> Action</th>
                            </tr>
                            <tr role="row" class="filter">
                                <td></td>
                                <td>
                                    <input type="text" class="form-control form-filter input-md" name="name"
                                           placeholder="Name" id="name">

                                </td>
                                <td>
                                    <input type="email" class="form-control form-filter input-md" name="email"
                                           placeholder="Email" id="email">
                                </td>
                                <td>
                                    <select class="form-control input-md level select2" name="level" id="level" data-placeholder="Choose Level">
                                        <option value="">Choose level</option>
                                        <option value="admin">Admin</option>
                                        <option value="player">Player</option>
                                    </select>
                                </td>
                                <td>
                                    <div class="margin-bottom-5">
                                        <a href="javascript:;"
                                           class="btn btn-sm btn-success btn-circle btn-icon-only filter-submit margin-bottom" title="Search">
                                            <i class="fa fa-search"></i>
                                        </a>

                                        <a href="javascript:;" class="btn btn-sm btn-danger btn-circle btn-icon-only filter-cancel" title="Empty">
                                            <i class="fa fa-times"></i>
                                        </a>
{{--                                        <button type="submit"--}}
{{--                                                class="btn btn-sm btn-default btn-circle btn-icon-only filter-export margin-bottom" title="Export">--}}
{{--                                            <i class="fa fa-file-excel-o"></i>--}}
{{--                                        </button>--}}
                                    </div>

                                </td>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        {!! Form::close() !!}

                    </div>
                </div>
                <div class="addNewAccountForm text-left" style="display: none;">
                    <form class="m-form m-form--fit m-form--label-align-right">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group">
                                <label for="accountName">Name</label>
                                <input type="text" class="form-control m-input" id="accountName"
                                       aria-describedby="" placeholder="Enter Your Name">
                            </div>
                            <div class="form-group m-form__group">
                                <label for="accountUserName">user Name</label>
                                <input type="text" class="form-control m-input" id="accountUserName"
                                       aria-describedby="" placeholder="Enter Your Name">
                            </div>
                            <div class="form-group m-form__group">
                                <label for="accountEmail">Email address</label>
                                <input type="email" class="form-control m-input" id="accountEmail"
                                       aria-describedby="emailHelp" placeholder="Enter email">

                            </div>
                            <div class="form-group m-form__group">
                                <label for="accountPassword">Password</label>
                                <input type="password" class="form-control m-input" id="accountPassword"
                                       placeholder="Password">
                            </div>

                            <div class="form-group m-form__group">
                                <label for="exampleSelect1">Select Type</label>
                                <select class="form-control m-input" id="exampleSelect1">
                                    <option>Super Admin</option>
                                    <option>Admin</option>

                                </select>
                            </div>
                            <div class="form-group m-form__group">
                                <label for="exampleSelect2">Select level</label>
                                <select class="form-control m-input" id="exampleSelect2">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                </select>
                            </div>

                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <button type="reset" class="btn btn-primary closeForm">Submit</button>
                                <button type="reset" class="btn btn-secondary closeForm">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>

        <div class="m-portlet__body table-responsive">
            <table class="table table-striped table-bordered table-hover table-checkable order-column"
                   id="admins_tbl">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>name</th>
                    <th>username</th>
                    <th>email</th>
                    <th>level</th>
                    <th>Status</th>
                    <th>action</th>
                </tr>
                </thead>
                <tbody>


                </tbody>

            </table>

        </div>
    </div>


@endsection

@section('js')

    <script src="{{url('/')}}/assets/js/admins.js" type="text/javascript"></script>

@stop
