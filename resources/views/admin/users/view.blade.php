@extends(admin_layout_vw().'.index')

@section('css')
    <link href="{{url('/')}}/assets/global/plugins/socicon/socicon.css" rel="stylesheet" type="text/css"/>

    <link href="{{url('/')}}/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{url('/')}}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css"
          rel="stylesheet" type="text/css"/>
    <link href="{{url('/')}}/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet"
          type="text/css"/>
    <style>
        .thumbnail a > img, .thumbnail > img {
            max-width: 325px !important;
        }

        .thumbnail {
            border: none !important;
        }

        .borderLeft {
            border-left: black;
            border-left-width: 1px;
            border-left-style: solid;
        }

        .borderBottom {
            border-bottom: black;
            border-bottom-width: 1px;
            border-bottom-style: solid;
        }
    </style>
@endsection
@section('content')



@endsection
@section('js')


@stop
