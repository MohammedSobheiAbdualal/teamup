@extends(admin_layout_vw().'.index')

@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->

@endsection
@section('content')

    <div class="table-responsive">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">

                    </h3>
                </div>
            </div>
                <div class="portlet-body">
                    <div class="table-container">
{{--                        {!! Form::open(['method'=>'POST','url'=>url(admin_manage_url().'/user/export')]) !!}--}}
{{----}}
                        <table class="table table-striped table-bordered table-hover table-checkable"
                               id="datatable_products">
                            <thead>
                            <tr role="row" class="heading">
                                <th width="1%">

                                </th>

                                <th width="30%"> Full Name</th>
                                <th width="30%"> Email</th>
                                <th width="20%"> Activation</th>
                                <th width="20%"> Action</th>
                            </tr>
                            <tr role="row" class="filter">
                                <td></td>
                                <td>
                                    <input type="text" class="form-control form-filter input-md" name="full_name"
                                           placeholder="Full name" id="full_name">
                                    {{--<input type="text" class="form-control form-filter input-md" name="name" placeholder=" اسم المستخدم" id="name">--}}
                                </td>
                                <td>
                                    <input type="email" class="form-control form-filter input-md" name="email"
                                           placeholder="Email" id="email">
                                </td>
                                <td>
                                    <select class="form-control input-md select2" name="is_active"
                                            data-placeholder="Choose Activation"
                                            id="is_active">
                                        <option value="">Choose Activation</option>
                                        <option value="1">{{trans('app.activate')}}</option>
                                        <option value="0">{{trans('app.deactivate')}}</option>
                                    </select>
                                </td>


                                <td rowspan="4">
                                    <div class="margin-bottom-5">
                                        <a href="javascript:;"
                                           class="btn btn-sm btn-success btn-circle btn-icon-only filter-submit margin-bottom"
                                           title="Search">
                                            <i class="fa fa-search"></i>
                                        </a>

                                        <a
                                            href="javascript:;"
                                           class="btn btn-sm btn-danger btn-circle btn-icon-only filter-cancel"
                                           title="Empty">
                                            <i class="fa fa-times"></i>
                                        </a>
{{--                                        <button type="submit"--}}
{{--                                                class="btn btn-sm btn-default btn-circle btn-icon-only filter-export margin-bottom"--}}
{{--                                                title="Export">--}}
{{--                                            <i class="fa fa-file-excel-o"></i>--}}
{{--                                        </button>--}}
                                    </div>

                                </td>
                            </tr>

                            </thead>
                            <tbody></tbody>
                        </table>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
{{--                        <span class="caption-subject bold uppercase"> {{$main_title}}</span>--}}
                    </div>

                </div>
                <div class="portlet-body table-responsive">

                    <table class="table table-striped table-bordered table-hover table-checkable order-column"
                           id="users_tbl">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th> Full name</th>
                            <th> Avatar</th>
                            <th> Email</th>
                            <th> Mobile</th>
                            <th> City</th>
                            <th> Activation</th>
                            <th> Register Date</th>
                            <th> Last login</th>
                            <th> Create League</th>
                            <th width="100px"> Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>


@endsection

@section('js')
    <script src="{{url('/')}}/assets/js/users.js" type="text/javascript"></script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
@stop
