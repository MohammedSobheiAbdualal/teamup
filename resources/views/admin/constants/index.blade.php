@extends(admin_layout_vw().'.index')
@section('css')
    <style>
        .m-content{
            padding-top:50px;
        }
        .categBox {
            height: 150px;
            width: 100%;
            display: table;
            transition:.5s all

        }
        .col-md-4{
            padding:10px 30px  !important;
        }

        .categ-text {
            color:white;
            font-size: 1.5rem;
            font-weight: 600;
            display: table-cell;
            vertical-align: middle;
            text-align: center;

        }
        .categLink:hover{
            text-decoration: none;
        }

    </style>
@endsection
@section('content')

    <div class="m-portlet__body">
        <div class="m-grid__item m-grid__item--fluid m-wrapper" style="background-color:white;padding-top: 50px;">


            <div class="m-content" >
                <div class="row">
                    <div class="col-md-4">
                        <a href="{{route('payments.index')}}" class="categLink">
                            <div class="categBox m--bg-brand">
                                <h4 class="categ-text">Payment Method</h4>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="{{route('awards.index')}}" class="categLink">
                            <div class="categBox m--bg-info">
                                <h4 class="categ-text">Awards</h4>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="{{route('cities.index')}}" class="categLink">
                            <div class="categBox m--bg-danger">
                                <h4 class="categ-text">Cities</h4>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="{{route('facilityGrounds.index')}}" class="categLink">
                            <div class="categBox m--bg-success ">
                                <h4 class="categ-text">Facility Grant </h4>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="{{route('facilitySizes.index')}}" class="categLink">
                            <div class="categBox m--bg-accent">
                                <h4 class="categ-text">Facility Size</h4>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="introduction.html" class="categLink">
                            <div class="categBox m--bg-warning">
                                <h4 class="categ-text">Instructions</h4>
                            </div>
                        </a>
                    </div>

                </div>

            </div>
        </div>

    </div>

@endsection
@section('js')

@stop
