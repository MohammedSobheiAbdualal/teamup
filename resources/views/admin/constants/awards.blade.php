@extends(admin_layout_vw().'.index')
@section('css')

@endsection
@section('content')

    <div class="m-portlet__body">
        <div class="m-grid__item m-grid__item--fluid m-wrapper" style="background-color:white;padding-top: 50px;">


            <div class="m-content" >
                <div class="portlet-body table-responsive">
                    <table class="table table-striped table-bordered table-hover table-checkable order-column"
                           id="awards_tbl">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Title(En)</th>
                            <th>Title(Ar)</th>
                            <th>action</th>
                        </tr>
                        </thead>

                    </table>
                </div>

            </div>
        </div>

    </div>

@endsection
@section('js')
    <script src="{{url('/')}}/assets/js/awards.js" type="text/javascript"></script>
@stop
