@extends(admin_layout_vw().'.index')
@section('css')
    <style>
        .m-content{
            padding-top:50px;
        }
        .categBox {
            height: 150px;
            width: 100%;
            display: table;
            transition:.5s all

        }
        .col-md-4{
            padding:10px 30px  !important;
        }

        .categ-text {
            color:white;
            font-size: 1.5rem;
            font-weight: 600;
            display: table-cell;
            vertical-align: middle;
            text-align: center;

        }
        .categLink:hover{
            text-decoration: none;
        }

    </style>
@endsection
@section('content')
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">

        </div>
        <div class="m-portlet__head-tools text-right" style="margin-bottom:25px;">

            <a href="{{route('cities.create')}}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air add-city-mdl" >
                            <span>
                                <i class="fa fa-plus"></i>
                                <span>New City</span>
                            </span>
            </a>

        </div>

    </div>
    <div class="m-portlet__body">
        <div class="m-grid__item m-grid__item--fluid m-wrapper" style="background-color:white;padding-top: 50px;">


            <div class="m-content" >
                <div class="portlet-body table-responsive">
                    <table class="table table-striped table-bordered table-hover table-checkable order-column"
                           id="cities_tbl">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name(En)</th>
                            <th>Name(Ar)</th>

                            <th>action</th>
                        </tr>
                        </thead>

                    </table>
                </div>

            </div>
        </div>

    </div>

@endsection
@section('js')
    <script src="{{url('/')}}/assets/js/cities.js" type="text/javascript"></script>
@stop
