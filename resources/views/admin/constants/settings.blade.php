@extends(admin_layout_vw().'.index')

@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{url('/')}}/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{url('/')}}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css"
          rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{url('/')}}/assets/global/css/components-md.min.css" rel="stylesheet" id="style_components"
          type="text/css"/>
    <link href="{{url('/')}}/assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME GLOBAL STYLES -->
@endsection
@section('content')

    <div class="row">

        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="fa fa-search font-dark"></i>
                        <span class="caption-subject bold uppercase"> Filter </span>
                    </div>

                </div>
                <div class="portlet-body">
                    <div class="table-container">
                        {!! Form::open(['method'=>'POST','url'=>url(admin_constant_url().'/setting/export')]) !!}

                        <table class="table table-striped table-bordered table-hover table-checkable"
                               id="datatable_products">
                            <thead>
                            <tr role="row" class="heading">
                                <th width="1%">
                                </th>
                                <th width="20%"> Title</th>
                                <th width="10%"> Language</th>
                                <th width="10%"> Type</th>
                                <th width="10%"> Action</th>
                            </tr>
                            <tr role="row" class="filter">
                                <td></td>
                                <td>
                                    <input type="text" class="form-control form-filter input-md" name="title"
                                           placeholder="Title" id="title">

                                </td>
                                <td>
                                    <select class="form-control input-md lang_id select2" name="lang_id" id="language_id"
                                            data-placeholder="Choose Language">
                                        <option value="">Choose Language</option>
                                        @foreach($languages as $language)
                                            <option value="{{$language->id}}">{{$language->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control input-md type select2" name="field_type" id="field_type"
                                            data-placeholder="Choose Type">
                                        <option value="">Choose Type</option>
                                        <option value="text">Text</option>
                                        <option value="number">Number</option>
                                    </select>
                                </td>
                                <td>
                                    <div class="margin-bottom-5">
                                        <a href="javascript:;"
                                           class="btn btn-sm btn-success btn-circle btn-icon-only filter-submit margin-bottom"
                                           title="Search">
                                            <i class="fa fa-search"></i>
                                        </a>

                                        <a href="javascript:;"
                                           class="btn btn-sm btn-danger btn-circle btn-icon-only filter-cancel"
                                           title="Empty">
                                            <i class="fa fa-times"></i>
                                        </a>
                                        <button type="submit"
                                                class="btn btn-sm btn-default btn-circle btn-icon-only filter-export margin-bottom"
                                                title="Export">
                                            <i class="fa fa-file-excel-o"></i>
                                        </button>
                                    </div>

                                </td>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="{{$icon}} font-dark"></i>
                        <span class="caption-subject bold uppercase"> {{$sub_title}}</span>
                    </div>
                    <div class="actions">
                        <a href="{{url(admin_constant_url().'/setting/create')}}" class="btn btn-circle btn-info add-setting-mdl">
                            <i class="fa fa-plus"></i>
                            <span class="hidden-xs"> Add New </span>
                        </a>
                    </div>

                </div>


                <div class="portlet-body">

                    <table class="table table-striped table-bordered table-hover table-checkable order-column"
                           id="settings_tbl">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th> Title</th>
                            <th> Description</th>
                            <th> Language</th>
                            <th> Type</th>
                            <th> Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

@endsection

@section('js')

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{url('/')}}/assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="{{url('/')}}/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="{{url('/')}}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"
            type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{{url('/')}}/assets/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    {{--    <script src="{{url('/')}}/assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>--}}

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{url('/')}}/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script src="{{url('/')}}/assets/js/settings.js" type="text/javascript"></script>

@stop