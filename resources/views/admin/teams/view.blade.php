@extends(admin_layout_vw().'.index')

@section('css')
    <style>
        .background-black{
            background-color:rgba(0,0,0,.8);
            text-align: center;
            padding:10px 0;
        }
        .dateTeam{
            color:white;
            margin-top:20px;
        }
        .viewTeam{

            width:100%;
            background-size: cover;
            padding:100px;
            text-align: center;
        }
        img{
            width:100%;
        }
        .teamLogo{
            margin:0 auto;
            text-align: center;
            width:120px;
        }
        .teamName{
            font-size:18px;
            color:white;
        }
        .whiteBox{
            display:inline-block;
            margin-right:20px;
            width:70px;
            height:50px;

            text-align: center;
            background-color:rgba(255,255,255,.4);
            color:white;
            border-radius: 5px;}
        .ownerImg{
            width:20%;
            height:100px;
            display: inline-block;
        }
        .ownerImg img{
            border-radius: 50%;
        }
        .ownerName{
            width:75%;
            display:inline-block
        }
        .ownerName h3{
            font-size:16px;
            margin-bottom:2px;
        }
        .ownerName span{
            font-size:12px;
        }
    </style>
@endsection
@section('content')
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                                    <span class="m-portlet__head-icon m--hide">
                                        <i class="la la-gear"></i>
                                    </span>
                    <h3 class="m-portlet__head-text">
                        Team Details
                    </h3>
                </div>
            </div>
        </div>

        <!--begin::Form-->
        <div class="col-lg-6">
            <div class="viewTeam" style=" background-image: url({{@$team->bg_image}});" >

                <div class="teamLogo">
                    <img src="{{@$team->logo}}" alt="" class="img-circle">
                </div>
            </div>
                <div class="background-black">
                <h4 class="teamName text-center">{{$team->name}}</h4>
                <div class="details">
                    <div class="players whiteBox">
                        players<br>
                        {{$team->total_players}}
                    </div>
                    <div class="awards whiteBox">
                        awards<br>
                        {{$team->total_awards}}
                    </div>
                    <div class="wins whiteBox">
                        wins<br>
                        {{$team->won_percent}}
                    </div>
                    <div class="followers whiteBox">
                        followers<br>
                        {{$team->followers()->count()}}
                    </div>
                </div>
                <div class="dateTeam text-center">
                    {{$team->created_at->toDateString()}}
                </div>
            </div>

            <div class="managerRate">

            </div>
            <div class="Bio">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Team Bio</h5>
                        <p class="card-text"> {{$team->bio}}</p>
                        <div class="owner">
                            <div class="ownerImg">
                                <img src="{{@$team->owner->avatar}}" alt="" class="img-circle">
                            </div>
                            <div class="ownerName">
                                <h3>Owner Name</h3>
                                <span>{{$team->owner->first_name.' '.$team->owner->last_name}}</span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-lg-6">

        </div>
        <!--end::Form-->
    </div>


@endsection
@section('js')

@stop
