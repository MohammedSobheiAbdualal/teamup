@extends(admin_layout_vw().'.index')

@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <style>
        .form-group {
            margin-top: 15px;
        }

        /*.listItem {*/
        /*    cursor: pointer;*/
        /*    margin: 10px;*/

        /*}*/

        /*.listItem img {*/
        /*    transition: .3s;*/
        /*}*/

        /*.listItem:hover img {*/
        /*    transform: scale(1.1);*/
        /*    transition: .3s;*/

        /*}*/

        .Title {
            color: black;
            font-size: 18px;
            font-weight: bold;
        }

        .sportBtn {
            display: none;
            margin-top: 10px;
        }

        .PositionTable {
            display: none;
        }

        #positionTable {
            display: none;
        }
        .list{
            display: inline-block!important;
        }

        input[type="radio"], input[type="checkbox"] {

            display: inline-block!important;
            margin:30px;
        }
        .input-hidden {

            position: absolute;
            /*left: -9980px;*/
            display:block!important;
        }

        input[type=radio]:checked + span {
            border: 1px solid #fff;
            box-shadow: 0 0 3px 3px #090;
        }

        /* Stuff after this is only to make things more pretty */
        input[type=radio] + span {
            /*border: 1px dashed #444;*/
            width: 130px;
            height: 150px;
            transition: 500ms all;
        }

        input[type=radio]:checked + span {
            transform:
                rotateZ(-10deg)
                rotateX(10deg);
        }
        .dropzone .dz-preview .dz-progress {
            opacity: .50;
        }
        .dropzone .dz-preview .dz-progress{
            display:none!important;
        }
    </style>
@endsection
@section('content')
    <div class="m-content" style="background-color:white !important">
        <!-- END: Subheader -->


        <div class="m-portlet__body">

            <div class="AddSport">
                <div class="modal-header">
                    <h5 class="modal-title">Create New Sport</h5>
                </div>

                <form class="m-form m-form--fit m-form--label-align-right" style="padding-top:20px;" id="form">

                    @csrf
                    <div class="m-portlet__body">
                        <div class="row"> <span class="alert-error" style="color:red"></span></div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="SportName" class="Title">Sport's Name (EN)</label>
                                    <input type="text" class="form-control m-input" id="SportName"
                                           aria-describedby="" placeholder="Enter Your Name" name="name_en" autofocus>
                                </div>
                                <div class="form-group m-form__group">
                                    <label for="ArabicName" class="Title"> Sport's Name (AR) </label>
                                    <input type="text" class="form-control m-input" id="ArabicName"
                                           aria-describedby="" placeholder="" name="name_ar">
                                </div>

                                <div class="form-group m-form__group">
                                    <label for="" class="Title" style="padding-right:20px;">select Icon:</label>
                                    <div class="list">
                                        @foreach($icons as $icon)
                                         <input type="radio"
                                                       name="icon"
                                                       id="icon" value="{{$icon->id}}" class="input-hidden">
                                            <span class="{{$icon->title}} " style="line-height: 2;font-size: -webkit-xxx-large;">
                                                </span>
                                        @endforeach
                                    </div>

                                </div>
                            </div>

{{--                            left side--}}
                            <div class="col-md-6">
                                <form class="m-form m-form--fit m-form--label-align-right">
                                    <div class="m-portlet__body">
                                        <div class="form-group m-form__group row">
                                            <label class="col-form-label col-lg-3 col-sm-12">Image Upload</label>
                                            <div class="col-lg-8 col-md-9 col-sm-12">
                                                <div class="m-dropzone dropzone" action="#" id="m-dropzone-one">
                                                    <div class="m-dropzone__msg dz-message needsclick">
                                                        <h3 class="m-dropzone__msg-title">Drop files here or click to upload.</h3>
                                                        <span class="m-dropzone__msg-desc"></span>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                </form>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group m-form__group">
                                            <label for="sportPosition" class="Title"
                                                   style="padding-right:20px;">Sport
                                                Position:</label>
                                            <label
                                                class="m-checkbox m-checkbox--air m-checkbox--solid m-checkbox--state-success">
                                                <input type="checkbox" id="sportPosition"
                                                       onclick='myFunction();' name="has_position" value="1">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>


                                <div class="col-md-6">
                                    <a href="#"
                                       class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air sportBtn"
                                       data-toggle="modal" data-target="#sportModal">
                                                        <span>
                                                            <i class="fa fa-plus"></i>
                                                            <span>ADD</span>
                                                        </span>

                                    </a>
                                    <div class="modal fade" id="sportModal" tabindex="-1" role="dialog"
                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="sportModal">Add Sport
                                                        Position</h5>
                                                    <button type="button" class="close"
                                                            data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form id="addNewPosition" name="addNewPosition">
                                                        <div class="form-group m-form__group">
                                                            <label for="PositionEn"
                                                                   class="Title">Position's
                                                                Name(EN)</label>
                                                            <input type="text" name="PositionEn"
                                                                   class="form-control m-input"
                                                                   id="PositionEn" aria-describedby=""
                                                                   placeholder="Enter Position's Name (EN)">

                                                        </div>
                                                        <div class="form-group m-form__group">
                                                            <label for="PositionAr"
                                                                   class="Title">Position's
                                                                Name(AR)
                                                            </label>
                                                            <input type="text" name="PositionAr"
                                                                   class="form-control m-input"
                                                                   id="PositionAr"
                                                                   aria-describedby="" placeholder="Enter Position's Name (AR)">

                                                            <div class="form-group m-form__group">
                                                                <p class="nameVal" style="color: red;font-weight: bold"></p>
                                                            </div>

                                                        </div>
                                                        <div class="form-group m-form__group">
                                                            <label for="PositionAr"
                                                                   class="Title">
                                                                Position Abbreviation
                                                            </label>
                                                            <input type="text" name="abbrev"
                                                                   class="form-control m-input"
                                                                   id="abbrev"
                                                                   aria-describedby="" placeholder="Enter Abbreviation ">

                                                            <div class="form-group m-form__group">
                                                                <p class="abbrevVal" style="color: red;font-weight: bold"></p>
                                                            </div>

                                                        </div>
                                                        <div class="m-form__actions">
                                                            <button type="button" class="btn btn-primary submitBtn"
                                                                    form="addNewPosition" id="PositionSubmit"
                                                                    value="Submit">Submit
                                                            </button>
                                                            <button type="reset" class="btn btn-secondary"
                                                                    data-dismiss="modal"
                                                                    aria-label="Close">Cancel
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <table class=" table table-striped- table-bordered table-hover"
                                           id="positionTable" style="margin-top:10px;">

                                        <thead>
                                        <th>PositionName (EN)</th>
                                        <th>PositionName (AR)</th>
                                        <th>Abbreviation</th>
                                        <th>Action</th>
                                        </thead>
                                        <tbody id="PositionBodyTable">

                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="m-portlet__foot m-portlet__foot--fit">
                                <div class="m-form__actions">
                                    <button type="button" class="btn btn-primary save" form="addNewSport"
                                            value="Submit">Submit
                                    </button>
                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>

@endsection

@section('js')
    <script src="{{url('/')}}/assets/js/sports.js" type="text/javascript"></script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script>
        function myFunction() {
            if ($("#sportPosition").prop('checked')) {
                $(".sportBtn").css("display", "inline-block");
            } else {
                $(".sportBtn").css("display", "none");
                $("#positionTable").css("display", "none");


            }
        }
        var myDropzone;
        Dropzone.autoDiscover = false;
        myDropzone = new Dropzone("#m-dropzone-one", {
            autoProcessQueue: false,
            acceptedFiles: "image/*",
        });

        Dropzone.autoDiscover = false;

        $(document).on('click', '.dz-remove', function () {
            This.parent('.dz-preview').remove();

        });
    </script>
@stop
