<!--begin::Global Theme Styles -->
<link href="{{url('/')}}/assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

<!--RTL version:<link href="assets/vendors/base/vendors.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
<link href="{{url('/')}}/assets/demo/demo12/base/style.bundle.css" rel="stylesheet" type="text/css" />
<link href="{{url('/')}}/assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

<!--RTL version:<link href="assets/demo/demo12/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

<!--end::Global Theme Styles -->

<!--begin::Page Vendors Styles -->
<link href="{{url('/')}}/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />

<!--RTL version:<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

<!--end::Page Vendors Styles -->
<link rel="shortcut icon" href="{{url('/')}}/assets/demo/demo12/media/img/logo/favicon.ico" />
<link rel="shortcut icon" href="{{url('/')}}/assets/demo/default/base/customStyle.css" />
<link rel="stylesheet" href="{{url('/')}}/assets/css/icon.css" />
<link rel="stylesheet" href="{{url('/')}}/assets/app/css/bootstrap-fileinput.css" />

<link href="{{url('/')}}/assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
<link href="{{url('/')}}/assets/demo/demo12/base/style.bundle.css" rel="stylesheet" type="text/css" />
<!--begin::Web font -->

<link href="{{url('/')}}/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<!--end::Web font -->
<style>
    .btn.btn-icon.btn-circle {
        border-radius: 50%;
    }
    .btn.btn-icon {
        height: 3rem;
        width: 3rem;
    }
    .btn.btn-icon {
        /*display: -webkit-inline-box;*/
        display: -ms-inline-flexbox;
        display: inline-flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        padding: 0;
    }

    .img-circle{
        border-radius: 50%!important;
        height: 100px;
        width: 100px;
    }
</style>
@yield('css')
