@extends(admin_layout_vw().'.index')

@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->

@endsection
@section('content')

    <div class="table-responsive">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">

                    </h3>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    {{--                    {!! Form::open(['method'=>'POST','url'=>url(admin_manage_url().'/user/export')]) !!}--}}

                    <table class="table table-striped table-bordered table-hover table-checkable"
                           id="datatable_products">
                        <thead>
                        <tr role="row" class="heading">
                            <th width="1%">

                            </th>
                            <th width="20%"> League Name</th>
                            <th width="20%"> Sport</th>
                            <th width="20%"> Owner Name</th>
                            <th width="20%"> Activation</th>
                            <th width="10%"> Action</th>
                        </tr>
                        <tr role="row" class="filter">
                            <td></td>
                            <td>
                                <input type="text" class="form-control form-filter input-md" name="league_name"
                                       placeholder="League Name" id="league_name">

                            </td>
                            <td>
                                <select class="form-control input-md select2" name="sport_id"
                                        data-placeholder="Choose Sport"
                                        id="sport_id">
                                    <option value="">Choose Sport</option>
                                    @foreach($sports as $sport)
                                        <option value="{{$sport->id}}">{{$sport->name_en}}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <input type="text" class="form-control form-filter input-md" name="owner_name"
                                       placeholder="owner Name" id="owner_name">

                            </td>
                            <td>
                                <select class="form-control input-md select2" name="is_active"
                                        data-placeholder="Choose Activation"
                                        id="is_active">
                                    <option value="">Choose Activation</option>
                                    <option value="1">{{trans('app.activate')}}</option>
                                    <option value="0">{{trans('app.deactivate')}}</option>
                                </select>
                            </td>

                            <td rowspan="4">
                                <div class="margin-bottom-5">
                                    <a href="javascript:"
                                       class="btn btn-sm btn-success btn-circle btn-icon-only filter-submit margin-bottom"
                                       title="Search">
                                        <i class="fa fa-search"></i>
                                    </a>

                                    <a
                                        href="javascript:"
                                        class="btn btn-sm btn-danger btn-circle btn-icon-only filter-cancel"
                                        title="Empty">
                                        <i class="fa fa-times"></i>
                                    </a>
                                    {{--                                    <button type="submit"--}}
                                    {{--                                            class="btn btn-sm btn-default btn-circle btn-icon-only filter-export margin-bottom"--}}
                                    {{--                                            title="Export">--}}
                                    {{--                                        <i class="fa fa-file-excel-o"></i>--}}
                                    {{--                                    </button>--}}
                                </div>

                            </td>
                        </tr>

                        <tr role="row" class="heading">
                            <th width="1%">

                            </th>
                            <th width="20%">Status</th>
                            <th width="20%"> Type</th>
                            <th width="20%"> Started from</th>
                            <th width="20%"> To</th>
                        </tr>
                        <tr role="row" class="filter">
                            <td></td>
                            <td>
                                <select class="form-control m-input m-input--air" id="type" name="type">
                                    <option value="">Choose Type</option>
                                    <option value="open">Open</option>
                                    <option value="specify">Specify</option>
                                </select>

                            </td>
                            <td>
                                <select class="form-control m-input m-input--air" id="status" name="status">
                                    <option value="">Choose Status</option>
                                    <option value="saved">Saved</option>
                                    <option value="published">Published</option>
                                </select>
                            </td>
                            <td>
                                <input class="form-control m-input" type="date" value=""
                                        name="start_date" id="start_from">
                            </td>
                            <td>
                                <input class="form-control m-input" type="date" value=""
                                       name="end_date" id="start_to">
                            </td>

                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    {{--                        <span class="caption-subject bold uppercase"> {{$main_title}}</span>--}}
                </div>

            </div>
            <div class="portlet-body table-responsive">

                <table class="table table-striped table-bordered table-hover table-checkable order-column"
                       id="leagues_tbl">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th> League Name</th>
                        <th> Logo</th>
                        <th> Owner</th>
                        <th> Type</th>
                        <th> Sport</th>
                        <th> Start Date</th>
                        <th> End Date</th>
                        <th> Status</th>
                        <th> Activation</th>
                        <th width="100px"> Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
    </div>


@endsection

@section('js')
    <script src="{{url('/')}}/assets/js/leagues.js" type="text/javascript"></script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
@stop
