@extends(admin_layout_vw().'.index')

@section('css')
    <style>
        .background-black {
            background-color: rgba(0, 0, 0, .8);
            text-align: center;
            padding: 10px 0;
        }

        .dateTeam {
            color: white;
            margin-top: 20px;
        }

        .viewTeam {

            width: 100%;
            background-size: cover;
            padding: 100px;
            text-align: center;
        }

        img {
            width: 100%;
        }

        .teamLogo {
            margin: 0 auto;
            text-align: center;
            width: 120px;
        }

        .teamName {
            font-size: 18px;
            color: white;
        }

        .whiteBox {
            display: inline-block;
            margin-right: 20px;
            width: 80px;
            height: 50px;

            text-align: center;
            background-color: rgba(255, 255, 255, .4);
            color: white;
            border-radius: 5px;
        }

        .ownerImg {
            width: 20%;
            height: 100px;
            display: inline-block;
        }

        .ownerImg img {
            border-radius: 50%;
        }

        .ownerName {
            width: 75%;
            display: inline-block
        }

        .ownerName h3 {
            font-size: 16px;
            margin-bottom: 2px;
        }

        .ownerName span {
            font-size: 12px;
        }

        .conditions {
            border-left: 5px solid #8ebf42;
            background-color: #eee;
            list-style-type: none;
            padding: 10px 20px;
        }
        .img-circle{
            width:60px;
            height: 60px;
        }
    </style>
@endsection
@section('content')
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                                    <span class="m-portlet__head-icon m--hide">
                                        <i class="la la-gear"></i>
                                    </span>
                    <h3 class="m-portlet__head-text">
                        League Details
                    </h3>
                </div>
            </div>
        </div>

        <!--begin::Form-->
        <div class="row">
            <div class="col-lg-6">
                <div class="viewTeam" style=" background-image: url({{@$league->bg_image}});">

                    <div class="teamLogo">
                        <img src="{{@$league->logo}}" alt="" class="img-circle">
                    </div>
                </div>
                <div class="background-black">
                    <h4 class="teamName text-center">{{$league->name}}</h4>
                    <div class="details">
                        <div class="players whiteBox">
                            Start From<br>
                            {{$league->start_date}}
                        </div>
                        <div class="awards whiteBox">
                            Ended<br>
                            {{$league->end_date}}
                        </div>
                        <div class="wins whiteBox">
                            Subscribe<br>
                            {{$league->subscription_type}}
                        </div>
                        <div class="followers whiteBox">
                            Type<br>
                            {{$league->type}}
                        </div>
                    </div>
                    <div class="details" style="margin-top: 10px">
                        <div class="players whiteBox">
                            Bench<br>
                            {{$league->bench}}
                        </div>
                        <div class="players whiteBox">
                            Main Line up <br>
                            {{$league->main_lineup}}
                        </div>
                        <div class="whiteBox" style="width:200px">
                            Penalties & Extras<br>
                            {{$league->extra}}
                        </div>

                    </div>
                    <div class="dateTeam text-center">

                    </div>
                </div>

                <div class="managerRate">

                </div>
                <div class="Bio">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">League Bio</h5>
                            <p class="card-text"> {{$league->league_description}}</p>
                            <div class="owner">
                                <div class="ownerImg">
                                    <img src="{{@$league->user->avatar}}" alt="" class="img-circle">
                                </div>
                                <div class="ownerName">
                                    <h3>Owner Name</h3>
                                    <span>{{$league->user->first_name.' '.$league->user->last_name}}</span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <!--begin::Section-->
                <div class="m-portlet m-portlet--full-height" style="margin-top: 20px">
                    <div
                        class="m-accordion m-accordion--default m-accordion--solid m-accordion--section m-accordion--padding-lg"
                        id="m_accordion_8" role="tablist">

                        <!--begin::Item-->
                        <div class="m-accordion__item">
                            <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_8_item_1_head"
                                 data-toggle="collapse" href="#m_accordion_8_item_1_body" aria-expanded="    false">
                                <span class="m-accordion__item-icon"><i class="flaticon-presentation-1"></i></span>
                                <span class="m-accordion__item-title">League Conditions</span>
                                <span class="m-accordion__item-mode"></span>
                            </div>
                            <div class="m-accordion__item-body collapse" id="m_accordion_8_item_1_body" role="tabpanel"
                                 aria-labelledby="m_accordion_8_item_1_head" data-parent="#m_accordion_8">
                                <div class="m-accordion__item-content">
                                   @if(count($league->restrictions)> 0)
                                    @foreach($league->restrictions as $restriction)
                                        <h5>{{ucfirst($restriction->type)}}</h5>
                                        <ul class="conditions">
                                            <li style="list-style-type: none;">
                                                {{$restriction->text}}
                                            </li>
                                        </ul>
                                    @endforeach
                                    @endif


                                       @if(count($league->Services)> 0)
                                           @foreach($league->Services as $service)
                                               <h5>{{ucfirst($service->type)}}</h5>
                                               <ul class="conditions">
                                                   <li style="list-style-type: none;">
                                                       {{$service->text}}
                                                   </li>
                                               </ul>
                                           @endforeach
                                       @endif

                                       @if(count($league->Rules)> 0)
                                           @foreach($league->Rules as $rule)
                                               <h5>{{ucfirst($rule->type)}}</h5>
                                               <ul class="conditions">
                                                   <li style="list-style-type: none;">
                                                       {{$rule->text}}
                                                   </li>
                                               </ul>
                                           @endforeach
                                       @endif
                                </div>
                            </div>
                        </div>

                        <!--end::Item-->

                        <!--begin::Item-->
                        <div class="m-accordion__item">
                            <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_8_item_2_head"
                                 data-toggle="collapse" href="#m_accordion_8_item_2_body" aria-expanded="    false">
                                <span class="m-accordion__item-icon"><i class="flaticon-users-1"></i></span>
                                <span class="m-accordion__item-title">League Celebrities Guests</span>
                                <span class="m-accordion__item-mode"></span>
                            </div>
                            <div class="m-accordion__item-body collapse" id="m_accordion_8_item_2_body" role="tabpanel"
                                 aria-labelledby="m_accordion_8_item_2_head" data-parent="#m_accordion_8">
                                <div class="m-accordion__item-content" style="margin-bottom: 10px">
                                    <p>&nbsp</p>
                                    @if(count($league->Guests)> 0)
                                        @foreach($league->Guests as $guest)

                                            <ul class="conditions">
                                                <li style="list-style-type: none;">
                                                    <img src="{{$guest->logo?$guest->logo : url('assets/img/player.svg')}}" class="img-circle">{{$guest->guest_name}}
                                                </li>
                                            </ul>
                                        @endforeach
                                    @endif

                                </div>
                            </div>
                        </div>

                        <!--end::Item-->

                        <!--begin::Item-->
                        <div class="m-accordion__item">
                            <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_8_item_3_head"
                                 data-toggle="collapse" href="#m_accordion_8_item_3_body" aria-expanded="    false">
                                <span class="m-accordion__item-icon"><i class="flaticon-placeholder-3"></i></span>
                                <span class="m-accordion__item-title">League Facilities</span>
                                <span class="m-accordion__item-mode"></span>
                            </div>
                            <div class="m-accordion__item-body collapse" id="m_accordion_8_item_3_body" role="tabpanel"
                                 aria-labelledby="m_accordion_8_item_3_head" data-parent="#m_accordion_8">
                                <div class="m-accordion__item-content">
                                    <p>&nbsp
                                    </p>
                                    <ul class="conditions">
                                        <li style="list-style-type: none;">
                                          <h6>Size</h6> &nbsp  {{$league->Facility->size_name}}
                                       </li>
                                    </ul>

                                    <ul class="conditions">
                                        <li style="list-style-type: none;">
                                            <h6>Ground</h6> &nbsp  {{$league->Facility->ground_name}}
                                        </li>
                                    </ul>

                                    <ul class="conditions">
                                        <li style="list-style-type: none;">
                                            <h6>Images</h6>
                                            @if(count($league->Facility->images)>0)
                                                @foreach($league->Facility->images as $image)
                                                    <a href="{{$image->image}}" target="_blank" class="imgpreview"> <img class="img-circle" src="{{$image->image}}"></a>
                                                @endforeach
                                            @endif
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <!--end::Item-->
                    </div>
                </div>
                <!--end::Item-->
            </div>
        </div>
        <!--end::Form-->
    </div>


@endsection
@section('js')

@stop
