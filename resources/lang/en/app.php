<?php
/**
 * Created by PhpStorm.
 * User: mohammedsobhei
 * Date: 11/7/18
 * Time: 8:34 PM
 */

return [
    'success' => 'Successfully action.',
    'error' => 'Failure action.',

    'not_data_found' => 'Data not found.',
    'invalid_token' => 'Invalid token.',
    'invalid_route' => 'Invalid route.',
    'client_input_error' => 'Client input error.',
    'required_positions' => 'Primary Positions and Secondary Position is required',
    'server_error' => 'Server error.',
    'code' => 'code is wrong',
    'user_notfound' => 'User is not exist.',
    'updated' => 'Updated successfully.',
    'not_updated' => 'Updated failure.',
    'mobile_email_not_found' => 'Email or mobile was incorrect.',

    'follow' => 'Follow successfully.',
    'unfollow' => 'Unfollow successfully.',

    'favorite' => 'Favorite successfully.',
    'unfavorite' => 'Unfavorite successfully.',

    'friend' => 'Friend successfully.',
    'unfriend' => 'Unfriend successfully.',

    'created' => 'Created successfully.',
    'not_created' => 'Created failure.',
    'deleted' => 'Deleted successfully',

    'invited' => 'Invited successfully',
    'not_invited' => 'Invited failure',

    'sent_code_verification' => 'Verification Code was sent to your mobile',
    'sent_forget_sms' => 'SMS with New Password was sent to your mobile.',
    'no_permission' => 'You do not have a permission',
     'payment_updated'=>'Payment has been Updated',
    'activate' => 'Active',
    'deactivate' => 'Suspended',

    'password_not_match' => 'Old password is incorrect',
    'password_updated' => 'Your password has been updated successfully',

    'verified' => 'Verified',
    'unverified' => 'Unverified',
    'user' => 'User',
    'not_mobile_found' => 'Mobile number is Incorrect',
    'user_created' => 'User was created',
    'user_updated' => 'User was updated.',

    'max_sport' => 'Sports must be between 1 to 3',

    'owner' => 'owner',
    'manager' => 'manager',
    'player' => 'player',
    'guest' => 'guest',
    'all' => 'All',

    'team' => 'Team: :name',

    'invitation_accept' => 'Invitation has been accepted',
    'invitation_cancel' => 'Invitation has been canceled',
    'invitation_reject' => 'Invitation has been rejected',
    'notification_message' => [
        'user_activate' => 'Your account was activated',
        'user_deactivate' => 'Your account was deactivated',
        'invite' => 'New invitation',
        'join' => 'Join your team',
        'accept' => 'Accept an invitation',
        'reject' => 'Reject an invitation',
        'cancel' => 'Cancel an invitation',
        'leave' => 'Leave team',
        'friend' => 'Added you as a Friend',
        'follow' => 'Follow you',
        'follow_team' => 'Follow your team',
        'follow_league' => 'Follow your league',
        'chat' => 'new message',

    ],
    'leagues' => [
        'status' => [
            'upcoming' => 'Upcoming',
            'ongoing' => 'Ongoing',
            'ended' => 'Ended',
        ],
        'invitation_status' => [
            'accepted' => 'Accepted',
            'rejected' => 'Rejected',
            'confirmed' => 'Confirmed',
            'joined' => 'Joined',
            'invited' => 'Invited',
            'request' => 'Request',
            'my_league' => 'My leagues',
            'withdraw' => 'withdrawn',

        ],
    ]
];
