<?php
/**
 * Created by PhpStorm.
 * User: mohammedsobhei
 * Date: 11/7/18
 * Time: 8:34 PM
 */

return [
    'success' => 'تمت العملية بنجاح',
    'error' => 'عذراً، لقد حدث خطأ ما',

    'not_data_found' => '.لا يوجد بيانات',
    'invalid_token' => 'رمز غير صالح',
    'invalid_route' => 'مسار خاطئ',
    'client_input_error' => 'خطأ في الإدخال',
    'required_positions' => 'يجب تحديد المراكز الأساسية والثانوية',
    'server_error' => 'خطأ في الخادم',
    'code' => 'رمز خاطئ',
    'user_notfound' => 'مستخدم غير موجود',
    'updated' => 'تم التحديث',
    'not_updated' => 'فشل التحديث.',
    'mobile_email_not_found' => 'الإيميل أو رقم الموبايل غير صحيح',

    'follow' => 'تمت المتابعة',
    'unfollow' => 'تم إلغاء المتابعة',

    'favorite' => 'تمت الاضافة للمفضلة',
    'unfavorite' => 'تم الحذف من المفضلة',

    'friend' => 'تمت قبول الصداقة',
    'unfriend' => 'تم إلغاء الصداقة',

    'created' => 'تم الإنشاء',
    'not_created' => 'فشل الإنشاء',
    'deleted' => 'تم الحذف بنجاح',
    'invited' => 'تمت الدعوة بنجاح',
    'not_invited' => 'فشل الدعوة',

    'sent_code_verification' => 'تم إرسال رمز التحقق الي رقم هاتفك',
    'sent_forget_sms' => 'تم إرسال رسالة بكلمة المرور الجديدة رقم هاتفك',

    'activate' => 'نشط',
    'deactivate' => 'موقوف',

    'verified' => 'مؤكد',
    'unverified' => 'غير مؤكد',
    'user' => 'مستخدم+',
    'not_mobile_found' => 'رقم الهاتف غير صحيح',
    'user_created' => 'تم إنشاء مستخدم',
    'user_updated' => 'تم تحديث ملف مستخدم',

    'max_sport' => 'يجب اختيار 1 الي 3 رياضات',
    'no_permission' => 'ليس لديك الصلاحية',

    'owner' => 'مالك',
    'manager' => 'مدير',
    'player' => 'لاعب',
    'guest' => 'زائر',

    'all' => 'الكل',
    'password_not_match' => 'كلمة المرور القديمة غير صحيحة',
    'password_updated' => 'لقد تم تحديث كلمة السر الخاصة بك بنجاح',
    'team' => ':name الفريق:',
    'invitation_accept' => 'تم قبول الدعوة',
    'invitation_cancel' => 'تم إلغاء الدعوة',
    'invitation_reject' => 'تم رقض الدعوة',

    'notification_message' => [
        'user_activate' => 'تم تفعيل حسابك',
        'user_deactivate' => 'تم تعطيل حسابك',
        'invite' => 'دعوة جديدة',
        'join' => 'انضم لفريقك',
        'accept' => 'قبول الدعوة',
        'reject' => 'رفض الدعوة',
        'cancel' => 'إلغاء الدعوة',
        'leave' => 'مغادرة الفريق',
        'friend' => 'قام بإضافتك كصديق',
        'follow' => 'قام بمتابعتك',
        'follow_team' => 'قام بمتابعة فريقك',
        'follow_league' => 'قام بمتابعة بطولتك',
        'chat' => 'رسالة جديدة',

    ],
    'leagues' => [
        'status' => [
            'upcoming' => 'القادمة',
            'ongoing' => 'االحالية',
            'ended' => 'المنتهية',
        ],
        'invitation_status' => [
            'accepted' => 'مقبولة',
            'rejected' => 'مرفوضة',
            'confirmed' => 'مؤكدة',
            'joined' => 'منضم',
            'invite' => 'دعوة',
            'request' => 'طلب',
            'my_league' => 'بطولاتي',
            'withdraw' => 'منسحب',
        ],
    ]
];
