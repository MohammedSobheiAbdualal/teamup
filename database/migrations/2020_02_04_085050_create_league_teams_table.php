<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeagueTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('league_teams', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id'); // (league creator or team owner) who makes invite
            $table->unsignedBigInteger('league_id');
            $table->unsignedBigInteger('team_id');

            // withdraw after accepted or after confirm
            $table->enum('status', ['invited', 'joined', 'accepted', 'withdraw', 'reject', 'confirm', 'retry'])->default('invited');
            $table->enum('type', ['team', 'league']);
            $table->boolean('is_confirmed')->default(false); // if withdraw and is_confirmed = false so withdraw before confirmed otherwise withdraw after confirmed
            $table->string('team_phone')->nullable();
            $table->boolean('is_waiting')->default(false);

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('league_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('team_id')->references('id')->on('teams')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('league_teams');
    }
}
