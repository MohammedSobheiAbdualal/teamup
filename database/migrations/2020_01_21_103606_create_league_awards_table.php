<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeagueAwardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('league_awards', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('league_id')->nullable();
            $table->unsignedBigInteger('league_winner_id')->nullable();
            $table->unsignedBigInteger('award_id')->nullable();
            $table->string('other_award')->nullable();
            $table->double('money_value')->default(0);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('league_id')->references('id')->on('leagues')->onDelete('cascade');
            $table->foreign('league_winner_id')->references('id')->on('league_winners')->onDelete('cascade');
            $table->foreign('award_id')->references('id')->on('awards')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('league_awards');
    }
}
