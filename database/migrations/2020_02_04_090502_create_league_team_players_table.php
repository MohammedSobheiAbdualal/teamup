<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeagueTeamPlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('league_team_players', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('league_team_id');
            $table->unsignedBigInteger('league_id');
            $table->unsignedBigInteger('team_id');
            $table->unsignedBigInteger('player_id');
            $table->integer('player_no');
            $table->unsignedBigInteger('position_id')->nullable();
            $table->unsignedBigInteger('secondary_position_id')->nullable();
            $table->boolean('is_captain')->default(false);
            $table->boolean('is_injured')->default(false);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('league_team_id')->references('id')->on('league_teams')->onDelete('cascade');
            $table->foreign('league_id')->references('id')->on('leagues')->onDelete('cascade');
            $table->foreign('team_id')->references('id')->on('teams')->onDelete('cascade');
            $table->foreign('player_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('position_id')->references('id')->on('positions')->onDelete('cascade');
            $table->foreign('secondary_position_id')->references('id')->on('positions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('league_team_players');
    }
}
