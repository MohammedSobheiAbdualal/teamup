<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeaguesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leagues', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name'); //
            $table->unsignedBigInteger('user_id'); //this user must have permission to create new league
            $table->unsignedBigInteger('sport_id');
            $table->enum('type', ['open', 'specify']);
            $table->integer('team_num')->default(0); // number of teams, required if type specify
            $table->date('start_date');
            $table->date('end_date');
            $table->dateTime('deadline_date'); // deadline registration date time
            $table->enum('subscription_type', ['free', 'paid']);
            $table->unsignedBigInteger('payment_method_id')->nullable(); // required if subscription_type = paid
            $table->double('fees')->nullable(); // required if subscription_type = paid
            $table->integer('frcd')->nullable();
            $table->integer('main_lineup');
            $table->integer('bench');
            $table->integer('reserve_num')->default(0);
            $table->integer('half_duration'); //  first | second half duration in minutes
            $table->enum('extra', ['penalties', 'extra_halves_penalties']);
            $table->integer('extra_half_duration')->nullable(); //  required if extra = extra_halves_penalties
            $table->longText('short_description');
            $table->longText('league_description')->nullable();
            $table->string('logo')->nullable();
            $table->string('bg_image')->nullable();
            $table->longText('celebrities_guest_description')->nullable();
            $table->longText('general_note')->nullable();
            $table->string('contact_number')->nullable();
            $table->boolean('allow_call_me')->default(false);
            $table->boolean('has_restricted')->default(false);
            $table->boolean('has_service')->default(false);
            $table->boolean('has_rule')->default(false);
            $table->enum('status', ['published', 'saved'])->default('saved');
            $table->boolean('is_active')->default(true);
            $table->boolean('is_nlf')->default(false);

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('sport_id')->references('id')->on('sports')->onDelete('cascade');
            $table->foreign('payment_method_id')->references('id')->on('payment_methods')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leagues');
    }
}
