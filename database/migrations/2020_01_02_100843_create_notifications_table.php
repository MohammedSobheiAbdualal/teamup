<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sender_id')->nullable();                                     // weekly cron job  weekly cron job        weekly cron job
            $table->enum('action', ['user_activate', 'user_deactivate', 'invite', 'join', 'accept', 'reject', 'cancel', 'leave', 'friend', 'follow', 'follow_team', 'follow_league']);

            $table->unsignedInteger('action_id')->nullable();
            $table->string('text')->nullable();
            $table->boolean('seen')->default(false);
            $table->string('type')->nullable();
            $table->integer('data_id')->nullable();

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('sender_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
