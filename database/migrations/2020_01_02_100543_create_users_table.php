<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->date('birthdate')->nullable();
            $table->string('password')->nullable();

            $table->string('mobile')->unique();
            $table->longText('bio')->nullable();
            $table->string('avatar')->nullable();
            $table->string('country_code');
            $table->string('bg_image')->nullable();
            $table->unsignedBigInteger('lang_id')->nullable();
            $table->unsignedBigInteger('city_id')->nullable();
            $table->string('verification_code')->nullable();

            $table->integer('signup_level')->default(1);
            $table->boolean('is_active')->default(true);
            $table->boolean('is_verified')->default(false);
            $table->timestamp('last_login_date')->nullable();
            $table->boolean('is_manager')->default(false);
            $table->boolean('has_league')->default(false);

            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('lang_id')->references('id')->on('languages')->onDelete('cascade');
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
