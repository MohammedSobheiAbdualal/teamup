<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facilities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('league_id');
            $table->string('name');
            $table->unsignedBigInteger('size_id');
            $table->unsignedBigInteger('ground_id');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('size_id')->references('id')->on('facility_sizes')->onDelete('cascade');
            $table->foreign('ground_id')->references('id')->on('facility_grounds')->onDelete('cascade');
            $table->foreign('league_id')->references('id')->on('leagues')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facilities');
    }
}
