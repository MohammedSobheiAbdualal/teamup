<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $admin = new \App\Admin();
        $admin->username = 'admin';
        $admin->email = 'admin@admin.com';
        $admin->password = bcrypt('123456');
        $admin->save();

        $trainer = new \App\User();
        $trainer->name = 'Api';
        $trainer->email = 'api_t@api.com';
        $trainer->password = bcrypt('123456');
        $trainer->email_verified_at = \Carbon\Carbon::now();
        $trainer->user_type = 'trainer';
        $trainer->save();

        $user = new \App\User();
        $user->name = 'Api';
        $user->email = 'api_u@api.com';
        $user->password = bcrypt('123456');
        $user->email_verified_at = \Carbon\Carbon::now();
        $user->user_type = 'user';
        $user->save();


    }
}
