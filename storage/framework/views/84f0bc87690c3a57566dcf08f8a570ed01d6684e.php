<?php $__env->startSection('css'); ?>
    <style>
        .m-content{
            padding-top:50px;
        }
        .categBox {
            height: 150px;
            width: 100%;
            display: table;
            transition:.5s all

        }
        .col-md-4{
            padding:10px 30px  !important;
        }

        .categ-text {
            color:white;
            font-size: 1.5rem;
            font-weight: 600;
            display: table-cell;
            vertical-align: middle;
            text-align: center;

        }
        .categLink:hover{
            text-decoration: none;
        }

    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="m-portlet__body">
        <div class="m-grid__item m-grid__item--fluid m-wrapper" style="background-color:white;padding-top: 50px;">


            <div class="m-content" >
                <div class="portlet-body table-responsive">
                    <table class="table table-striped table-bordered table-hover table-checkable order-column"
                           id="payments_tbl">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Title(En)</th>
                            <th>Title(Ar)</th>
                            <th>Type</th>
                            <th>action</th>
                        </tr>
                        </thead>

                    </table>
                </div>

            </div>
        </div>

    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(url('/')); ?>/assets/js/payments.js" type="text/javascript"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make(admin_layout_vw().'.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\team-up-bitbucket\resources\views/admin/constants/payments.blade.php ENDPATH**/ ?>