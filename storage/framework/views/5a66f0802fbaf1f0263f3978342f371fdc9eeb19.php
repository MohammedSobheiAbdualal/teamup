<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
<!--begin::Global Theme Bundle -->
<script src="<?php echo e(url('/')); ?>/js/jquery.min.js" type="text/javascript"></script>


<script src="<?php echo e(url('/')); ?>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
<script src="<?php echo e(url('/')); ?>/assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

<!--end::Global Theme Bundle -->

<!--begin::Page Vendors -->
<script src="<?php echo e(url('/')); ?>/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>

<!--end::Page Vendors -->

<!--begin::Page Scripts -->
<script src="<?php echo e(url('/')); ?>/assets/app/js/dashboard.js" type="text/javascript"></script>
<script src="<?php echo e(url('/')); ?>/assets/demo/demo12/custom/components/bootbox/bootbox.min.js" type="text/javascript"></script>
<script src="<?php echo e(url('/')); ?>/assets/demo/default/custom/crud/forms/widgets/dropzone.js" type="text/javascript"></script>

<!--end::Page Scripts -->
<script>
    WebFont.load({
        google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
        active: function() {
            sessionStorage.fonts = true;
        }
    });
</script>
<script>
    var baseURL = '<?php echo e(url(admin_vw())); ?>';
    var baseAssets = '<?php echo e(url('assets')); ?>';

    $(window).keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
</script>
<?php echo $__env->yieldContent('js'); ?>

<script>
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
</script>



<!--begin::Page Vendors -->

<!--end::Page Vendors -->

<script src="<?php echo e(url('/')); ?>/assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
<script src="<?php echo e(url('/')); ?>/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="<?php echo e(url('/')); ?>/assets/demo/default/custom/crud/datatables/extensions/buttons.js" type="text/javascript"></script>
<!--begin::Page Vendors -->
<script src="<?php echo e(url('/')); ?>/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
<!--end::Page Vendors -->
<script src="<?php echo e(url('/')); ?>/assets/app/js/customjs.js" type="text/javascript"></script>

<!--end::Global Theme Bundle -->

<!--begin::Page Vendors -->
<script src="<?php echo e(url('/')); ?>/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>

<!--end::Page Vendors -->

<!--begin::Page Scripts -->
<script src="<?php echo e(url('/')); ?>/assets/app/js/dashboard.js" type="text/javascript"></script>
<script src="<?php echo e(url('/')); ?>/assets/app/js/bootstrap-fileinput.js" type="text/javascript"></script>
<!--end::Page Scripts -->

<!-- begin::Page Loader -->
<script>
    $(window).on('load', function () {
        $('body').removeClass('m-page--loading');
    });
</script>
<script>
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-top-right",
        "onclick": null,
        "showDuration": "1000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
</script>
<script>

    // var myDropzone;
    // Dropzone.autoDiscover = false;
    // myDropzone = new Dropzone("#m-dropzone-one", {
    //     autoProcessQueue: false
    // });

</script>


    
<?php echo $__env->yieldPushContent('js'); ?>
<?php /**PATH C:\xampp\htdocs\team-up-bitbucket\resources\views/admin/layout/js.blade.php ENDPATH**/ ?>