
<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=<?php echo e(google_api_key()); ?>&libraries=places&callback=initialize"></script>

<div class="modal fade" id="<?php echo e($modal_id); ?>" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo e($modal_title); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>

            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="portlet-body form">
                            <?php echo Form::open(['method'=>$form['method'],'id'=>$form['form_id'],'class'=>'form-horizontal form','url'=>$form['url'] ,'files'=>true]); ?>

                            <div class="alert alert-danger" role="alert" style="display: none"></div>
                            <?php echo $__env->make('layouts.flash-message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                            <div class="form-body">
                                <?php $__currentLoopData = $form['fields']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $fields): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($fields == 'image' || $fields == 'video'): ?>
                                        <div class="form-group m-form__group">
                                            <label class="control-label col-md-3"><?php echo e($form['fields_name'][$key]); ?></label>
                                            <div class="col-md-9">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                                         style="width: 200px; height: 150px;">
                                                        <?php if(isset($form['values'])): ?>
                                                            <?php if($fields == 'video'): ?>
                                                                <video src="<?php echo e($form['values'][$key]); ?>" controls
                                                                       width="200"></video>
                                                            <?php else: ?>
                                                                <img src="<?php echo e($form['values'][$key]); ?>">

                                                            <?php endif; ?>
                                                        <?php else: ?>
                                                            <img
                                                                src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"
                                                                alt=""/>

                                                        <?php endif; ?>
                                                    </div>
                                                    <div>
                                                            <span class="btn red btn-outline btn-file">
                                                                <span class="fileinput-new"> Select </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="<?php echo e($key); ?>"
                                                                       id="<?php echo e($key); ?>"> </span>
                                                        <a href="javascript:" class="btn red fileinput-exists"
                                                           data-dismiss="fileinput">
                                                            Remove </a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    <?php endif; ?>

                                    <?php if($fields == 'ckeditor'): ?>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo e($form['fields_name'][$key]); ?></label>

                                            <div class="col-md-12">
                                                <textarea class="summernote" name="<?php echo e($key); ?>" name="<?php echo e($key); ?>"
                                                          id="<?php echo e($key); ?>" rows="5"
                                                          placeholder="<?php echo e($form['fields_name'][$key]); ?>"><?php if(isset($form['values'])): ?><?php echo e($form['values'][$key]); ?><?php endif; ?></textarea>

                                                
                                                
                                                 </div>

                                        </div>
                                            <script
                                                src="<?php echo e(url('/')); ?>/assets/demo/default/custom/crud/forms/widgets/summernote.js"
                                                type="text/javascript"></script>

                                        <?php endif; ?>
                                    <?php if($fields == 'text'): ?>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo e($form['fields_name'][$key]); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" name="<?php echo e($key); ?>" id="<?php echo e($key); ?>" class="form-control"
                                                       placeholder="<?php echo e($form['fields_name'][$key]); ?>"
                                                       <?php if(isset($form['values'])): ?> value="<?php echo e($form['values'][$key]); ?>" <?php endif; ?>>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <?php if($fields == 'text_dis'): ?>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo e($form['fields_name'][$key]); ?></label>
                                            <div class="col-md-12">
                                                <input type="text" name="<?php echo e($key); ?>" id="<?php echo e($key); ?>" class="form-control"
                                                       placeholder="<?php echo e($form['fields_name'][$key]); ?>" disabled
                                                       <?php if(isset($form['values'])): ?> value="<?php echo e($form['values'][$key]); ?>" <?php endif; ?>>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <?php if($fields == 'time'): ?>
                                        <div class="form-group">
                                            <label class="control-label col-md-3"><?php echo e($form['fields_name'][$key]); ?></label>
                                            <div class="col-md-3">
                                                <div class="input-icon">
                                                    <i class="fa fa-clock-o"></i>
                                                    <input type="text" name="<?php echo e($key); ?>" id="<?php echo e($key); ?>"
                                                           class="form-control timepicker timepicker-24"
                                                           <?php if(isset($form['values'])): ?> value="<?php echo e($form['values'][$key]); ?>" <?php endif; ?>>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <?php if($fields == 'email'): ?>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo e($form['fields_name'][$key]); ?></label>
                                            <div class="col-md-9">
                                                <input type="email" name="<?php echo e($key); ?>" id="<?php echo e($key); ?>"
                                                       class="form-control"
                                                       placeholder="<?php echo e($form['fields_name'][$key]); ?>"
                                                       <?php if(isset($form['values'])): ?> value="<?php echo e($form['values'][$key]); ?>" <?php endif; ?>>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <?php if($fields == 'password'): ?>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo e($form['fields_name'][$key]); ?></label>
                                            <div class="col-md-9">
                                                <input type="password" name="<?php echo e($key); ?>" id="<?php echo e($key); ?>"
                                                       class="form-control"
                                                       placeholder="<?php echo e($form['fields_name'][$key]); ?>">
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <?php if($fields == 'textarea'): ?>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo e($form['fields_name'][$key]); ?></label>
                                            <div class="col-md-12">
                                                <textarea name="<?php echo e($key); ?>" id="<?php echo e($key); ?>" rows="5"
                                                          placeholder="<?php echo e($form['fields_name'][$key]); ?>"
                                                          class="form-control"><?php if(isset($form['values'])): ?><?php echo e($form['values'][$key]); ?><?php endif; ?></textarea>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if($fields == 'map'): ?>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo e($form['fields_name'][$key]); ?></label>
                                            <div class="col-md-12">
                                                <div class="col col-md-2"></div>
                                                <div class="col col-md-10 form_area">
                                                    <input id="searchInput" class="input-controls" type="text"
                                                           placeholder="Enter a location">
                                                    <div class="map" id="<?php echo e($key); ?>"
                                                         style="width: 90%; height: 300px;"></div>
                                                    <input type="hidden" <?php if(isset($form['values'])): ?> value="<?php echo e($form['values']['lat']); ?>" <?php else: ?> value="<?php echo e(getInitialLocation()['lat']); ?>" <?php endif; ?> name="lat" id="lat">
                                                    <input type="hidden" <?php if(isset($form['values'])): ?> value="<?php echo e($form['values']['lng']); ?>" <?php else: ?> value="<?php echo e(getInitialLocation()['lng']); ?>" <?php endif; ?> name="lng" id="lng">
                                                    <input type="hidden" value="Riyadh" name="location" id="location">
                                                </div>

                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                            function initialize() {
                                                    <?php if(isset($form['values'])): ?>
                                                var lat="<?php echo e($form['values']['lat']); ?>";
                                                var lng="<?php echo e($form['values']['lng']); ?>";
                                                <?php else: ?>
                                                var lat="<?php echo e(getInitialLocation()['lat']); ?>";
                                                var lng="<?php echo e(getInitialLocation()['lng']); ?>";
                                                    <?php endif; ?>

                                                var latlng = new google.maps.LatLng(lat,lng);

                                                var map = new google.maps.Map(document.getElementById('map'), {
                                                    center: latlng,
                                                    zoom: 10
                                                });
                                                var marker = new google.maps.Marker({
                                                    map: map,
                                                    position: latlng,
                                                    draggable: true,
                                                    anchorPoint: new google.maps.Point(0, -29)
                                                });
                                                var input = document.getElementById('searchInput');
                                                map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                                                var geocoder = new google.maps.Geocoder();
                                                var autocomplete = new google.maps.places.Autocomplete(input);
                                                autocomplete.bindTo('bounds', map);
                                                var infowindow = new google.maps.InfoWindow();
                                                autocomplete.addListener('place_changed', function () {
                                                    infowindow.close();
                                                    marker.setVisible(false);
                                                    var place = autocomplete.getPlace();
                                                    if (!place.geometry) {
                                                        window.alert("Autocomplete's returned place contains no geometry");
                                                        return;
                                                    }

                                                    // If the place has a geometry, then present it on a map.
                                                    if (place.geometry.viewport) {
                                                        map.fitBounds(place.geometry.viewport);
                                                    } else {
                                                        map.setCenter(place.geometry.location);
                                                        map.setZoom(17);
                                                    }

                                                    marker.setPosition(place.geometry.location);
                                                    marker.setVisible(true);

                                                    bindDataToForm(place.formatted_address, place.geometry.location.lat(), place.geometry.location.lng());
                                                    infowindow.setContent(place.formatted_address);
                                                    infowindow.open(map, marker);

                                                });
                                                // this function will work on marker move event into map
                                                google.maps.event.addListener(marker, 'dragend', function () {
                                                    geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                                                        if (status == google.maps.GeocoderStatus.OK) {
                                                            if (results[0]) {
                                                                bindDataToForm(results[0].formatted_address, marker.getPosition().lat(), marker.getPosition().lng());
                                                                infowindow.setContent(results[0].formatted_address);
                                                                infowindow.open(map, marker);
                                                            }
                                                        }
                                                    });
                                                });
                                            }

                                            function bindDataToForm(address, lat, lng) {
                                                // alert(document.getElementById('lat').value);
                                                document.getElementById('location').value = address;
                                                document.getElementById('lat').value = lat;
                                                document.getElementById('lng').value = lng;
                                                console.log('location = ' + address);
                                                console.log('lat = ' + lat);
                                                console.log('lng = ' + lng);
                                            }

                                            // google.maps.event.addDomListener(window, 'load', initialize);
                                        </script>
                                    <?php endif; ?>
                                    <?php if(is_array($fields)): ?>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo e($form['fields_name'][$key]); ?></label>
                                            <div class="col-md-9">
                                                <div class="input-icon">
                                                    <select class="form-control select2 <?php echo e($key); ?>" name="<?php echo e($key); ?>"
                                                            <?php if($modal_id == 'add-admin' || $modal_id == 'edit-admin'): ?>
                                                            multiple
                                                            <?php endif; ?>
                                                            data-placeholder="Choose <?php echo e($form['fields_name'][$key]); ?>..."
                                                            id="<?php echo e($key); ?>">
                                                        <?php $__currentLoopData = $fields; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=> $field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($k); ?>"
                                                                    <?php if(isset($form['values']) && $form['values'][$key] == $k): ?> selected <?php endif; ?>><?php echo e(ucfirst($field)); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <?php if(is_object($fields) && strpos($key,'[]') !== false): ?>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo e($form['fields_name'][$key]); ?></label>
                                            <div class="col-md-9">
                                                <div class="input-icon">
                                                    <select class="form-control select2 <?php echo e($key); ?>" name="<?php echo e($key); ?>"
                                                            <?php if(strpos($key,'[]') !== false): ?> multiple
                                                            <?php endif; ?> data-placeholder="Choose <?php echo e($form['fields_name'][$key]); ?> ..."
                                                            id="<?php echo e($key); ?>"
                                                            style="    padding: 0;">
                                                        <option></option>

                                                        <?php if(strpos($key,'[]') !== false && isset($form['values'][$key])): ?>
                                                            <?php $__currentLoopData = $form['values'][$key]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <option value="<?php echo e($item->id); ?>"
                                                                        <?php if(in_array($item->id,$roles_id)): ?> selected <?php endif; ?>><?php echo e(ucfirst($item->display_name)); ?></option>

                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            <?php $__currentLoopData = $fields; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                                <?php if(in_array($field->id,$form['values']['role_res[]'])): ?> <?php continue; ?> <?php endif; ?>
                                                                <option
                                                                    value="<?php echo e($field->id); ?>"><?php echo e(ucfirst($field->display_name)); ?></option>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        <?php else: ?>
                                                            <?php $__currentLoopData = $fields; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <option value="<?php echo e($field->id); ?>"
                                                                        <?php if(isset($form['values']) && $form['values'][$key] == $field->id): ?> selected <?php endif; ?>><?php echo e(ucfirst($field->display_name)); ?></option>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        <?php endif; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if(is_object($fields) && strpos($key,'[]') === false): ?>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo e($form['fields_name'][$key]); ?></label>
                                            <div class="col-md-9">
                                                <div class="input-icon">
                                                    <select class="form-control select2 <?php echo e($key); ?>" name="<?php echo e($key); ?>"
                                                            <?php if(strpos($key,'[]') !== false): ?> multiple
                                                            <?php endif; ?> data-placeholder="Choose <?php echo e($form['fields_name'][$key]); ?> ..."
                                                            id="<?php echo e($key); ?>"
                                                            style="    padding: 0;">
                                                        <option></option>

                                                        <?php $__currentLoopData = $fields; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($key == 'cat_id'): ?>
                                                                <option value="<?php echo e($field->category_id); ?>"
                                                                        <?php if(isset($form['values']) && $form['values'][$key] == $field->category_id): ?> selected <?php endif; ?>><?php echo e(ucfirst($field->name)); ?></option>
                                                            <?php else: ?>
                                                                <option value="<?php echo e($field->id); ?>"
                                                                        <?php if(isset($form['values']) && $form['values'][$key] == $field->id): ?> selected <?php endif; ?>><?php echo e(ucfirst($field->name)); ?></option>
                                                            <?php endif; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <button type="submit" class="btn btn-primary closeForm save">Submit
                                                </button>
                                                <button type="button" class="btn btn-secondary closeForm"
                                                        data-dismiss="modal">Cancel
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                <?php echo Form::close(); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<?php /**PATH C:\xampp\htdocs\team-up-bitbucket\resources\views/admin/modal.blade.php ENDPATH**/ ?>