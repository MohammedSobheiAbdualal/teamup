$(document).ready(function () {

    if ($("#users_tbl").length) {

        var users_tbl = $("#users_tbl");
        users_tbl.on('preXhr.dt', function (e, settings, data) {
            //.name,.title,.server,.searcher.,.status
            // data.name = $('.name').val();
            data.full_name = $('#full_name').val();
            data.email = $('#email').val();
            data.is_active = $('#is_active').val();

        }).dataTable({
            "processing": true,
            "serverSide": true,

            "ajax": {
                url: baseURL + "/manage/user-data"
                , "dataSrc": function (json) {
                    //Make your callback here.
                    if (json.status !== undefined && !json.status) {
                        $('#users_tbl_processing').hide();
                        bootbox.alert(json.message);
                        //
                    } else
                        return json.data;
                }
            },
            columns: [
                {data: 'id', name: 'id'},
                {data: 'full_name', name: 'full_name'},
                {data: 'avatar100', name: 'avatar100'},
                {data: 'email', name: 'email'},
                {data: 'mobile', name: 'mobile'},
                {data: 'city', name: 'city'},
                {data: 'is_active', name: 'is_active'},
                {data: 'created_date', name: 'created_date'},
                {data: 'last_login_date', name: 'last_login_date'},
                {data: 'has_league', name: 'has_league'},
                {data: 'action', name: 'action'}
            ],

            language: {
                "sProcessing": "<img src='" + baseAssets + "/img/preloader.svg'>",
            },
            "searching": false,
            "ordering": false,

            bStateSave: !0,
            lengthMenu: [[5, 10, 15, 20, -1], [5, 10, 15, 20, "All"]],
            pageLength: 10,
            pagingType: "full_numbers",
            columnDefs: [{orderable: !1, targets: [0]}, {searchable: !1, targets: [0]}, {className: "dt-right"}],
            order: [[2, "asc"]]
        });
    }
    $(document).on('change', '.is_email_verified', function (event) {

        var _this = $(this);
        var user_id = _this.data('id');
        event.preventDefault();
        $.ajax({
            url: baseURL + '/manage/verify-email',
            type: 'PUT',
            dataType: 'json',
            data: {'_token': csrf_token, 'user_id': user_id},
            success: function (data) {

                if (data.status) {
                    $('.alert').hide();
                    toastr['success'](data.message, '');
                    users_tbl.api().ajax.reload();

                } else {
                    toastr['error'](data.message);
                }

            }
        });

    });


    $(document).on("click", ".filter-submit", function () {
//                if ($(this).val().length > 3)
        users_tbl.api().ajax.reload();
    });
    $(document).on('click', '.filter-cancel', function () {

        $(".select2").val('').trigger('change');
        $(this).closest('tr').find('input,select').val('');
        // $('#is_admin_confirm,.status').val('').trigger('change');
        users_tbl.api().ajax.reload();
    });

    $(document).on('click', '.is_active', function (event) {

        var _this = $(this);
        var user_id = _this.data('id');
        var val= 0;
        if( _this.is(':checked')){
            val= 1;
        }

      // event.preventDefault();
        $.ajax({
            url: baseURL + '/manage/user-activation',
            type: 'PUT',
            dataType: 'json',
            data: {'_token': csrf_token, 'user_id': user_id,'is_active':val},
            success: function (data) {

                if (data.status) {
                    $('.alert').hide();

                    users_tbl.api().ajax.reload();
                    toastr['success'](data.message, '');

                } else {
                    toastr['error'](data.message);
                }

            }
        });

    });

    $(document).on('change', '.has_league', function (event) {
        var _this = $(this);
        var user_id = _this.data('id');
        var val= 0;
       if( _this.is(':checked')){
           val= 1;
       }

        event.preventDefault();
        $.ajax({
            url: baseURL + '/manage/user-league',
            type: 'PUT',
            dataType: 'json',
            data: {'_token': csrf_token, 'user_id': user_id,'is_league':val},
            success: function (data) {

            }
        });
    });

});
