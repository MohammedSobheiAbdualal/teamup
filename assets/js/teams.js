$(document).ready(function () {

    if ($("#teams_tbl").length) {

         var teams_tbl = $("#teams_tbl");
        teams_tbl.on('preXhr.dt', function (e, settings, data) {
            data.sport_id = $('#sport_id').val();
            data.team_name = $('#team_name').val();
           data.owner_name = $('#owner_name').val();
           data.is_active = $('#is_active').val();
        }).dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: baseURL + "/team/team-data"
                ,"dataSrc": function (json) {
                    //Make your callback here.
                    if (json.status !== undefined && !json.status) {
                        $('#teams_tbl_processing').hide();
                        bootbox.alert(json.message);
                        //
                    } else
                        return json.data;
                }
            },
            columns: [
                {data: 'id', name: 'id'},
                 {data: 'name', name: 'name'},
                {data: 'logo100', name: 'logo100'},
                //
                {data: 'owner', name: 'owner'},
                {data: 'total_players', name: 'total_players'},
                {data: 'sport', name: 'sport'},
                {data: 'is_active', name: 'is_active'},
             {data: 'action', name: 'action'}
            ],

            language: {
                "sProcessing": "<img src='" + baseAssets + "/img/preloader.svg'>",
            },
            "searching": false,
            "ordering": false,

            bStateSave: !0,
            lengthMenu: [[5, 10, 15, 20, -1], [5, 10, 15, 20, "All"]],
            pageLength: 10,
            pagingType: "full_numbers",
            columnDefs: [{orderable: !1, targets: [0]}, {searchable: !1, targets: [0]}, {className: "dt-right"}],
            order: [[1, "asc"]]
        });
    }


    $(document).on('click', '.is_active', function (event) {

        var _this = $(this);
        var team_id = _this.data('id');
        var val= 0;
        if( _this.is(':checked')){
            val= 1;
        }
       // event.preventDefault();
        $.ajax({
            url: baseURL + '/team/activation',
            type: 'PUT',
            dataType: 'json',
            data: {'_token': csrf_token, 'team_id': team_id,'is_active':val},
            success: function (data) {

                if (data.status) {

                    if ($("#teams_tbl").length)
                        teams_tbl.api().ajax.reload();

                    $('.alert').hide();
                    toastr['success'](data.message, '');
                }

            else {
                    toastr['error'](data.message);
                }

            }
        });

    });

    $(document).on("click", ".filter-submit", function () {
//                if ($(this).val().length > 3)
        teams_tbl.api().ajax.reload();
    });
    $(document).on('click', '.filter-cancel', function () {

      //  $(".select2").val('').trigger('change');
        $(this).closest('tr').find('input,select').val('');
        // $('#is_admin_confirm,.status').val('').trigger('change');
        teams_tbl.api().ajax.reload();
    });

});
