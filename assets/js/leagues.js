$(document).ready(function () {
var table= $("#leagues_tbl");
    if ($("#leagues_tbl").length) {

        table.on('preXhr.dt', function (e, settings, data) {
            data.sport_id = $('#sport_id').val();
            data.league_name = $('#league_name').val();
            data.owner_name = $('#owner_name').val();
            data.is_active = $('#is_active').val();

            data.type = $('#type ').val();
            data.status = $('#status').val();
            data.start_from = $('#start_from').val();
            data.start_to = $('#start_to').val();
        }).dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: baseURL + "/league/league-data"
                ,"dataSrc": function (json) {
                    //Make your callback here.
                    if (json.status !== undefined && !json.status) {
                        $('#teams_tbl_processing').hide();
                        bootbox.alert(json.message);
                        //
                    } else
                        return json.data;
                }
            },
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'logo100', name: 'logo100'},
                {data: 'user', name: 'user'},
                {data: 'type', name: 'type'},
                {data: 'sport', name: 'sport'},
                {data: 'start_date', name: 'start_date'},
                {data: 'end_date', name: 'end_date'},
                {data: 'status', name: 'status'},
                {data: 'is_active', name: 'is_active'},
                {data: 'action', name: 'action'}
            ],

            language: {
                "sProcessing": "<img src='" + baseAssets + "/img/preloader.svg'>",
            },
            "searching": false,
            "ordering": false,

            bStateSave: !0,
            lengthMenu: [[5, 10, 15, 20, -1], [5, 10, 15, 20, "All"]],
            pageLength: 10,
            pagingType: "full_numbers",
            columnDefs: [{orderable: !1, targets: [0]}, {searchable: !1, targets: [0]}, {className: "dt-right"}],
            order: [[1, "asc"]]
        });
    }


    $(document).on('click', '.is_active', function (event) {

        var _this = $(this);
        var league_id = _this.data('id');
        var val= 0;
        if( _this.is(':checked')){
            val= 1;
        }
        event.preventDefault();
        $.ajax({
            url: baseURL + '/league/activation',
            type: 'PUT',
            dataType: 'json',
            data: {'_token': csrf_token, 'league_id': league_id,'is_active':val},
            success: function (data) {

                if (data.status) {
                    $('.alert').hide();
                    toastr['success'](data.message, '');

                    if ($("#leagues_tbl").length)
                        table.api().ajax.reload();

                }

                else {
                    toastr['error'](data.message);
                }

            }
        });

    });

    $(document).on("click", ".filter-submit", function () {
//                if ($(this).val().length > 3)
        table.api().ajax.reload();
    });
    $(document).on('click', '.filter-cancel', function () {

        //  $(".select2").val('').trigger('change');
        $(this).closest('tr').find('input,select').val('');
        // $('#is_admin_confirm,.status').val('').trigger('change');
        table.api().ajax.reload();
    });

});
