$(document).ready(function () {

    var table=$("#sports_tbl");;
    if ($("#sports_tbl").length) {
        table.on('preXhr.dt', function (e, settings, data) {
          data.sport_name = $('#sport_name').val();
          data.is_active = $('#is_active').val();
        }).dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: baseURL + "/sport/sports-data"
                ,"dataSrc": function (json) {
                    //Make your callback here.
                    if (json.status !== undefined && !json.status) {
                        $('#sports_tbl_processing').hide();
                        bootbox.alert(json.message);
                        //
                    } else
                        return json.data;
                }
            },
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name_en', name: 'name_en'},
                {data: 'name_ar', name: 'name_ar'},
                {data: 'image', name: 'image'},
                {data: 'icon', name: 'icon'},
                {data: 'position', name: 'position'},
                {data: 'is_active', name: 'is_active'},
                {data: 'action', name: 'action'}
            ],

            language: {
                "sProcessing": "<img src='" + baseAssets + "/img/preloader.svg'>",
            },
            "searching": false,
            "ordering": false,

            bStateSave: !0,
            lengthMenu: [[5, 10, 15, 20, -1], [5, 10, 15, 20, "All"]],
            pageLength: 10,
            pagingType: "full_numbers",
            columnDefs: [{orderable: !1, targets: [0]}, {searchable: !1, targets: [0]}, {className: "dt-right"}],
            order: [[1, "asc"]]
        });
    }


    $(document).on('click', '.is_active', function (event) {

        var _this = $(this);
        var sport_id = _this.data('id');
        var val= 0;
        if( _this.is(':checked')){
            val= 1;
        }
        // event.preventDefault();
        $.ajax({
            url: baseURL + '/sport/activation',
            type: 'PUT',
            dataType: 'json',
            data: {'_token': csrf_token, 'sport_id': sport_id,'is_active':val},
            success: function (data) {

                if (data.status) {

                    if ($("#sports_tbl").length)
                        table.api().ajax.reload();

                    $('.alert').hide();
                    toastr['success'](data.message, '');
                }

                else {
                    toastr['error'](data.message);
                }

            }
        });

    });

    $(document).on("click", ".filter-submit", function () {
//                if ($(this).val().length > 3)
        table.api().ajax.reload();
    });
    $(document).on('click', '.filter-cancel', function () {

        //  $(".select2").val('').trigger('change');
        $(this).closest('tr').find('input,select').val('');
        // $('#is_admin_confirm,.status').val('').trigger('change');
        table.api().ajax.reload();
    });

    $(document).on('click', '.btn-delete', function (event) {

        var _this = $(this);
        var id=$(this).data('id');

        var sport_name = _this.closest('tr').find("td:eq(1)").text();
        bootbox.confirm({
            message: "Are you sure of the deletion admin name (" + sport_name + ")?",
            buttons: {

                cancel: {
                    label: '<i class="fa fa-remove"></i> Cancel',
                    className: 'btn-danger'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm',
                    className: 'btn-success'
                }
            },
            callback: function (result) {
                if (result) {
                    $.ajax({
                        url: baseURL + '/sports/'+id,
                        type: 'DELETE',
                        dataType: 'json',
                        data: {'_token': csrf_token},
                        success: function (data) {

                            if (data.status) {
                                $('.alert').hide();
                                toastr['success'](data.message, '');

                                if ($("#sports_tbl").length)
                                    table.api().ajax.reload();

                            }
                            else {
                                toastr['error'](data.message);
                            }

                        }
                    });
                }
            }
        });


    });

});


$(document).on('click','.submitBtn',function () {

    var en =$("input[name=PositionEn]").val();
     var ar=$("input[name=PositionAr]").val();
     var abbrev=$("input[name=abbrev]").val();

    if ((en == null || en == "") || (ar == null || ar == "") ){

        $(".nameVal").html("Name in English and Arabic must be filled out");
        return false;
    }
    else if((abbrev == null || abbrev == "")){

            $(".abbrevVal").html("Position Abbreviation must be filled out");
        return false;
    }
    else
    {

        $('#PositionBodyTable').append("<tr><td>" + en  + "</td><td>" + ar + "</td><td>" + abbrev + "</td><td>" +" <a class='btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air delete-row'><i class='fa fa-trash' data-placement='bottom' title='delete'></i> </a>"+'<input type="hidden" name="position[0][]" value="'+en+'"><input type="hidden" name="position[1][]" value="'+ar+'"><input type="hidden" name="'+"position[2][]"+'" value="'+abbrev+'"></td></tr>');


        $("#sportModal").modal("hide");
        $('.modal-backdrop').remove();
        $("input[name=PositionEn]").val('');
        $("input[name=PositionAr]").val('');
        $("input[name=abbrev]").val('');
        $("#positionTable").css ("display","block") ;
        $(".nameVal").html('');
        $(".abbrevVal").html('');
    }
});

$(document).on('click','.delete-row',function () {
    $(this).closest('tr').remove();
});

$(document).on('click','.save',function (event) {
    var _this = $(this);

    event.preventDefault();
    form=$('#form')[0];
    var formData = new FormData(form);
    image=$('.dz-filename').find('span').text();
    $.ajax({
        url:baseURL+'/sports',
        type: 'post',
        data: formData,

        contentType: false,
        processData: false,
        success: function (data) {

            if (data.status) {
                if(image != '' && image !=null)
                {
                    myDropzone.options.url = data.items.url;
                    myDropzone.processQueue();
                }
                $('.alert-error').html('');
                toastr['success'](data.message, '');
                $("#form")[0].reset();
                $('#PositionBodyTable').html('');
                $('#positionTable').css('display','none');

            }
            else {
                var $errors = '<strong>' + data.message + '</strong>';
                $errors += '<ul>';
                console.log(data.items);
                $.each(data.items, function (i, v) {
                    $errors += '<li>' + v.message + '</li>';
                });
                $errors += '</ul>';
                console.log($errors);
                $('.alert-error').show();
                $('.alert-error').html($errors);

            }

            // _this.find('.fa-spin').hide();

        },
        error:function (xhr) {
            console.log(xhr);
        }
    });
});

$(document).on('click','.edit',function (event) {
    var _this = $(this);

    event.preventDefault();
    form=$('#editForm')[0];
    var formData = new FormData(form);
    var sport_id=$('#editForm').find('#sport_id').val();
     image=$('.dz-filename').find('span').text();

    $.ajax({
        url:baseURL+'/sport/'+sport_id,
        type: 'post',
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {

            if (data.status) {

                if(image != '' && image !=null)
                {
                    myDropzone.options.url = data.items.url;
                    myDropzone.processQueue();
                }

                $('.alert-error').html('');
                toastr['success'](data.message, '');
                // $("#editForm")[0].reset();
                // $('#PositionBodyTable').html('');
                // $('#positionTable').css('display','none');

            }
            else {
                var $errors = '<strong>' + data.message + '</strong>';
                $errors += '<ul>';
                $.each(data.items, function (i, v) {
                    $errors += '<li>' + v.message + '</li>';
                });
                $errors += '</ul>';
                console.log($errors);
                $('.alert-error').show();
                $('.alert-error').html($errors);

            }

            // _this.find('.fa-spin').hide();

        },
        error:function (xhr) {
            console.log(xhr);
        }
    });
});
